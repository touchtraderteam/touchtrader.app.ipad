# TouchTrader prototype alpha v1.0

This application is a prototype for TouchTrader.IO in order to test touch based interfaces for a crypto exchange simulator.

## Note

Commits and history has been reseted due to privacy requests by the developers at the ICO stage of TouchTrader.
Any technical questions or doubts, feel free to send an email to info@touchtrader.io and we'll be happy to reply ;)


## Ecosystem

https://www.touchtrader.io

https://ico.touchtrader.io

## Built With

* [XCode](https://developer.apple.com/xcode/) - Framework used
* [ObjectiveC](https://en.wikipedia.org/wiki/Objective-C) - Language used (why not Swift? We like Objective-C :) )
* [SpriteKit](https://developer.apple.com/documentation/spritekit) - Great for prototyping


## Acknowledgments

* Hat tip to all the one's involved in TouchTrader and by putting in the extra hours to take this idea to the next level.
