//
//  bgViewController.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "bgViewController.h"
#import "mainScene.h"
#import "menuScene.h"

@implementation bgViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

  
    
    // Create and configure the scene.
    
  
}

- (void)viewWillLayoutSubviews
{
    
    [super viewWillLayoutSubviews];
    
    [self initialSetup];
    
    if (current_scene!=nil) {
        
    }
    else {
        [self presentScene:@"menu"];
    }
    
    
}



/**
 *
 */
-(void) initialSetup {
    
    // Configure the view.
    skView = (SKView *)self.view;
    //skView.showsFPS = YES;
    //skView.showsNodeCount = YES;
    //skView.showsPhysics = YES;
    //skView.showsDrawCount = YES;
   // NSLog(@"width: %f", skView.bounds.size.width);
   // NSLog(@"height %f", skView.bounds.size.height);
    
}


/**
 *  @presentScene
 */
-(void) presentScene : (NSString *) sceneName {
    
 
    if ([sceneName isEqualToString:@"menu"]) {
       current_scene = [menuScene sceneWithSize:skView.bounds.size];
    }
    
    else if ([sceneName isEqualToString:@"main"]) {
      current_scene = [mainScene sceneWithSize:skView.bounds.size];
    }
    
    current_scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:current_scene];

    
}



- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
