//
//  bgMyScene.h
//  blockGame
//

//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "dataManager.h"
#import "factory.h"
#import "gameContainer.h"


#import "blockBucket.h"
#import "dropLineBucket.h"
#import "gameEventZone.h"
#import "interSpreadZone.h"


@class menuContainer;
@class wallet;
@class gameContainer;
@class gameEventZone;
@class interSpreadZone;
@class block;



@interface mainScene : SKScene  <SKPhysicsContactDelegate> {
    
    NSMutableArray * blockCollection;
    // Gesture Recognizers.
    UITapGestureRecognizer * tapGestureRecognizer;
    UIPanGestureRecognizer * panGestureRecognizer;
    UILongPressGestureRecognizer * longGestureRecognizer;
    UISwipeGestureRecognizer *swipeRecognizer;
    // Initial window
    SKSpriteNode * iFrame;
    // Double tap
    UITapGestureRecognizer *doubleTapGestureRecognizer;
 
    
    // Deposit monye
    SKShapeNode * depositMoney;
    SKLabelNode * depositMoneyLabel;
    // ***
    BOOL canTouch;
    BOOL blockIsSelected;
    // **
    
    // Inverted trade
    block * askBucketCoin;
    block * askDropLineCoin;
    BOOL askCoinCollidesWithDropLineCoin;
    
    //Bid line collision
    BOOL blockCollidesWithBidLine;
    
    BOOL moneyBlockCollidesWithBidLine;
    
    //BOOL blockCollidesWithDropLine;
    BOOL blockCollidesWithDropLineBucket;
    dropLineBucket * currentActiveDropLineBucket;
    
    // coinWallet collides with Bucket
    BOOL coinWalletCollidesBucket;
    BOOL blockBucketCoinCollidesWallet;
    
    blockBucket * currentActiveBucket;
    block * currentBlockWalletCoin;
    wallet * currentActiveWallet;
    block * currentBlockBucketCoin;
}


// Main Containers
@property (nonatomic, strong) menuContainer * menuContainer;
@property (nonatomic, strong) gameContainer * gameContainer;


// Block buckets
@property (nonatomic, strong) blockBucket * bitcoinBucket;
@property (nonatomic, strong) blockBucket * moneyBucket;

// Game Event zone
@property (nonatomic, strong) gameEventZone * gameZone;

// DropLine buckets
@property (nonatomic, strong) dropLineBucket * topDropLineBucket;
@property (nonatomic, strong) dropLineBucket * bottomDropLineBucket;

// Control oriented
@property (nonatomic, strong) SKSpriteNode * selectedNode;


-(void) runHeatMap;
- (void)panGesture:(UIPanGestureRecognizer *)recognizer;

@end
