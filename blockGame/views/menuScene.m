//
//  bgMyScene.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "menuScene.h"
#import "Constant.h"
#import "bgViewController.h"




@implementation menuScene

-(id)initWithSize:(CGSize)size {
    
    if (self = [super initWithSize:size]) {
        [self setScene];
    }
    return self;
}


/**
 *
 */
-(void) didMoveToView:(SKView *)view{

}


/**
 *
 */
-(void) setScene {
    
    self.backgroundColor = [UIColor whiteColor];
    
    //  SKLabelNode * titleNode = [[factory singleton]buildLabel:@"Touch Trader" :CGPointMake(self.frame.size.width/2, self.frame.size.height/2) :[UIColor blueColor]];
    
    hasStarted = FALSE;
    touchTraderLogo = [SKSpriteNode spriteNodeWithImageNamed:@"touchtrader_icon_min"];

    [self addChild:touchTraderLogo];
    int position_x = self.frame.origin.x + self.frame.size.width/2;
    int position_y = self.frame.origin.y + self.frame.size.height/2;
    
    [touchTraderLogo setPosition:CGPointMake(position_x, position_y)];
    [touchTraderLogo setSize:CGSizeMake(831,136)];
    
    [self runAction:[SKAction waitForDuration:2] completion:^ {
        
   
        [self presentMainScene];
        hasStarted = TRUE;

    }];
    
 
    
}


/**
 *
 */
- (void) presentMainScene {
    SKScene * mainSceneInstance = [[mainScene alloc] initWithSize:self.size];
    SKTransition *doors = [SKTransition doorsOpenHorizontalWithDuration:0.5];
    
    [self.view presentScene:mainSceneInstance transition:doors];
    
}



/**
 *   @A scene method i imagine.
 */
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    /*for (UITouch *touch in touches) {
        
        if (!hasStarted) {
            [self presentMainScene];
            
        }
        
    }
     */
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    
    
}



@end
