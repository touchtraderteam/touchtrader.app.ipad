//
//  Constant.h
//  ISnap
//
//  Created by Mac on 9/5/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import "dataManager.h"

@class blockBucket;
@class dropLineBucket;


#ifndef blockGame_Constant_h
#define blockGame_Constant_h

// Global References
blockBucket * _globalBitcoinBucket;
blockBucket * _globalMoneyBucket;
dropLineBucket * _globalBitcoinDropLineBucket;
dropLineBucket * _globalMoneyDropLineBucket;


// Debug Mode
#define DEBUG_MODE 1

// RESPONSIVE

#define IS_IPHONE (([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height <= 480.0f) ? YES:NO)

#define IS_IPHONE_5 (([UIScreen mainScreen].scale == 2.f && [UIScreen mainScreen].bounds.size.height > 480.0f)?YES:NO)

#define IS_IPAD (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? YES : NO)
#define IS_RETINA_DISPLAY_DEVICE (([UIScreen mainScreen].scale == 2.f)?YES:NO)


// Menu height
#define MAIN_MENU_HEIGHT 60


// MAIN ID'S
#define GAME_CONTAINER @"gameContainer"
#define MENU_CONTAINER @"menuContainer"

// MONEY
#define BUCKET_MONEY_NAME @"moneyBucket"
#define MONEY_BUCKET_ID @"money"

// BITCOIN
#define BUCKET_BITCOIN_NAME @"bitcoinBucket"
#define BITCOIN_BUCKET_ID @"bitcoin"

// WALLET
#define BITCOIN_WALLET_NAME @"wallet_bitcoinWallet"
#define MONEY_WALLET_NAME @"wallet_moneyWallet"


#define TRADING_BID_TIMER_VALUE 15



BOOL interspreadOn;

BOOL gameIsFrozen;
NSString * currentTargetTradeBlock;


// EXTREME VALUES

float total_user_net_value;
float money_bucket_current_net_value;
float bitcoin_bucket_current_net_value;

float bitcoin_orderbook_bestprice;
float money_orderbook_bestprice;



// TRADE DATA FILE RELATED
#define TRADE_DATA_FILE_NAME @"bitcoinData"
#define BID_KEY @"bid"
#define ASK_KEY @"ask"
#define DROPLINE_BLOCK_QUANTITY 4

#define HIGHEST_BID_VALUE [[dataManager singleton] getExtremeValues:BID_KEY:@"high"]
#define LOWEST_BID_VALUE [[dataManager singleton] getExtremeValues:BID_KEY:@"low"]

#define HIGHEST_ASK_VALUE [[dataManager singleton] getExtremeValues:ASK_KEY:@"high"]
#define LOWEST_ASK_VALUE [[dataManager singleton] getExtremeValues:ASK_KEY:@"low"]


#define TRADE_BID_BLOCK_QUANTITY [[dataManager singleton] getBidEventsQuantity]
#define TRADE_ASK_BLOCK_QUANTITY [[dataManager singleton] getAskEventsQuantity]


// BLOCK COLLIDING
static const uint32_t gamecontainer_category =  0x1 << 0;
static const uint32_t droplinebucket_category =  0x1 << 1;
static const uint32_t bucket_category  =  0x1 << 2;
static const uint32_t block_category =  0x1 << 3;
static const uint32_t wallet_money_category =  0x1 << 4;
static const uint32_t wallet_bitcoin_category =  0x1 << 5;
static const uint32_t dropline_block_category =  0x1 << 6;

static const uint32_t bidline_category =  0x1 << 7;

/*
static const uint32_t block_bitcoin_bid_category =  0x1 << 8;
static const uint32_t block_money_bid_category =  0x1 << 9;
*/


//Trade control
int tradeIsPossible;

// DropLine trade control
int targetBlockBucketIsFull;


// THEMING

// SLICK THEME
#define LABEL_FONT @"system"
#define LABEL_FONTSIZE 40
#define BLOCK_LABEL_FONT @"system"
#define BLOCK_LABEL_FONTSIZE 10
#define DROPLINE_EXTREME_LABEL_COLOR [UIColor whiteColor]
#define TEXT_COLOR_GRAY [UIColor colorWithRed:73.0f/255.0f green:73.0f/255.0f blue:73.0f/255.0f alpha:1.0]

#define SLICK_WALLET_LABEL_COLOR [UIColor whiteColor]
#define SLICK_OPTIMA_BOLD @"Optima-Bold"
#define SLICK_OPTIMA_REGULAR @"Optima-Regular"
#define SLICK_OPTIMA_ITALIC @"Optima-Italic"
#define SLICK_OPTIMA_BOLD_ITALIC @"Optima-BoldItalic"


#define SLICK_PROFIT_RED [UIColor colorWithRed:204.0f/255.0f green:56.0f/255.0f blue:56.0f/255.0f alpha:1.0]
#define SLICK_PROFIT_GREEN [UIColor colorWithRed:60.0f/255.0f green:200.0f/255.0f blue:97.0f/255.0f alpha:1.0]


// ANOTHER THEME


// GAME EVENT ZONE
#define GAME_INFO_LABEL_COLOR [UIColor whiteColor] //[UIColor colorWithRed:83.0f/255.0f green:82.0f/255.0f blue:82.0f/255.0f alpha:1.0]


// INDICATORS
BOOL INDICATOR_BID_ASK_BUILD;
BOOL INDICATOR_BID_ASK_DISPLAY;

BOOL HEAT_MAP_DISPLAY;
BOOL HEAT_MAP_DRAG_BITCOIN_DISPLAY;


// SLICK THEME
#define SLICK_BLACK [UIColor colorWithRed:55.0f/255.0f green:55.0f/255.0f blue:55.0f/255.0f alpha:1.0]
#define SLICK_GRAY [UIColor colorWithRed:85.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1.0]
#define COIN_MAX_BAR_WIDTH 300
#define COIN_MIN_BAR_WIDTH 80

// PERCENTAGE BAR
#define LIGHT_GREEN_COLOR [UIColor colorWithRed:0.93 green:0.79 blue:0.34 alpha:1.0]
#define LIGHT_ORANGE_COLOR [UIColor colorWithRed:0.54 green:0.74 blue:0.36 alpha:1.0]


// WALLET
#define MONEY_WALLET_START_VALUE 60000
#define MONEY_BUCKET_START_VALUE 20000
#define BITCOIN_WALLET_START_VALUE 0
#define BITCOIN_CURRENCY @"BTC"
#define MONEY_CURRENCY @"$"
#define CURRENCY_SYMBOL $
#define MONEY_WALLET_BACKGROUND_COLOR [UIColor colorWithRed:56.0f/255.0f green:56.0f/255.0f blue:56.0f/255.0f alpha:1.0]


// COLORS
#define BITCOIN_BLUE_COLOR [UIColor colorWithRed:242.0f/255.0f green:144.0f/255.0f blue:25.0f/255.0f alpha:1.0]
#define MONEY_GREEN_COLOR [UIColor colorWithRed:0.22 green:0.58 blue:0.31 alpha:1.0]
#define BUCKET_COLOR [UIColor colorWithRed:0.79 green:0.78 blue:0.78 alpha:1.0]
#define DROPLINE_BUCKET_COLOR [UIColor whiteColor]
#define GAMECONTAINER_COLOR [UIColor whiteColor]
#define DROP_LINE_COLOR [UIColor darkGrayColor]

#define PROFIT_YELLOW_COLOR [UIColor colorWithRed:0.0f/255.0f green:180.0f/255.0f blue:17.0f/255.0f alpha:1.0]
#define LOSS_COLOR [UIColor colorWithRed:216.0f/255.0f green:49.0f/255.0f blue:49.0f/255.0f alpha:1.0]


#define SOFT_BLUE [UIColor colorWithRed:37.0f/255.0f green:126.0f/255.0f blue:176.0f/255.0f alpha:1.0]

#define GRAY_BACKGROUND [UIColor colorWithRed:32/255.0 green:138/255.0 blue:165/255.0 alpha:1]
#define BACKGROUND_COLOR [UIColor whiteColor]


// HEATMAP COLORS
#define HEATMAP_LABEL_LOSS_COLOR [UIColor colorWithRed:233.0f/255.0f green:14.0f/255.0f blue:14.0f/255.0f alpha:1.0]
#define HEATMAP_LABEL_PROFIT_COLOR [UIColor colorWithRed:128.0f/255.0f green:192.0f/255.0f blue:75.0f/255.0f alpha:1.0]

#define HEATMAP_CIRCLE_PROFIT_COLOR [UIColor colorWithRed:120.0f/255.0f green:231.0f/255.0f blue:71.0f/255.0f alpha:1.0]
#define HEATMAP_CIRCLE_LOSS_COLOR [UIColor colorWithRed:233.0f/255.0f green:14.0f/255.0f blue:14.0f/255.0f alpha:1.0]

#define HEATMAP_BITCOIN_CIRCLE_PROFIT_COLOR [UIColor colorWithRed:4.0f/255.0f green:180.0f/255.0f blue:87.0f/255.0f alpha:1.0]

// HEATMAP z-index
#define HEATMAP_CIRCLE_ZPOSITION 11
#define HEATMAP_QUANTITYBAR_ZPOSITION 10
#define potentialProfitWrapper_ZPOSITION 20

// TRADING FLOW
float bitcoin_reference_lowest_value;
float money_reference_highest_value;

// DROPLINE
#define DROP_LINE_HEIGHT 1
#define BLOCK_DROPLINE_SPACE 20
#define BLOCK_TRADE_FADE_DURATION 1.5



// DROPLINE BUCKET
#define DROPLINEBUCKET_DROPLINES_LIMIT 3
#define DROPLINES_BLOCKS_LIMIT 10
#define DROPLINE_BETWEEN_SPACE 60



// BLOCK BUCKET RELATED
#define BUCKET_BACKGROUND_COLOR [UIColor purpleColor]

// CURRENCY


// PREFIXES
#define BLOCK_BUCKET_PREFIX @"block_bucket"
#define BLOCK_WALLET_PREFIX @"block_wallet"
#define BLOCK_DROPLINE_PREFIX @"block_dropline"
#define BLOCK_BLOCKCOIN_PREFIX @"block_coin"

#define WALLET_PREFIX @"wallet_"

#define BIDLINE_COIN_PREFIX @"block_bidline"




// MODEL RELATED
#define BLOCK_BUCKET_TRADE_UNIQUE_VALUE 4000
#define BLOCK_BUCKET_VISIBLE_QUANTITY 5
#define BLOCK_BUCKET_TRADE_QUANTITY_VALUE 1
//#define BLOCK_BUCKET_TRADE_BICOIN_UNIQUE_VALUE 5000

#define BLOCK_NAME @"block_"

// DESIGN RELATED
#define BLOCK_BUCKET_SPACE 10
#define BITCOIN_BLOCK_BUCKET_ORIGIN_X 0
#define BITCOIN_BLOCK_BUCKET_ORIGIN_Y 0
#define BITCOIN_TEXTURE @"bitcoin_texture.png"
#define DOLLAR_TEXTURE @"dollar_texture.png"



// MEDIA RELATED
#define SOUND_EXTENSION @"caf"

// BLOCK SIZE RELATED

#define COIN_DEFAULT_SIZE 55


#define BITCOIN_BLOCK_MAX_WIDTH 95
#define BITCOIN_BLOCK_MAX_HEIGHT 95

#define BITCOIN_BLOCK_MIN_WIDTH 25
#define BITCOIN_BLOCK_MIN_HEIGHT 25

#define BLOCK_MAX_WIDTH 55
#define BLOCK_MAX_HEIGHT 55

#define BLOCK_MIN_WIDTH 25
#define BLOCK_MIN_HEIGHT 25

#define BLOCK_BUCKET_WIDTH (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 30 : 25)
#define BLOCK_BUCKET_HEIGHT (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 30 : 25)

#define BLOCK_PILAR_WIDTH (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 20 : 15)




#endif
