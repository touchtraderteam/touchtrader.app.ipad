//
//  bgViewController.h
//  blockGame
//

//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface bgViewController : UIViewController {
    
    SKView * skView;
    SKScene * current_scene;
}

@end
