//
//  bgMyScene.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "mainScene.h"

@implementation mainScene {
    

}


-(id)initWithSize:(CGSize)size {
    
    if (self = [super initWithSize:size]) {
        [self loadTradeData];
    }
    return self;
}


/**
 *  @loadTradeData
 */
-(void) loadTradeData {
   
    [[dataManager singleton] setupTradeDataJson];
    
    // Setup default values
    total_user_net_value =  (BLOCK_BUCKET_TRADE_UNIQUE_VALUE * BLOCK_BUCKET_VISIBLE_QUANTITY);
    money_bucket_current_net_value = total_user_net_value;
    bitcoin_bucket_current_net_value = 0;
    
    
    // Setups the reference bid and ask lowest and high values for the future.
    [[dataManager singleton] setFirstBlocksReferenceValue];
    
}


/**
 *  @setScene
 */
-(void) setScene {
    
    self.backgroundColor = BACKGROUND_COLOR;
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.physicsWorld.gravity = CGVectorMake(0, 0);
    self.physicsWorld.contactDelegate = self;
    gameIsFrozen = false;
    interspreadOn = false;
    
    tradeIsPossible = 1;
    
}


/**
 *  @didMoveToView: great place to initiate a lot of stuff.
 */
-(void) didMoveToView:(SKView *)view {
    
    [self setScene];
    [self addGestureRecognizers];
    [self loadContainers];
    [self loadInitialMoneyDeposit];
    
}


/**
 *  @addGestureRecognizers.
 */
-(void) addGestureRecognizers {
    
    
    swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget: self action:@selector( handleSwipe:)];
    [swipeRecognizer setDirection: UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer: swipeRecognizer ];
    
    
    
   tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [[self view] addGestureRecognizer:tapGestureRecognizer];
    
    // Long Hold
    longGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGesture:)];
    longGestureRecognizer.minimumPressDuration = 1;
    longGestureRecognizer.allowableMovement = 600;
    [self.view addGestureRecognizer:longGestureRecognizer];
    
    // Double tap
    doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(doubleTapGesture:)];
    doubleTapGestureRecognizer.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGestureRecognizer];
    [tapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
    
    // Start pan gesture recognizer
    panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
    [[self view] addGestureRecognizer:panGestureRecognizer];
    
    // Swipe gesture recognizer
    
    //swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    //swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    //swipeRecognizer.direction = UI;
    //[[self view] addGestureRecognizer:swipeRecognizer];
    
    
    
    
    
    //pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:[self action:@selector(pinchGesture:)]' action:<#(SEL)#>]
    
}



/**
 *  @handleSwipe
 */
- (void)handleSwipe:(UISwipeGestureRecognizer *) recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
       
        CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        touchLocation = [self convertPointFromView:touchLocation];
        //[self selectBlockForDoubleTap:touchLocation:@"doubletap"];
         NSLog(@"ENDED");
    }
  
    
    
    if ( recognizer.numberOfTouches == 2) {
        NSLog(@"NUMBER OF TOUCHES 2");
        // do something if the swipe right had exactly 2 fingers involved
    } else {
        NSLog(@"SOMETHING");
        // do something else
    }
    

    if(recognizer.state == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"SWIPE RIGHT");

    }
    
    else if(recognizer.state == UISwipeGestureRecognizerDirectionLeft) {
         NSLog(@"SWIPE LEFT");
    }
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"UIGestureRecognizerStateBegan RIGHT");

    }
    
    if (recognizer.state == UIGestureRecognizerStateRecognized) {
        NSLog(@"UIGestureRecognizerStateRecognized RIGHT");
    }
    

}

/**
 *  @longPressGesture
 */
-(void) swipeGesture: (UISwipeGestureRecognizer *) recognizer {
    
    
    if (recognizer.state == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"SWIPING RIGHT");
    }
    
    else if (recognizer.state == UISwipeGestureRecognizerDirectionLeft) {
         NSLog(@"SWIPING LEFT");
        
    }
    
    else if (recognizer.state == UISwipeGestureRecognizerDirectionUp) {
        NSLog(@"SWIPING UP");
        
    }
    
   // else if (recognizer.state == UISwipeGestureRecognizerDirectionDown) {
     //   NSLog(@"SWIPING DOWN");
   // }
 
}


/**
 *  @selectBlockForDoubleTap
 */
-(void) selectBlockForDoubleTap:(CGPoint) tapLocation : (NSString *) gestureType {
    
    SKSpriteNode *touchedNode = (SKSpriteNode *)[self nodeAtPoint:tapLocation];
    
    // FOR BLOCK BUCKETS
 
    //BOOL bucketBlockDoubleClicked = false;
    //BOOL bucketBlockLongClicked = false;
    BOOL blockSelected = true;
    
    if ([touchedNode.name hasPrefix:BLOCK_BUCKET_PREFIX]) {
        _selectedNode = (SKSpriteNode *) touchedNode;
        blockSelected = TRUE;
    }
    else if([[touchedNode parent].name hasPrefix:BLOCK_BUCKET_PREFIX]) {
        _selectedNode = (SKSpriteNode *) [touchedNode parent];
        blockSelected = TRUE;
    }
    
    else if([[touchedNode parent].name hasPrefix:BIDLINE_COIN_PREFIX]) {
        _selectedNode = (SKSpriteNode *) [touchedNode parent];
        blockSelected = TRUE;
    }
    

    if (blockSelected) {
        
        block * coin = (block *) _selectedNode;
        //if ([coin isBidLineBlock] && blockCollidesWithBidLine) {
        if ([coin isBidLineBlock] && interspreadOn) {
            // Bidline success
            blockCollidesWithBidLine = FALSE;
            [[interSpreadZone singleton] finishInterSpreadSuccess];
        }
        
        // Directly attached to block bucket
        if ([[_selectedNode parent] isKindOfClass:[blockBucket class]]) {
            
            blockBucket * tempBlockBucket = (blockBucket *)[_selectedNode parent];
            if ([gestureType isEqualToString:@"doubletap"]) {
                if ([tempBlockBucket isBitCoinBucket]) {
                    
                }
                else {
                    [[gameEventZone singleton] displayInfoTime:@"Merging block coin..":2];
                    [tempBlockBucket bundleUpCoins:(block *)_selectedNode];
                }
                
            }
            
            else if ([gestureType isEqualToString:@"longpress"]) {
                if ([tempBlockBucket isBitCoinBucket]) {
                    
                }
                else {
                    [tempBlockBucket splitUpCoin:(block *)_selectedNode];
                }
            }
        }
    }
    
 

    
    /* Previous double tap for wallet.
    if ([touchedNode.name hasPrefix:WALLET_PREFIX]) {
        _selectedNode = (SKSpriteNode *) touchedNode;
            
        if ([touchedNode.name isEqualToString:BITCOIN_WALLET_NAME]) {
            tempBlockBucket = _bitcoinBucket;
            walletDoubleClicked = true;
        }
            
        else if ([touchedNode.name isEqualToString:MONEY_WALLET_NAME]) {
            tempBlockBucket = _moneyBucket;
            walletDoubleClicked = true;
        }
            
            
           
        if (walletDoubleClicked) {
            if ([[tempBlockBucket getBucketWallet] isWalletOpen]) {
                [[tempBlockBucket getBucketWallet] closeWallet];
                gameIsFrozen = false;

            }
            else {
                [[tempBlockBucket getBucketWallet] openWallet];
                gameIsFrozen = true;
            }
                
       }
 
    }
     */

}

/**
 *  @selectBlockForTap
 *
 */
-(void) selectBlockForTap:(CGPoint) tapLocation {
   
    SKSpriteNode *touchedNode = (SKSpriteNode *)[self nodeAtPoint:tapLocation];

    if (![_selectedNode isEqual:touchedNode]) {
        
        
        // 1. Arrange the previous block.
        if ([_selectedNode isKindOfClass:[block class]]) {
            //block * blockObject = (block *) _selectedNode;
           // [blockObject setGameColor];
        }
        
        
        if([touchedNode isKindOfClass:[block class]] ) {
            _selectedNode = touchedNode;
        }
        
        if ([[touchedNode parent] isKindOfClass:[block class]]) {
            _selectedNode = (SKSpriteNode *) [touchedNode parent];
        }
        
        if([touchedNode isKindOfClass:[block class]] || [[touchedNode parent] isKindOfClass:[block class]]) {

            block * blockObject = (block *) _selectedNode;
            [[menuContainer singleton] setBlockInfo:blockObject];
        
        }
        
        else {
           // _selectedNode = nil;
        }
    }
}


/**
 *  @initialSetup
 */
- (void)selectBlockForDrag:(CGPoint)touchLocation {
    
    SKSpriteNode *touchedNode = (SKSpriteNode *)[self nodeAtPoint:touchLocation];

    // 1.   A different Node has been selected.
    BOOL checkDragBlockCoin = FALSE;
    BOOL checkDragWallet = FALSE;
    
	if (![_selectedNode isEqual:touchedNode]) {

        
        if ([_selectedNode isKindOfClass:[block class]]) {
            // nothing here/
        }
        
        // FOR BLOCK BUCKETS
        if ([touchedNode.name hasPrefix:WALLET_PREFIX]) {
            checkDragWallet = TRUE;
            _selectedNode = (SKSpriteNode *) touchedNode;
        }

        // FOR BLOCK BUCKETS
        if ([touchedNode.name hasPrefix:BLOCK_BLOCKCOIN_PREFIX] && [[touchedNode parent].name hasPrefix:BLOCK_BUCKET_PREFIX]) {
            checkDragBlockCoin = TRUE;
            _selectedNode = (SKSpriteNode *)[touchedNode parent];
        }
        
        if ([touchedNode.name hasPrefix:BLOCK_BLOCKCOIN_PREFIX] && [[[touchedNode parent] parent].name hasPrefix:BLOCK_BUCKET_PREFIX]) {
            checkDragBlockCoin = TRUE;
            _selectedNode = (SKSpriteNode *)[[touchedNode parent] parent];
        }
        
        else if ([touchedNode.name hasPrefix:BLOCK_BUCKET_PREFIX]) {
            checkDragBlockCoin = TRUE;
            _selectedNode = touchedNode;
        }
        
        
        // FOR DROPLINES
        if ([touchedNode.name hasPrefix:BLOCK_BLOCKCOIN_PREFIX] && [[touchedNode parent].name hasPrefix:BLOCK_DROPLINE_PREFIX]) {
            checkDragBlockCoin = TRUE;
            _selectedNode = (SKSpriteNode *)[touchedNode parent];
        }
        
        else if ([touchedNode.name hasPrefix:BLOCK_DROPLINE_PREFIX]) {
            checkDragBlockCoin = TRUE;
            _selectedNode = touchedNode;
        }
        
        
        
        // FOR WALLETS
        /*
        if ([touchedNode.name hasPrefix:BLOCK_BLOCKCOIN_PREFIX] && [[touchedNode parent].name hasPrefix:BLOCK_WALLET_PREFIX]) {
            checkDragBlockCoin = TRUE;
            _selectedNode = (SKSpriteNode *)[touchedNode parent];
        }
        
        else if ([touchedNode.name hasPrefix:BLOCK_WALLET_PREFIX]) {
            checkDragBlockCoin = TRUE;
            _selectedNode = touchedNode;
        }
         */



        if (checkDragBlockCoin) {
            block * blockObject = (block *) _selectedNode;
           // [blockObject setBundleCollisionInteraction];
           // _selectedNode = blockObject;
            [[menuContainer singleton] setBlockInfo:blockObject];
        }
        else if (checkDragWallet) {
          
        }
        else {
          // 2. User touched some object that is not a block.
          _selectedNode = nil;
        }
	}

}
    

/**
 *  @tapGesture
 */
-(void) tapGesture:(UITapGestureRecognizer *) recognizer {

    if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        touchLocation = [self convertPointFromView:touchLocation];
        [self selectBlockForTap:touchLocation];
    }
    
}


/**
 *
 */
-(void) doubleTapGesture:(UITapGestureRecognizer *) recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        touchLocation = [self convertPointFromView:touchLocation];
        [self selectBlockForDoubleTap:touchLocation:@"doubletap"];
    }
    
}




/**
 *  @longPressGesture
 */
-(void) longPressGesture: (UILongPressGestureRecognizer *) recognizer {
    
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
       
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        touchLocation = [self convertPointFromView:touchLocation];
        [self selectBlockForDoubleTap:touchLocation:@"longpress"];
    }
}


/**
 *  @Gesture recognizers: pan gesture (DRAG)
 */
- (void)panGesture:(UIPanGestureRecognizer *)recognizer {
    
  //  if (!gameIsFrozen) {
    
        // DRAG -- STARTING
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            CGPoint touchLocation = [recognizer locationInView:recognizer.view];
            touchLocation = [self convertPointFromView:touchLocation];
            
            [self selectBlockForDrag:touchLocation];
            
            // I want to drag the coin
            if ([[_selectedNode name] hasPrefix:WALLET_PREFIX]) {
                currentActiveWallet = (wallet *) [_selectedNode parent];
                [currentActiveWallet popupCoin];
                _selectedNode = [currentActiveWallet getCoin];
                
            }
            
            if ([[_selectedNode name] hasPrefix:BLOCK_BUCKET_PREFIX]) {
                
                if (HEAT_MAP_DISPLAY) {
                    block * coin = (block *) _selectedNode;
                    if ([coin isBitCoinBlock]) {
                        HEAT_MAP_DRAG_BITCOIN_DISPLAY = TRUE;
                        [_gameContainer displayInstanceHeatMap:coin];
                    }
                }
                else {
               
                }
            }
        }
    
    
        // CHANGING DRAG -- MOVING AROUND
        else if (recognizer.state == UIGestureRecognizerStateChanged) {

             if ([[_selectedNode name] hasPrefix:BLOCK_BUCKET_PREFIX]) {
                 CGPoint translation = [recognizer translationInView:recognizer.view];
                 translation = CGPointMake(translation.x, -translation.y);
                 [self panForTranslation:translation];
                 [recognizer setTranslation:CGPointZero inView:recognizer.view];
                 
                 if (blockCollidesWithBidLine) {
                     block * coin = (block *) _selectedNode;
                     SKShapeNode * bidLine = [[interSpreadZone singleton] getBidLine];
                     CGPoint bidLinePoint = [self convertPoint:bidLine.position fromNode:bidLine];
                     float topPoint = bidLinePoint.y + bidLine.frame.size.height/2;
                     float bottomPoint = bidLinePoint.y - bidLine.frame.size.height/2;
                     CGPoint coinPosition = [self convertPoint:coin.position fromNode:coin];
                     [coin continueBidMode:topPoint :bottomPoint: coinPosition.y];
                 }

             }
            
             else if ([[_selectedNode name] hasPrefix:BLOCK_WALLET_PREFIX]) {
                 CGPoint translation = [recognizer translationInView:recognizer.view];
                 translation = CGPointMake(translation.x, -translation.y);
                 [self panForTranslation:translation];
                 [recognizer setTranslation:CGPointZero inView:recognizer.view];
             }
        }
    
    
        // ENDED DRAG ------
        else if (recognizer.state == UIGestureRecognizerStateEnded) {
  
            if ([[_selectedNode name] hasPrefix:BLOCK_BUCKET_PREFIX]) {
                
                if(HEAT_MAP_DRAG_BITCOIN_DISPLAY && HEAT_MAP_DISPLAY) {
                    HEAT_MAP_DRAG_BITCOIN_DISPLAY = FALSE;
                    //block * coin = (block *) _selectedNode;
                    HEAT_MAP_DISPLAY = FALSE;
                    [_gameContainer displayHeatmap];
                }
                
                if (blockBucketCoinCollidesWallet) {
                    [self tryAddBlockCoinToWallet];
                }
                
                else if (blockCollidesWithBidLine) {
                    
                    blockCollidesWithBidLine = FALSE;
                    block * coin = (block *) _selectedNode;
                    
                    if ([coin isMoneyBlock]) {
                        
                       // [_moneyBucket arrangeBlocks];
                        /*
                        if ([[interSpreadZone singleton] canMakeBid:coin]) {
                            [_moneyBucket removeCoinFromBucket:coin];
                            [coin setBucketBlock:FALSE];
                            [coin setName:BIDLINE_COIN_PREFIX];
                            [[interSpreadZone singleton] attachMoneyTrade:coin:_moneyBucket];
                        }
                        
                        else {
                            [_moneyBucket arrangeBlocks];
                            //[[gameEventZone singleton] displayInfoTime:@"Price :;]
                           
                        }
                         */
                 
                    }
                    
                    else {
                        
                        if (interspreadOn == FALSE) {
                            interspreadOn = TRUE;
                            [_bitcoinBucket removeCoinFromBucket:coin];
                            [coin setBucketBlock:FALSE];
                            [coin setBidLineBlock:TRUE :@"bitcoin"];
                            [coin setName:BIDLINE_COIN_PREFIX];
                            [[interSpreadZone singleton] generateInterBid:coin:_bitcoinBucket];
                        }
                    }
                }
                
                /** Bucket coin move animation to dropline coin **/
                else if (askCoinCollidesWithDropLineCoin) {
                    
                    CGPoint point = CGPointMake(askBucketCoin.frame.origin.x, askBucketCoin.frame.origin.y);
                    CGPoint back_point = [askDropLineCoin convertPoint:point toNode:askBucketCoin];
                    
                    float position_x;
                    float position_y;
                    
                    if ([askBucketCoin isBitCoinBlock]) {
                        position_x = back_point.x;// - [[askDropLineCoin getQuantityLongBar] childNodeWithName:@"barInside"].frame.size.width + (askBucketCoin.frame.size.width/2);
                        position_y = back_point.y;// - [[askDropLineCoin getQuantityLongBar] childNodeWithName:@"barInside"].frame.size.height + (askBucketCoin.frame.size.height/2);
                    }
                    else {
                        position_x = back_point.x;//- [[askDropLineCoin getQuantityLongBar] childNodeWithName:@"barInside"].frame.size.width + (askBucketCoin.frame.size.width/2);
                        position_y = back_point.y;// - [[askDropLineCoin getQuantityLongBar] childNodeWithName:@"barInside"].frame.size.height + (askBucketCoin.frame.size.height/2);
                    }
                    
                    askCoinCollidesWithDropLineCoin = FALSE;
                    [askBucketCoin removeCollisionInteraction];
                    [askBucketCoin runAction:[SKAction moveTo:CGPointMake(position_x, position_y) duration:1] completion:^ {
                        [askBucketCoin runAction:[SKAction scaleTo:0 duration:0.8] completion:^ {
                            
                            if ([askBucketCoin isMoneyBlock]) {
                                [_moneyBucket removeCoinFromBucket:askBucketCoin];
                            }
                            else {
                                [_bitcoinBucket removeCoinFromBucket:askBucketCoin];
                            }
                            [askDropLineCoin addOneCoin:askBucketCoin];
                        }];
                    }];
                }

                else {
                   [self tryBucketDropLineTrade];
                }
            }
            
            else if ([[_selectedNode name] hasPrefix:BLOCK_WALLET_PREFIX]) {

                if (coinWalletCollidesBucket) {
                    if ([currentActiveBucket bucketIsFull]) {
                        [[currentActiveBucket getBucketWallet] fadeoutCoin];
                        [[gameEventZone singleton] displayInfoTime:@"Trade area is already full.":1.5];
                    }
                    else {
                        [currentActiveBucket addWalletCoin:currentBlockWalletCoin];
                        [[gameEventZone singleton] displayInfoTime:@"Adding coin.":1];
                    }
                }
                
                else {
                    [currentActiveWallet fadeoutCoin];
                }
                
                currentActiveBucket = nil;
                currentBlockWalletCoin =nil;
               
            }
            
            
            
            
        } // Drag end recognizer
    

}


/**
 *  @tryAddBlockCoinToWallet
 */
-(void) tryAddBlockCoinToWallet {
    
    if (currentBlockBucketCoin != nil ) {
        [currentActiveWallet addCoin:currentBlockBucketCoin];
        [[gameEventZone singleton] displayInfoTime:@"Coin added to wallet":1];
    }
    
    else {
        NSLog(@"Some problem occurred with tryaddblockCoinToWallet");
    }
    
    blockBucketCoinCollidesWallet = FALSE;
    currentActiveWallet = nil;
    currentBlockBucketCoin = nil;
    
}


/**
 *  @Main operation to do trade
 */
-(void) tryBucketDropLineTrade {

    block * blockBucketObject = (block *) _selectedNode;
    blockBucket * tempBlockBucket = (blockBucket *) [blockBucketObject parent];
    
    blockBucket * targetBlockBucket;
    blockBucket * sourceBlockBucket;
    
    if ([blockBucketObject isBitCoinBlock]) {
        sourceBlockBucket = _bitcoinBucket;
        targetBlockBucket = _moneyBucket;
    }
    else {
        sourceBlockBucket = _moneyBucket;
        targetBlockBucket = _bitcoinBucket;
    }
    

    if ([targetBlockBucket bucketIsFull]) {
        targetBlockBucketIsFull = 1;
    }
    else {
        targetBlockBucketIsFull = 0;
    }
    
    
    
    /**
     *  Ok !
     *  There's a trade to be done. Just need to see if there's any remaining blocks.
     */
    if (blockCollidesWithDropLineBucket && [currentActiveDropLineBucket hasDropLineBlocksToTrade]) {
        
        //[blockObject doTrade];
        //if ([blockBucketObject isIncompleteBlock]) {
        //    [[gameEventZone singleton] displayInfo:@"Cannot trade incomplete bitcoin blocks."];
        //}
        //else {
            [currentActiveDropLineBucket doTotalDropLineTrade:sourceBlockBucket:blockBucketObject:targetBlockBucket];
            // Note: blockobject already self removed itself: just refresh it's children trade blocks values
            [tempBlockBucket refreshTradeStatusRemove:blockBucketObject];
            [[gameEventZone singleton] displayInfo:@"Trade in progress..."];
 
        //}
        blockCollidesWithDropLineBucket = FALSE;
    }
    
    // Put it back and dont do nothing.
    else {
        [currentActiveDropLineBucket resetDropLineTrade];
        [tempBlockBucket arrangeBlocks];
    }
}


/**
 *  @coinCollideWithBidLine
 *  @ Collision
 */
-(void) coinCollideWithBidLine : (SKShapeNode *) bidLine : (SKNode *) coin_p : (SKPhysicsContact *) contact : (NSString *) operation {
    
    block * coin = (block *) coin_p;
    
    if ([coin isBitCoinBlock] && interspreadOn == FALSE) {

        if ([operation isEqualToString:@"start"]) {
   
            // Get the point of the bidline converted to the main scene.
            CGPoint bidLinePoint = [self convertPoint:bidLine.position fromNode:bidLine];
            float topPoint = bidLinePoint.y + bidLine.frame.size.height/2;
            float bottomPoint = bidLinePoint.y - bidLine.frame.size.height/2;
            float currentContactPoint = contact.contactPoint.y;
        
            // Rounds up to zero basis to get the proper coin price.
            [coin setBidMode:topPoint:bottomPoint:currentContactPoint];
            blockCollidesWithBidLine = TRUE;
            [[interSpreadZone singleton] switchInterSpreadDisplay:true];

        }
    
        else if ([operation isEqualToString:@"finish"]) {
            blockCollidesWithBidLine = FALSE;
            [coin dropBidMode];
            [[interSpreadZone singleton] switchInterSpreadDisplay:false];
        }
    }
    
    
    /*
    else if ([coin isMoneyBlock] && blockCollidesWithBidLine) {
        NSLog(@"Means a trade is on it's way");
         if ([operation isEqualToString:@"start"]) {
             [[gameEventZone singleton] displayInfo:@"Bid for bitcoin ?"];
             moneyBlockCollidesWithBidLine = TRUE;
         }
         else if ([operation isEqualToString:@"finish"]) {
             moneyBlockCollidesWithBidLine = FALSE;
         }
    }
     */
}



/**
 *  @coinCollideWithDropLineBucketCoin  
 *  @ INVERTED TRADE
 */
-(void) coinCollideWithDropLineBucketCoin : (SKNode *) dropLineCoin : (SKNode *) bucketCoin : (NSString *) operation {
    
    block * askBucketCoin_temp = (block *) bucketCoin;
    block * askDropLineCoin_temp = (block *) dropLineCoin;
    
    BOOL tradePossible = FALSE;
    BOOL isMoneyBid = FALSE;
    
    //NSLog(@"[tempBucket_temp isBitCoinBucket] : %@ : %@", [dropLineCoin name], [bucketCoin name]);
    
    if ([askBucketCoin_temp isBitCoinBlock] && [askDropLineCoin_temp isBitCoinBlock]) {
        tradePossible = TRUE;
    }
    
    else if ([askBucketCoin_temp isMoneyBlock] && [askDropLineCoin_temp isMoneyBlock]) {
        tradePossible = TRUE;
        isMoneyBid = TRUE;
    }

   
    if (tradePossible) {
        
        if (askCoinCollidesWithDropLineCoin) {
            [askDropLineCoin resetPulse];
        }
    
        askCoinCollidesWithDropLineCoin = TRUE;
        askBucketCoin = (block *) askBucketCoin_temp;
        askDropLineCoin = (block *) askDropLineCoin_temp;
        
        if ([operation isEqualToString:@"start"]) {
            float dropLineTradeValue = [askDropLineCoin getTradeInstanceValue];
             NSMutableString * question;
            
            if (isMoneyBid) {
                question = [NSMutableString stringWithFormat:@"Add Coin Bid of %.2f ?", dropLineTradeValue];
            }
            
            else {
                question = [NSMutableString stringWithFormat:@"Add Bitcoin Ask of %.2f ?", dropLineTradeValue];
            }
            
            [askBucketCoin showPotentialProfit:[askDropLineCoin getTradeInstanceValue]];
            [askDropLineCoin pulseBlockForever];
            [[gameEventZone singleton] displayInfo:question];

        }
        
        if ([operation isEqualToString:@"finish"]) {
            [askBucketCoin hidePotentialProfit];
            [askDropLineCoin resetPulse];
            [[gameEventZone singleton] removeInfo];
            askCoinCollidesWithDropLineCoin = FALSE;
        }

  }
    
}



/**
 *  @blockCoinCollideWithWallet
 */
-(void) blockCoinCollideWithWallet : (SKNode *) wallet_p : (SKNode *) blockCoin_p : (NSString *) operation {
    
    if ([operation isEqualToString:@"start"]) {
        
        block * contactCoin = (block *) blockCoin_p;
        wallet * contactWallet = (wallet *) wallet_p;
        
        if (([contactCoin isBitCoinBlock] && [contactWallet isBitcoinWallet]) || ([contactCoin isMoneyBlock] && [contactWallet isMoneyWallet])) {
            blockBucketCoinCollidesWallet = TRUE;
            [[gameEventZone singleton] displayInfo:@"Add coin to wallet?"];
            currentActiveWallet = (wallet *)wallet_p;
            currentBlockBucketCoin = (block *) blockCoin_p;
        }
        
        
    }
    
    if ([operation isEqualToString:@"finish"]) {
        blockBucketCoinCollidesWallet = FALSE;
        [[gameEventZone singleton] removeInfo];
        currentActiveWallet = nil;
        currentBlockBucketCoin =nil;
    }
    
}


/**
 *  @walletBlockCoinCollideWithBlockBucket
 */
-(void) walletBlockCoinCollideWithBlockBucket : (SKNode *) blockWalletCoin_p : (SKNode *) bucket_p : (NSString *) operation {
    
    
    if ([operation isEqualToString:@"start"]) {
        coinWalletCollidesBucket = TRUE;
        [[gameEventZone singleton] displayInfo:@"Add coin for trading?"];
        currentActiveBucket = (blockBucket *)bucket_p;
        currentBlockWalletCoin = (block *) blockWalletCoin_p;
    }
    
    if ([operation isEqualToString:@"finish"]) {
        coinWalletCollidesBucket = FALSE;
        [[gameEventZone singleton] removeInfo];
        currentActiveBucket = nil;
        currentBlockWalletCoin =nil;
    }
    
}

/**
 *  @runHeatMap
 */
-(void) runHeatMap {

    if (HEAT_MAP_DISPLAY == false) {
        HEAT_MAP_DISPLAY = true;
        [[gameEventZone singleton] displayInfoTime:@"Heatmap On" :2];
    }
    
    else {
        HEAT_MAP_DISPLAY = false;
        [[gameEventZone singleton] displayInfoTime:@"Heatmap Off" :2];
    }
}

/**
 * @blockCollideWithDropLineBucket
 *
 */
-(void) blockCollideWithDropLineBucket : (SKNode *) block_p : (SKNode *) dropLineBucket_p : (NSString *) operation{
    
    
    if([block_p isKindOfClass:[block class]]
       && [dropLineBucket_p isKindOfClass:[dropLineBucket class]]) {

        block * focusBlock = (block *) block_p;
        dropLineBucket * dropLineFocusLineBucket = (dropLineBucket *) dropLineBucket_p;
        
        // Trade Operation Possibility.
         if(([focusBlock isBitCoinBlock] && [dropLineFocusLineBucket isMoneyBucket]) || (![focusBlock isBitCoinBlock] && ![dropLineFocusLineBucket isMoneyBucket])) {
            
            if ([operation isEqualToString:@"start"]) {
                blockCollidesWithDropLineBucket = TRUE;
                currentActiveDropLineBucket = dropLineFocusLineBucket;
                [[gameEventZone singleton] displayInfo:@"Trade?"];
            }
            else if ([operation isEqualToString:@"finish"]) {
                blockCollidesWithDropLineBucket = FALSE;
                currentActiveDropLineBucket = nil;
                [[gameEventZone singleton] removeInfo];
            }
             
            if (blockCollidesWithDropLineBucket) {
                //focusBlock blockZoomUpAction];
                
                //if (![focusBlock isIncompleteBlock]) {
                [dropLineFocusLineBucket checkPossibleDropLineTrade:focusBlock];
                //}
                //else {
                 //   [[gameEventZone singleton] displayInfo:@"Cannot trade incomplete bitcoins"];
                //}
                
            }
            
            else {
                [focusBlock hidePotentialProfit];
                [focusBlock blockZoomDownAction];
                [dropLineFocusLineBucket resetDropLineTrade];
            }
             
        }
        
        
        // Bid/ASK
        /*
        else if(([focusBlock isBitCoinBlock] && [dropLineFocusLineBucket isBitcoinBucket]) || (![focusBlock isBitCoinBlock] && ![dropLineFocusLineBucket isBitcoinBucket])) {
             
            if ([operation isEqualToString:@"finish"]) {
                [[gameEventZone singleton] removeInfo];
            }
             
            else if ([operation isEqualToString:@"start"]) {
                [[gameEventZone singleton] displayInfo:@"Bid/Ask?"];
            }

        }
         */
    
        
    }
}

/**
 *  @Takes care of the collision of the bucket with the Block
 *
 */
-(void) bucketCollideWithBlock : (SKNode *) block_p : (SKNode *) blockBucket_p : (NSString *) operation {
    
    if([block_p isKindOfClass:[block class]] && [blockBucket_p isKindOfClass:[blockBucket class]]) {
        
        block * blockObject = (block *) block_p;
        //blockBucket * bucketNode = (blockBucket *) blockBucket_p;

        // Collision IN
        if ([operation isEqualToString:@"start"]) {
            
            if([blockObject isBucketBlock]) {
                
            }
            else {
                
                // 1. Is the bucket the same type as the block
                // 2. Add the block to the collection and fire animation
                [_selectedNode runAction:[[factory singleton] blockZoomUpAction]];
                /*
                if ([[bucketNode getMode] isEqualToString:[blockObject getType]]) {
                    if ([bucketNode addBlockToBucket:blockObject]) {
                        [blockObject setInsideContainer:[bucketNode name]];
                        [_selectedNode runAction:[[factory singleton] blockZoomUpAction]];
                    }
                    
                }
                 */
                
            }
            
        }
        
        
        // Collision OUT
        else if ([operation isEqualToString:@"finish"]) {
            // If i can remove it and it's inside the bucket
            [_selectedNode runAction:[[factory singleton] blockZoomDownAction]];
            /*
            if ([bucketNode removeBlockFromBucket:blockObject.name] ) {
                
                if ([blockObject isInsideBucket]) {
                    [blockObject setInsideContainer:@"null"];
                    [_selectedNode runAction:[[factory singleton] blockZoomDownAction]];
                }
                
            
                
            }
             */
            
        }

    }
    
}

/**
 *  @didBeginContact in view;
 */
- (void)didBeginContact:(SKPhysicsContact *)contact {
    
   
    // 1
    SKPhysicsBody *firstBody, *secondBody;
 
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }

    // Bucket-Block collision
    if ((firstBody.categoryBitMask & bucket_category) != 0 &&
        (secondBody.categoryBitMask & block_category) != 0) {
        
        if ([[secondBody.node name] hasPrefix:BLOCK_WALLET_PREFIX] ) {
            [self walletBlockCoinCollideWithBlockBucket:secondBody.node:firstBody.node:@"start"];
        }
        
    }
    
    // Bucket-Block collision
   /* if ((firstBody.categoryBitMask & block_category) != 0 &&
        (secondBody.categoryBitMask & block_bitcoin_bid_category) != 0) {
        
      // if ([[secondBody.node name] hasPrefix:BLOCK_WALLET_PREFIX] ) {
       //     [self walletBlockCoinCollideWithBlockBucket:secondBody.node:firstBody.node:@"start"];
       // }
        
    }
    
    // Bucket-Block collision
    if ((firstBody.categoryBitMask & block_bitcoin_bid_category) != 0 &&
        (secondBody.categoryBitMask & block_category) != 0) {
        
        // if ([[secondBody.node name] hasPrefix:BLOCK_WALLET_PREFIX] ) {
        //     [self walletBlockCoinCollideWithBlockBucket:secondBody.node:firstBody.node:@"start"];
        // }
        
    }
    */
    
    
    // Block-Wallet collision
    else if ((firstBody.categoryBitMask & block_category) != 0 &&
        (secondBody.categoryBitMask & wallet_money_category) != 0) {
        if ([[firstBody.node name] hasPrefix:BLOCK_BUCKET_PREFIX] ) {
            [self blockCoinCollideWithWallet:secondBody.node:firstBody.node:@"start"];
        }
    }
    
    // Dropline Bucket - Block collision
    else if ((firstBody.categoryBitMask & droplinebucket_category) != 0 &&
              (secondBody.categoryBitMask & block_category) != 0) {
        if ([[secondBody.node name] hasPrefix:BLOCK_BUCKET_PREFIX] ) {
            [self blockCollideWithDropLineBucket:secondBody.node :firstBody.node: @"start"];
        }
    }
    
    
    // Block - Dropline collision
    else if ((firstBody.categoryBitMask & block_category ) != 0 &&
             (secondBody.categoryBitMask & dropline_block_category) != 0) {
       [self coinCollideWithDropLineBucketCoin:secondBody.node :firstBody.node: @"start"];
    }
    
    
    // Block -bidline collision
    else if ((firstBody.categoryBitMask & block_category ) != 0 &&
             (secondBody.categoryBitMask & bidline_category) != 0) {
        [self coinCollideWithBidLine:(SKShapeNode *)secondBody.node :firstBody.node:contact: @"start"];
    }
    
    
}


/**
 *  @didEndContact
 */
-(void) didEndContact:(SKPhysicsContact *)contact {
    
    // 1
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    
    else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    

    
    // Bucket-Block collision
    if ((firstBody.categoryBitMask & bucket_category) != 0 &&
        (secondBody.categoryBitMask & block_category) != 0) {
        
        if ([[secondBody.node name] hasPrefix:BLOCK_WALLET_PREFIX] ) {
            [self walletBlockCoinCollideWithBlockBucket:secondBody.node:firstBody.node:@"finish"];
        }
        
    }
    
    // Bucket-Block collision
    else if ((firstBody.categoryBitMask & block_category) != 0 &&
        (secondBody.categoryBitMask & wallet_money_category) != 0) {
        
        if ([[firstBody.node name] hasPrefix:BLOCK_BUCKET_PREFIX] ) {
            [self blockCoinCollideWithWallet:secondBody.node:firstBody.node:@"finish"];
        }
    }

    
    else if ((firstBody.categoryBitMask & droplinebucket_category) != 0 &&
             (secondBody.categoryBitMask & block_category) != 0) {
        [self blockCollideWithDropLineBucket:secondBody.node :firstBody.node: @"finish"];
    }
    
    
    else if ((firstBody.categoryBitMask & block_category ) != 0 &&
             (secondBody.categoryBitMask & dropline_block_category) != 0) {
        [self coinCollideWithDropLineBucketCoin:secondBody.node :firstBody.node: @"finish"];
    }
    
    // Block -bidline collision
    else if ((firstBody.categoryBitMask & block_category ) != 0 &&
             (secondBody.categoryBitMask & bidline_category) != 0) {
        [self coinCollideWithBidLine:(SKShapeNode *) secondBody.node :firstBody.node:contact: @"finish"];
    }

}


/**
 *  @loadInitialMoneyDeposit
 */
-(void) loadInitialMoneyDeposit {

    int position_x = self.frame.size.width/2;
    int position_y = self.frame.size.height/2 + 50;
    
    // Container
    iFrame = [[SKSpriteNode alloc]init];
    [iFrame setSize:CGSizeMake(self.frame.size.width/3, self.frame.size.height/3)];
    [iFrame setColor:SLICK_BLACK];
    [iFrame setName:@"moneyDepositFrame"];
    [iFrame setPosition:CGPointMake(position_x, self.frame.origin.y + self.frame.size.height)];
    [iFrame setHidden:false];
    
    // Elements
    SKLabelNode * infoLabel = [SKLabelNode labelNodeWithFontNamed:SLICK_OPTIMA_BOLD];
    infoLabel.text = [NSString stringWithFormat:@"Initial deposit $60K"];
    [infoLabel setColor:TEXT_COLOR_GRAY];

   
    // money Symbol
    
    SKSpriteNode * moneySymbol = [SKSpriteNode spriteNodeWithImageNamed:@"dollar_sign.png"];
    [moneySymbol setSize:CGSizeMake(128, 128)];
    [moneySymbol setAlpha:0.2];
    float moneySymbol_y = 0 + iFrame.frame.size.height/5;
    [moneySymbol setPosition:CGPointMake(0, moneySymbol_y)];
    
    
    // Deposit money button.
    // SKSpriteNode * depositMoney = [SKSpriteNode spriteNodeWithImageNamed:@"start_button.png"];
    // depositMoney.name = @"depositMoneyButton";//how the node is identified later
    // [depositMoney setSize:CGSizeMake(200, 60)];
    depositMoney = [SKShapeNode node];
    [depositMoney setPath:CGPathCreateWithRoundedRect(CGRectMake(-125, -15, 250, 50), 4, 4, nil)];
    [depositMoney setName:@"depositMoneyButton"];
    depositMoney.fillColor = SLICK_BLACK;
    depositMoney.strokeColor = [UIColor whiteColor];
    depositMoney.antialiased = true;
    depositMoney.zPosition = 400;
    
   
    depositMoneyLabel = [[SKLabelNode alloc]init];
    depositMoneyLabel.name = @"depositMoneyButton";
    depositMoneyLabel.text = @"Start";
    depositMoneyLabel.position = CGPointMake(0, 0);
    depositMoneyLabel.fontColor = [UIColor whiteColor];
    depositMoneyLabel.fontName = SLICK_OPTIMA_BOLD;
    depositMoneyLabel.zPosition = 400;
    [depositMoney addChild:depositMoneyLabel];
    
    


    // Add stuff.
    [iFrame addChild:moneySymbol];
    [iFrame addChild:depositMoney];
    [iFrame addChild:infoLabel];
    [iFrame setAlpha:0];
    [self addChild:iFrame];
    
    // set positions after adding.
    float depositMoney_y = 0 - iFrame.frame.size.height/3;
    [depositMoney setPosition:CGPointMake(0, depositMoney_y)];
    
    SKAction * fadeIn = [SKAction fadeInWithDuration:1.2];
    SKAction * moveDown = [SKAction moveToY:position_y duration:1.2];
    SKAction * scrollGroup = [SKAction group:@[fadeIn,moveDown]];

    [iFrame runAction:scrollGroup];
    

}


/**
 *  @depositMoneyAction
 */
-(void) startGameAction {
    
    SKAction * fadeOut = [SKAction fadeOutWithDuration:0.5];
    [iFrame runAction:fadeOut completion:^ {
        [_moneyBucket playWalletAnimation];
        [iFrame removeFromParent];
        //[self addBlinkPoint];
        [[menuContainer singleton] displayMenuSection:@"menuButton_performance"];

    }];
    
    
    [self fadeInContainers];
}






/**
 *  @loadContainers
 */
-(void) loadContainers {
    
    _gameContainer = [[gameContainer singleton] initialSetup:self];
    _menuContainer = [[menuContainer singleton] initialSetup:self];
    
    // Top drop line bucket. (Top)
    _topDropLineBucket = [[dropLineBucket alloc] init];
    [_topDropLineBucket initialSetup:@"topdroplinebucket":_gameContainer];
    [_topDropLineBucket setUpPosition];
    
    // Bottom Line bucket (Bottom).
    _bottomDropLineBucket = [[dropLineBucket alloc] init];
    [_bottomDropLineBucket initialSetup:@"topdroplinebucket":_gameContainer];
    [_bottomDropLineBucket setDownPosition];
    
    // Money Bucket.
    _moneyBucket = [[blockBucket alloc]init];
    [_moneyBucket setup:BUCKET_MONEY_NAME:_gameContainer];
    [_moneyBucket setMode:MONEY_BUCKET_ID];
    
    // Bitcoin Bucket.
    _bitcoinBucket = [[blockBucket alloc]init];
    [_bitcoinBucket setup:BUCKET_BITCOIN_NAME :_gameContainer];
    [_bitcoinBucket setMode:BITCOIN_BUCKET_ID];
    
    
    // Game Container.
    [_gameContainer addChild:_moneyBucket];
    [_gameContainer addChild:_bitcoinBucket];
    [_gameContainer addChild:_topDropLineBucket];
    [_gameContainer addChild:_bottomDropLineBucket];
    
    
    // SetupGame Event Zone
    [_gameContainer addChild:[gameEventZone singleton]];
    [[gameEventZone singleton] setupGameEvent];
    
    // Setup InterSpreadZone
    
    [_gameContainer addChild:[interSpreadZone singleton]];
    [[interSpreadZone singleton] setupInterSpread];
    
    
   
    _topDropLineBucket.hidden = TRUE;
    _bottomDropLineBucket.hidden = TRUE;
    _bitcoinBucket.hidden = TRUE;
    [interSpreadZone singleton].hidden = TRUE;
    
    //_menuContainer.hidden = TRUE;
    
    _globalBitcoinBucket = _bitcoinBucket;
    _globalMoneyBucket = _moneyBucket;
    
    _globalBitcoinDropLineBucket = _topDropLineBucket;
    _globalMoneyDropLineBucket = _bottomDropLineBucket;

    [self addChild:_gameContainer];
    [self addChild:_menuContainer];
    [self setContainersZIndex];
    
}


/**
 *  @fadeContainers
 */
-(void) fadeInContainers {
    
    SKAction * delay = [SKAction waitForDuration:2];
    SKAction * delay_2 = [SKAction waitForDuration:3];
    SKAction * fadeIn = [SKAction fadeInWithDuration:1];
    SKAction * fadeInToAlpha = [SKAction fadeAlphaTo:0.5 duration:1];
    
    [_topDropLineBucket setAlpha:0];
    [_bottomDropLineBucket setAlpha:0];
    [_bitcoinBucket setAlpha:0];
    [[interSpreadZone singleton] setAlpha:0];
    
    _topDropLineBucket.hidden = FALSE;
    _bottomDropLineBucket.hidden = false;
    _bitcoinBucket.hidden = FALSE;
    [interSpreadZone singleton].hidden = FALSE;

    SKAction *sequence = [SKAction sequence:@[delay, fadeIn]];
    SKAction *sequence_2 = [SKAction sequence:@[delay_2, fadeIn]];
    SKAction *sequence_3 = [SKAction sequence:@[delay_2,fadeInToAlpha]];
    

    [_bitcoinBucket runAction:sequence];
    [_topDropLineBucket runAction:sequence_2];
    [_bottomDropLineBucket runAction:sequence_2];
    [[interSpreadZone singleton] runAction:sequence_3];
    [_menuContainer setPerfomanceBar];
    //[_gameContainer loadIndicators];

}


/**
 *
 */
-(void) setContainersZIndex {
    
    _gameContainer.zPosition = 0;
    _topDropLineBucket.zPosition = 1;
    _bottomDropLineBucket.zPosition = 1;

    _moneyBucket.zPosition = 2;
    _bitcoinBucket.zPosition = 2;
    
    _menuContainer.zPosition = 0;
    
}




CGPoint mult(const CGPoint v, const CGFloat s) {
	return CGPointMake(v.x*s, v.y*s);
}


/**
 *
 */
- (CGPoint)boundLayerPos:(CGPoint)newPos {
    CGSize winSize = self.size;
    CGPoint retval = newPos;
    retval.x = MIN(retval.x, 0);
    retval.x = MAX(retval.x, -[_gameContainer size].width+ winSize.width);
    retval.y = [self position].y;
    return retval;
}

/**
 *  @panForTranslation
 */
- (void)panForTranslation:(CGPoint)translation {
    
    if (_selectedNode != nil) {
        CGPoint position = [_selectedNode position];
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    
}


/**
 *  @touchesBegan
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    //blockBucket * tempBlockBucket;
    //BOOL walletClicked = false;

    //if fire button touched, bring the rain
    if ([node.name isEqualToString:@"depositMoneyButton"]) {
        [self startGameAction];
        depositMoney.fillColor = [UIColor whiteColor];
        depositMoneyLabel.color = [UIColor blackColor];

    }
    
    //if fire button touched, bring the rain
    else if ([node.name hasPrefix:@"menuButton"]) {
        [[menuContainer singleton] displayMenuSection:node.name];
    }

     /**
    else if ([node.name isEqualToString:BITCOIN_WALLET_NAME]) {
        tempBlockBucket = _bitcoinBucket;
        walletClicked = true;
    }
    
    else if ([node.name isEqualToString:MONEY_WALLET_NAME]) {
        tempBlockBucket = _moneyBucket;
        walletClicked = true;
    }
    
  
   
    if (walletClicked) {
        if ([[tempBlockBucket getBucketWallet] isWalletOpen]) {
            [[tempBlockBucket getBucketWallet] closeWallet];
            gameIsFrozen = false;
        }
        else {
            [[tempBlockBucket getBucketWallet] openWallet];
            gameIsFrozen = true;
        }
        
    }
     **/
    
    else if ([node.name isEqualToString:@"breakWalletLabel"] && [[node parent].name isEqualToString:@"breakWalletButton"]) {
        [[_moneyBucket getBucketWallet] walletBreakSelected:node];
    
    }
    
    else if ([node.name isEqualToString:@"breakWalletButton"]) {
        [[_moneyBucket getBucketWallet] walletBreakSelected:[node childNodeWithName:@"breakWalletLabel"]];
    }

    
}

/**
 *
 */

/*
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
	UITouch *touch = [touches anyObject];
	CGPoint positionInScene = [touch locationInNode:self];
	CGPoint previousPosition = [touch previousLocationInNode:self];
    
	CGPoint translation = CGPointMake(positionInScene.x - previousPosition.x, positionInScene.y - previousPosition.y);
    
	[self panForTranslation:translation];
}
 
 */



/**
 *  @touchedEnded
 */

/*
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
   // CGPoint positionInScene = [touch locationInNode:self];
   
    if([self isBlock:_selectedNode]) {
        
        block * blockObject = (block *) _selectedNode;
        if ([blockObject isInsideBucket]) {
          
            [_selectedNode runAction:[[factory singleton] blockZoomDownAction]];
        }
        
        else {
            
        }
        
        [self eventReleaseBlock:blockObject];
        
         
     }
    

 
}
 */



-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
