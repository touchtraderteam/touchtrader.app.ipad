//
//  dropLineBucket.h
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constant.h"
#import "dropLine.h"
#import "dataManager.h"
#import "gameEventZone.h"


@class block;
@class blockBucket;

@interface dropLineBucket : SKSpriteNode {
    
    NSMutableDictionary * dropLineCollection;
    SKSpriteNode * parentScene;
    UIColor * defaultColor;
    NSMutableArray * initial_horizontal_positions;
    
    int dropLineBucket_width;
    int dropLineBucket_height;
    int currentDropLines;
    
    int dropLineBucket_left_limit;
    int dropLineBucket_right_limit;
    
    
  
    NSString * dropLineBucketType;
    BOOL moneyBucket;
    
    int lastFetchStart;
    
    
    BOOL hitLastDropLine;

}

-(block *) getLowestDropLineBucketCoin;


-(void) runDropLineHeatMap : (block *) targetBlockCoin;
-(void) hideDropLineHeatMap;

-(BOOL) hasDropLineBlocksToTrade;
- (void) resetDropLineTrade;

- (void) doTotalDropLineTrade: (blockBucket *) sourceBlockBucket : (block *) blockBucketObject : (blockBucket *) targetBlockBucket;

- (BOOL) checkDropLineTrade : (block *) blockObject;
- (BOOL) checkPossibleDropLineTrade : (block *) blockObject;


-(BOOL) isBitcoinBucket;
- (BOOL) isMoneyBucket;


- (NSString *) getType;
//-(NSString *) getDropLineBucketType;
//-(BOOL) addDropLine : (dropLine *) dropLine;

-(BOOL) refreshDropLineBucketStatus : (SKSpriteNode *) dropLineObject;

-(void) addDropLines;


- (void) initialSetup : (NSString *) uniqueName : (SKSpriteNode *) parentFrame_p;

-(void) setUpPosition;
-(void) setDownPosition;


@end
