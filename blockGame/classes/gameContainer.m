//
//  block.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "gameContainer.h"
#import <SpriteKit/SpriteKit.h>

@implementation gameContainer {
    
    
    
}

static gameContainer *singleton = nil;

+ (gameContainer *) singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
    
    }
    return singleton;
}

- (id)init {
    if ( (self = [super init]) ) {
        // your custom initialization
        
    }
    return self;
}


/**
 *
 */
- (CGRect) originCoordinates {
    
    
    // Assuming CGAnchor as 0,5: 0,5
    // Note: future anchor point should be calculated automatically
    
    float origin_width = self.frame.size.width;
    float origin_height = self.frame.size.height;

    
    float origin_x = self.frame.origin.x - (origin_width/2);
    float origin_y = self.frame.origin.y - (origin_height/2);
    
    
    return CGRectMake(origin_x, origin_y, origin_width, origin_height);
}



/**
 *
 */
-(void) addBlinkPoint {
    
    CGRect circle = CGRectMake(867, 260, 12, 12);
    SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
    shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
    shapeNode.fillColor = [UIColor colorWithRed:198.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0];
    shapeNode.lineWidth = 1;
    shapeNode.antialiased = YES;
    
    [shapeNode runAction:[self getBlinkAction]];
    [self addChild:shapeNode];
    
}


/**
 *  @getBlinkAction
 */
-(SKAction *) getBlinkAction {
    
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.2 duration:3.0];
    SKAction *fadeIn = [SKAction fadeInWithDuration: 1.5];
    SKAction *pulse = [SKAction sequence:@[fadeOut,fadeIn]];
    
    //SKAction *pulseThreeTimes = [SKAction repeatAction:pulse count:3];
    SKAction * blinkAction = [SKAction repeatActionForever:pulse];
    
    
    return blinkAction;
    
}



/**
 *  @displayBitcoinTrend
 */
-(void) displayBitcoinTrend {
    
    if ([self childNodeWithName:@"bitcointrend"] == nil) {
        // Set Background image
        SKTexture *bitcoinTrendTexture = [SKTexture textureWithImageNamed:@"background_chart.png"];
        SKSpriteNode * bitcoinTrendImage = [SKSpriteNode spriteNodeWithTexture:bitcoinTrendTexture size:self.frame.size];
        bitcoinTrendImage.name = @"bitcointrend";
        [bitcoinTrendImage setAlpha:0];
        bitcoinTrendImage.zPosition = -1;
        
        float position_x = self.frame.origin.x + self.frame.size.width/2;
        float position_y_bottom = self.frame.origin.y + bitcoinTrendImage.frame.size.height/2 + 50;
        [bitcoinTrendImage setPosition:CGPointMake(position_x, position_y_bottom)];
        

        CGRect circle = CGRectMake(867, 260, 12, 12);
        SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
        shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
        shapeNode.fillColor = [UIColor colorWithRed:198.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0];
        shapeNode.lineWidth = 1;
        shapeNode.antialiased = YES;
        shapeNode.zPosition = 10;
        shapeNode.name = @"blinkpoint";
        shapeNode.position = CGPointMake(200, 200);
        
        [shapeNode runAction:[self getBlinkAction]];
        [self addChild:shapeNode];
        [self addChild:bitcoinTrendImage];

    }
        
    if ([self childNodeWithName:@"bitcointrend"].alpha == 0) {
        [[self childNodeWithName:@"bitcointrend"] runAction:[SKAction fadeAlphaTo:1 duration:0.2]];
    }
    
    else {
        [[self childNodeWithName:@"bitcointrend"] runAction:[SKAction fadeAlphaTo:0 duration:0.2]];
    }
    
}


/**
 *  @loadIndicators
 */
-(void) loadIndicators  {
    
    [self displayBidAskGraphic];
    [self displayBitcoinTrend];
}



/**
 *  @displayInstanceHeatMap
 */
-(void) displayInstanceHeatMap : (block *) coin {
    
    [_globalBitcoinBucket closeBlockBucketHeatMap];
    [[_globalBitcoinBucket getBucketWallet] hideHeatMap];
    
    [_globalMoneyDropLineBucket runDropLineHeatMap:coin];
    [_globalBitcoinDropLineBucket runDropLineHeatMap:coin];
}

/**
 *  @displayHeatMap
 */
-(void) displayHeatmap {
    
   // block * targetBlockCoin = [_globalBitcoinBucket getLowestBitcoin];
    block * targetBlockCoin = [_globalMoneyDropLineBucket getLowestDropLineBucketCoin];
    //[targetBlockCoin showHeatMapBlockBucket:0];
    //[targetBlockCoin hideHeatMap];
    
    if (HEAT_MAP_DISPLAY == false) {
        
        HEAT_MAP_DISPLAY = true;
        
        if (targetBlockCoin == nil) {
           [[gameEventZone singleton] displayInfoTime:@"No Bitcoin to Map" :2];
        }
        
        else {
            [[gameEventZone singleton] displayInfo:@"Heatmap On"];
            [_globalBitcoinBucket runBlockBucketHeatMap:targetBlockCoin];
            [[_globalBitcoinBucket getBucketWallet] runHeatMap:targetBlockCoin];
            
            [_globalMoneyDropLineBucket hideDropLineHeatMap];
            [_globalBitcoinDropLineBucket hideDropLineHeatMap];
            
            //[_globalMoneyDropLineBucket runDropLineHeatMap:targetBlockCoin];
            //[_globalBitcoinDropLineBucket runDropLineHeatMap:targetBlockCoin];
        }
    }
    
    else {
        HEAT_MAP_DISPLAY = FALSE;
        HEAT_MAP_DRAG_BITCOIN_DISPLAY = FALSE;
        [_globalMoneyDropLineBucket hideDropLineHeatMap];
        [_globalBitcoinDropLineBucket hideDropLineHeatMap];
        [[_globalBitcoinBucket getBucketWallet] hideHeatMap];
        //[targetBlockCoin hideHeatMap];
        [_globalBitcoinBucket closeBlockBucketHeatMap];
        [[gameEventZone singleton] displayInfoTime:@"HeatMap OFF" :3];
    }
    
}


/**
 *  @buildBidAskGraphic
 */
-(void) buildBidAskGraphic {
    
    // Set Bottom ask trend image
    SKTexture *bottomAskTrend = [SKTexture textureWithImageNamed:@"bottom_ask_trend.png"];
    SKSpriteNode * bottomAskTrendImage = [SKSpriteNode spriteNodeWithTexture:bottomAskTrend size:CGSizeMake(1100, 234)];
    
    bottomAskTrendImage.name = @"bottomTrend";
    bottomAskTrendImage.alpha = 0;
    [self addChild:bottomAskTrendImage];
    [bottomAskTrendImage setAlpha:0];
    bottomAskTrendImage.zPosition = -1;
    
    float position_x = self.frame.origin.x + self.frame.size.width/2 - 183;
    float position_y_bottom = self.frame.origin.y +  bottomAskTrendImage.frame.size.height;
    float position_y_up = self.frame.origin.y + self.frame.size.height;
    //[bottomAskTrendImage setPosition:CGPointMake(self.frame.origin.x - 183, 0 - self.frame.size.height/3 - 50)];
    [bottomAskTrendImage setPosition:CGPointMake(position_x, position_y_bottom)];
                                                 
    // Set Top bid trend image
    SKTexture *topAskTrend = [SKTexture textureWithImageNamed:@"top_bid_trend.png"];
    SKSpriteNode * topAskTrendImage = [SKSpriteNode spriteNodeWithTexture:topAskTrend size:CGSizeMake(1100, 234)];
    [self addChild:topAskTrendImage];
    
    topAskTrendImage.alpha = 0;
    [topAskTrendImage setAlpha:0];
    topAskTrendImage.zPosition = -1;
    topAskTrendImage.name = @"topTrend";
    
    [topAskTrendImage setPosition:CGPointMake(position_x, position_y_up)];
    //[topAskTrendImage setPosition:CGPointMake(self.frame.origin.x - 178, 0 + self.frame.size.height/3 +50)];

    INDICATOR_BID_ASK_BUILD = TRUE;
    
}

/**
 *  @displayBidAskGraphic
 */
-(void) displayBidAskGraphic {
    
    if (INDICATOR_BID_ASK_BUILD == false) {
        [self buildBidAskGraphic];
    }
    
    // show
    
    
    if ([self childNodeWithName:@"bottomTrend"].alpha == 0) {
        
        SKAction * fadeGraphics = [SKAction fadeAlphaTo:0.6 duration:0.2];
        [[self childNodeWithName:@"bottomTrend"] runAction:fadeGraphics];
        [[self childNodeWithName:@"topTrend"] runAction:fadeGraphics];

    }
    
    else {
        SKAction * fadeGraphics = [SKAction fadeAlphaTo:0 duration:0.2];
        [[self childNodeWithName:@"bottomTrend"] runAction:fadeGraphics];
        [[self childNodeWithName:@"topTrend"] runAction:fadeGraphics];
    }

}


/**
 *  @initialSetup
 */
-(gameContainer *) initialSetup: (SKScene *) parentFrame {
    
    

  
    int background_width = parentFrame.size.width;
    int background_height = parentFrame.size.height; //- (parentFrame.size.height/6)

    uniqueName = @"gamecontainer";
    defaultSize = CGSizeMake(background_width,background_height);
    
    //setPosition is according to the parent frame
    [self setName:uniqueName];
    [self setSize:defaultSize];
    self.anchorPoint =  CGPointMake(0,0);
    [self setName:uniqueName];
    
 
    
    int start_x = parentFrame.frame.origin.x;
    int start_y = parentFrame.frame.origin.y - MAIN_MENU_HEIGHT - 20;


    [self setPosition:CGPointMake(start_x ,start_y)];
    
    // Set Background image
    SKTexture *backgroundImageTexture = [SKTexture textureWithImageNamed:@"background_mainimage.png"];
    SKSpriteNode * backgroundMainImage = [SKSpriteNode spriteNodeWithTexture:backgroundImageTexture size:CGSizeMake(self.frame.size.width, self.frame.size.height)];
    [self addChild:backgroundMainImage];
    [backgroundMainImage setAlpha:0.8];
    backgroundMainImage.zPosition = -2;
    [backgroundMainImage setPosition:CGPointMake(self.frame.origin.x + self.frame.size.width/2, self.frame.origin.y + self.frame.size.height/2 + 100)];
    
    
    return self;
    
}



/**
 *
 */
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
 

}




@end
