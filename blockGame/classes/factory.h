//
//  factory.h
//  blockGame
//
//  Created by daniel.oliveira on 19/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constant.h"
#import "block.h"

@interface factory : NSObject{
    
    
}

// Unique singleton
+ (factory *)singleton;

/**
 * @ Generates an unique random number
 */


- (SKLabelNode *) buildBlockLabel : (NSString *) stringName_p : (CGPoint) position_p : (UIColor *) label_font_color;
- (SKSpriteNode *) buildBlock : (CGRect) parentFrame : (NSString *) uniqueName : (CGPoint) point;

- (SKSpriteNode *) buildGameContainer : (CGRect) parentSize;
- (SKSpriteNode *) buildBucket : (CGRect) parentFrame : (NSString *) uniqueName;

/**
 *  @ SpriteNodes Generators
 */
- (SKLabelNode *) buildLabel : (NSString *) stringName_p : (CGPoint) position_p : (UIColor *) color;
//- (block *) buildBlock : (NSString *)format_p : (CGRect *) size_p;


- (SKAction *) sequenceAction;
-(SKAction *) blockZoomUpAction;
-(SKAction *) blockZoomDownAction;

/**
 * @ SkActions Generators
 */

- (SKAction *) rotateAction;


- (CGFloat) getRandomNumber : (CGFloat) low : (CGFloat) high;



@end
