//
//  dropLine.m
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "dropLine.h"

@implementation dropLine {
    
    
}


-(id) init {
    self = [super init];
    if(self)
    {
       
    }
    return self;
}


/**
 *
 */
-(void) initialSetup : (SKSpriteNode *) fatherNode : (int) index {
    
    
    defaultColor = DROP_LINE_COLOR;
    defaultSize = CGSizeMake(fatherNode.frame.size.width,DROP_LINE_HEIGHT);
    blockCollection = [[NSMutableDictionary alloc] init];
    initial_horizontal_positions = [[NSMutableArray alloc]init];

    dropLineBucket * tempBucket = (dropLineBucket *) fatherNode;
    isMoneyDropLine = [tempBucket isMoneyBucket];
    
    appendPreviousFullTradeBlockCount = 0;
    appendPreviousFullTradeBlockValue = 0;
    
    dropLineRotateValue = 1.4;
    dropLineRotationTime = 0.2;
    
    if (isMoneyDropLine ) {
        uniqueName = [NSMutableString stringWithFormat:@"dropline_money_%d",index];

    }
    else {
        uniqueName = [NSMutableString stringWithFormat:@"dropline_bitcoin_%d",index];
    }
    
    [self setName:uniqueName];
    //[self setInteraction];
    [self setColor:defaultColor];
    [self setSize:defaultSize];
    
    [self setAnchorPoint:CGPointMake(0.5, 0.5)];
 
    
  
    bitcoin_start_x = 0 + self.frame.size.width/4;
    money_start_x = 0 - self.frame.size.width/4;
    usableDropLineWidth = self.frame.size.width - ((self.frame.size.width/4) * 2);
 
    [self rotateDropLine];

}

/**
 *  @blockDegToRad
 */
float dropLineblockDegToRad(float degree) {
	return degree / 180.0f * M_PI;
}




/**
 *  @checkBlockTrade
 *  This function takes care of checking the collision between a bucket block
 *  and the dropline block.
 *  It will 
 *   1.  Assess the blocks/ partial blocks that can be traded on the dropline from the value of the bucket block.
 *   2.  Graphic events to show which blocks could be traded.
 *
 */
-(float) checkBlockTrade : (float) bucketBlockTradeValue {
    
   NSMutableArray * children = [self getCoinChildren];

    // Counter
    float dropLineBlocksCumulateTradeValue = 0;
    float returnBlockBucketValue = bucketBlockTradeValue;
    int i =0;

    
    lastBlockTradeDifference = 0;
    BOOL dropLineBlockStopsHere = false;
    
    for (i=0; i<[children count]; i++) {
        
        block * tempDropLineBlock = (block *) [children objectAtIndex:i];
        float fullDropLineBlockValue = [tempDropLineBlock getFullQuantityTradeValue];
        [tempDropLineBlock blockHeadCoinZoomUpAction];
        
        dropLineBlocksCumulateTradeValue += fullDropLineBlockValue;
        
        // The cycle continues, we check the other dropline blocker.
        if (returnBlockBucketValue > fullDropLineBlockValue) {
            returnBlockBucketValue -= fullDropLineBlockValue;
        }
            
        else {
            dropLineBlockStopsHere = true;
        }
            
        /**
         *  The bucket block trading value is still bigger than the dropline bid
         */
        if (dropLineBlocksCumulateTradeValue <= bucketBlockTradeValue) {
            [tempDropLineBlock setDoingTrade:TRUE:returnBlockBucketValue];
        }
            
        /**
         *  The bucket block is not bigger than the cumulative dropline block.
         *  My trading stops here.
         */
         else if (dropLineBlockStopsHere) {
            [tempDropLineBlock setDoingTrade:TRUE:returnBlockBucketValue];
            [tempDropLineBlock setLastDoingTrade:TRUE];
            lastBlockTradeDifference = returnBlockBucketValue;
            break;
        }
    
    }
    
    
    [self setDropLineDoingTrade:true];
    [self setColor:[UIColor blueColor]];
    
    if (dropLineBlockStopsHere) {
        return 0;
    }

    return returnBlockBucketValue;
}



/**
 *  @resetBlockTrade
 */
-(void)resetBlockTrade {
    
    NSArray * children = [self children];
    int i;
    
    for (i=0; i<[children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[block class]]) {
            block * tempBlock = (block *) [children objectAtIndex:i];
            if([tempBlock isDoingTrade]) {
              [tempBlock setDoingTrade:FALSE:0];
              [tempBlock setLastDoingTrade:FALSE];
              [tempBlock blockHeadCoinZoomDownAction];
              [tempBlock resetPulse];
     
            }
        }
    }
    
    [self setDropLineDoingTrade:FALSE];

}


/**
 *
 */
-(BOOL) isDropLineDoingTrade {
    return isDropLineDoingTrade;
}


/**
 *
 */
- (BOOL) hasBlocksToTrade {
   
    NSArray * children = [self children];
    int i;
    
    
    for (i=0; i<[children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[block class]]) {
            return true;
        }
    }

    
    return FALSE;
}


/**
 * @setDropLineDoingTrade
 */
-(void) setDropLineDoingTrade : (BOOL) isDropLineDoingTrade_p {
    
    isDropLineDoingTrade = isDropLineDoingTrade_p;
}


/**
 *  @getFirstTradeBlock
 */
- (block *) getFirstTradeBlock {
    
    NSArray * children = [self children];
    int i;
    
    for (i=0; i<[children count]; i++) {
        if ([[children objectAtIndex:i] isKindOfClass:[block class]]) {
            return [children objectAtIndex:i];
            break;
        }
    }
    return nil;
    
}


/**
 *  @doDropLineTrade
 *  This is the main Caller for the trade to be done.
 *
 */
- (void) doDropLineTrade: (blockBucket *) sourceBlockBucket_p : (block *) blockBucketObject_p  : (blockBucket *) targetBlockBucket_p {
    
    // Dropline color.
    [self setColor:[UIColor redColor]];
    
    NSMutableArray * coins = [self getDropLineCoinChildrenOnTrade];
    //NSMutableArray * removeChilds = [[NSMutableArray alloc] init];
    
    sourceBlockBucket_trade = sourceBlockBucket_p;
    targetBlockBucket_trade = targetBlockBucket_p;
    blockBucketObject_trade = blockBucketObject_p;
    
    if ([self isMoneyDropLine]) {
        block * coin = (block *) [coins objectAtIndex:0];
        [coin doPartialMoneyTrade:[blockBucketObject_p getPercentualValue]];
    }
    
    else {
        
        int i;
        float quantityCoinsTraded;
        float quantityCoinsTradedValue;
    
        for (i=0; i<[coins count]; i++) {
        
            block * coin = (block *) [coins objectAtIndex:i];
     
            // f.e. a coin of 3120 x 19 qt
            if ([coin isLastDoingTrade]) {
                quantityCoinsTraded = [coin getNumberOfBlockCoinsBeingTraded];
                quantityCoinsTradedValue = [coin getBlockBeingTradeValue];
                [coin doPartialTrade];
                break;
            }
        
            else {
                [coin doFullTrade];
                //break;
            }
        }
    }
}


/**
 *  @dropLineAppendTrade_callback
 */
- (void) dropLineAppendTrade_callback : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue {
    
    appendPreviousFullTradeBlockCount = numberOfBlocksTraded;
    appendPreviousFullTradeBlockValue = numberOfBlocksTradedValue;
    
}


/**
 *  @callBack from block to finish trade (coin partialTrade).
 */
- (void) dropLineFinishTrade_callback : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue {
    
    // DropLine actions
    [self setDropLineDoingTrade:FALSE];
    [self setColor:defaultColor];
    
    // Remove DropLine if necessary.
    if (![self hasTradeBlocksInside]) {
        if([[self parent] isKindOfClass:[dropLineBucket class]]) {
            dropLineBucket * tempDropLineBucket = (dropLineBucket *) [self parent];
            [tempDropLineBucket refreshDropLineBucketStatus:self];
        }
    }
    
    
    if (appendPreviousFullTradeBlockCount !=0) {
        float newTradeAppendValueBlocks = numberOfBlocksTraded + appendPreviousFullTradeBlockCount;
        float newTradeAppendValue = numberOfBlocksTradedValue+appendPreviousFullTradeBlockValue;
        appendPreviousFullTradeBlockCount= 0;
        appendPreviousFullTradeBlockValue=0;
        [targetBlockBucket_trade addTradeCoins:newTradeAppendValueBlocks:newTradeAppendValue];
    }
    
    else {
        [targetBlockBucket_trade addTradeCoins:numberOfBlocksTraded:numberOfBlocksTradedValue];
    }
    
    NSLog(@"------- THIS IS THE MAIN TRADE");
    
    NSLog(@" numberOfBlocksTraded: %f  numberOfBlocksTradedValue: %f", numberOfBlocksTraded,numberOfBlocksTradedValue );
    
    [sourceBlockBucket_trade blockBucketFinalTradeOperations];
    [targetBlockBucket_trade blockBucketFinalTradeOperations];
    
    // Refresh remaining widgets.
    [[gameEventZone singleton] displayInfoTime:@"Trade Successful.":3];
    [[soundPlayer singleton] playTradeSuccess];
    [[menuContainer singleton] refreshPerfomanceBar];
    
}


/**
 *  @getCoinChildren
 */
-(NSMutableArray *) getCoinChildren {
    
    NSArray * children = [self children];
    NSMutableArray * coinsChildren = [[NSMutableArray alloc] initWithCapacity:[children count]];
    int i;
    
    for (i=0; i<[children count]; i++) {
        if ([[children objectAtIndex:i] isKindOfClass:[block class]]) {
            block * dropLineCoin = (block *) [children objectAtIndex:i];
            [coinsChildren addObject:dropLineCoin];
        }
    }
    
    if ([coinsChildren count] <= 0) {
        NSLog(@"DROPLINE:: coins Children empty");
    }
    
    
    return coinsChildren;
    
}

/**
 *
 */
-(NSMutableArray *) getDropLineCoinChildrenOnTrade {
    
    NSArray * children = [self children];
    NSMutableArray * coinsChildren = [[NSMutableArray alloc] initWithCapacity:[children count]];
    int i;
    
    for (i=0; i<[children count]; i++) {
        if ([[children objectAtIndex:i] isKindOfClass:[block class]]) {
            block * dropLineCoin = (block *) [children objectAtIndex:i];
            if ([dropLineCoin isDoingTrade]) {
                [coinsChildren addObject:dropLineCoin];
            }
        }
    }
    
    if ([coinsChildren count] <= 0) {
        NSLog(@"DROPLINE:: coins Children empty");
    }
    
    
    return coinsChildren;
    
}

/**
 *  @lowerTradeValue
 */
-(float) lowerTradeValue {
    return tradeLabelValue_min;
}


/**
 *  @higherTradeValue
 */
-(float) higherTradeValue {
    return tradeLabelValue_max;
}


/**
 *  @startPointX
 */
- (int) startPointX {
    return dropLine_block_start_x;
}


/**
 *
 */
-(BOOL) hasTradeBlocksInside {
    
    NSArray * children_check = [self children];
    int i;
    
    for (i=0; i<[children_check count]; i++) {
        
        if ([[children_check objectAtIndex:i] isKindOfClass:[block class]]) {
            return TRUE;
        }
        
    }
    return FALSE;
    
}



/**
 *  @attachDropLineTradeValueLabels
 */
-(void) attachDropLineTradeValueLabels {
    
    SKLabelNode *tradeValueLimit_min_label = [SKLabelNode labelNodeWithFontNamed:SLICK_OPTIMA_BOLD];
    SKLabelNode *tradeValueLimit_max_label = [SKLabelNode labelNodeWithFontNamed:SLICK_OPTIMA_BOLD];
    
    //tradeValueLimit_max_label.fontName = SLICK_OPTIMA_BOLD;
    //tradeValueLimit_min_label.fontName = SLICK_OPTIMA_BOLD;
    
    tradeValueLimit_min_label.text =[NSString stringWithFormat:@"$%.f", tradeLabelValue_min];
    tradeValueLimit_max_label.text =[NSString stringWithFormat:@"$%.f", tradeLabelValue_max];
    
    tradeValueLimit_min_label.fontColor = DROPLINE_EXTREME_LABEL_COLOR;
    tradeValueLimit_max_label.fontColor = DROPLINE_EXTREME_LABEL_COLOR;
    
    [tradeValueLimit_min_label setColor:[UIColor whiteColor]];
    [tradeValueLimit_max_label setColor:[UIColor whiteColor]];
    
    tradeValueLimit_max_label.fontSize = 22;
    tradeValueLimit_min_label.fontSize = 22;
    
    float position_x_min = 0;
    float position_x_max = 0;
    
    int extra_spacing = 150;
    
    float padding_space;
    if ([self isMoneyDropLine]) {
        position_x_min = money_start_x - extra_spacing;
        position_x_max = bitcoin_start_x + extra_spacing;
        padding_space = -20;
        
    }
    else {
        position_x_min = bitcoin_start_x + extra_spacing;
        position_x_max = money_start_x - extra_spacing;
        padding_space = 5;
    }
   

    float position_y = self.frame.origin.y;
    
    [tradeValueLimit_min_label setPosition:CGPointMake(position_x_min ,position_y+padding_space)];
    [tradeValueLimit_max_label setPosition:CGPointMake(position_x_max ,position_y+padding_space)];
    
    [self addChild:tradeValueLimit_min_label];
    [self addChild:tradeValueLimit_max_label];

}



/**
 *  @rotateDropLine
 */
- (void) rotateDropLine {
    
    
    float rotateValue = dropLineRotateValue;
    [self setAlpha:0];
    
    if (!isMoneyDropLine) {
        rotateValue = -dropLineRotateValue;
    }
    
    
    SKAction *resetSpin = [SKAction sequence:@[[SKAction rotateByAngle:dropLineblockDegToRad(rotateValue) duration:dropLineRotationTime]]];
    SKAction * fadeIn = [SKAction fadeInWithDuration:1.4];
    
    [self runAction:resetSpin completion:^ {
        [self runAction:fadeIn];
        
    }];
    
}





/**
 *
 */
float dropLineDegToRad(float degree) {
	return degree / 180.0f * M_PI;
}


/**
 *
 */

-(NSString *) getBlockType {
    
    if (isMoneyDropLine) {
        blockType = MONEY_BUCKET_ID;
    }
    
    else {
        
        blockType = BITCOIN_BUCKET_ID;
    }
    
    return blockType;
}


/**
 *  @isMoneyDropLine
 */
-(BOOL) isMoneyDropLine {
    
    return isMoneyDropLine;
}

/**
 *  @isBitcoinDropLine
 */
-(BOOL) isBitcoinDropLine {
    if (isMoneyDropLine == FALSE) {
        return TRUE;
    }
    
    return FALSE;
}



/**
 *  @runDropLineHeatMap
 */
-(void) runDropLineHeatMap: (block *) targetBlockCoin {
    
    if ([self hasBlocksToTrade]) {
        
        NSArray * children = [self children];
        int i;
        
        for (i=0; i<[children count]; i++) {
            
            if ([[children objectAtIndex:i] isKindOfClass:[block class]]) {
                block * tempBlock = (block *) [children objectAtIndex:i];
                
                if ([tempBlock isBitCoinBlock]) {
                    if ([tempBlock getBitcoinTradePercentalValue] > [targetBlockCoin getBitcoinTradePercentalValue]) {
                        [tempBlock showHeatMap:[targetBlockCoin getBitcoinTradePercentalValue]];
                    }
                }
                else {                    
                    if ([tempBlock getBitcoinTradePercentalValue] > [targetBlockCoin getBitcoinTradePercentalValue]) {
                        [tempBlock showHeatMap:[targetBlockCoin getBitcoinTradePercentalValue]];
                    }
                }
                //[tempBlock showPotentialProfit:[targetBlockCoin getBitcoinTradePercentalValue]];
                
            }
        }
 
    }
}


/**
 *
 */
-(void) hideDropLineHeatMap {
    if ([self hasBlocksToTrade]) {
        
        NSArray * children = [self children];
        int i;
        
        for (i=0; i<[children count]; i++) {
            if ([[children objectAtIndex:i] isKindOfClass:[block class]]) {
                block * tempBlock = (block *) [children objectAtIndex:i];
                [tempBlock hideHeatMap];
            }
        }
    }
}

/**
 *  @Gets the blocks from data and builds them on the dropline
 */
//-(void) buildBlocksFromTradeData : (NSString *) tradeTypeKey : (int) start : (int) end {
-(int) buildBlocksFromTradeData : (NSString *) tradeTypeKey : (int) start  {
    
    tradeValueLimit_min = start;
    tradeValueLimit_max = start + 4; // max number of blocks that we can have for each dropline.
    
    NSMutableArray * blockArray = [[dataManager singleton] getTradeDataWithInterval:tradeTypeKey:start:tradeValueLimit_max];
    float stackBlockWidth = 0;
    /**
     *  GOTTA SEE THIS STUFF!
     */
    
    float previousBlockWidth = 0;
   
    if (blockArray != nil) {
        
        float block_space = BLOCK_DROPLINE_SPACE;
        float start_y = 0;
        float start_x;
        
        
        if (![self isMoneyDropLine]) {
             start_x = bitcoin_start_x;
        }
        else {
             start_x = money_start_x;
        }
        
        
        float front_x = start_x;
        int i = 0;
        int blockLimit = (int)[blockArray count];
        
        for(i=0; i < blockLimit; i++) {
     
            
            id value = blockArray[i];
            
            NSNumber * single_value = [value objectForKey:@"hkd"];
            NSNumber * bitcoin_quantity = [value objectForKey:@"btc"];
            NSMutableString * uniqueBlockName;
            
            uniqueBlockName = [NSMutableString stringWithFormat:@"%@_money_%d_%@", BLOCK_DROPLINE_PREFIX, i, [self name]];

            block * blockObject = [[block alloc]init];
            [blockObject setup:uniqueBlockName];
            [blockObject setDropLineBlock:TRUE];
            [blockObject setInsideContainer:[self name]];
            [blockObject setCoinType:[self getBlockType]];
            [blockObject updateCoinValueSettings:[single_value floatValue] :[bitcoin_quantity floatValue]];
            [blockObject setDropLineCoinCollisionInteraction];
            
            stackBlockWidth += blockObject.frame.size.width;
            
            if (i==0) {
                tradeLabelValue_min = [blockObject getTradeValue];
            }
            if (i== (blockLimit -1)) {
                tradeLabelValue_max = [blockObject getTradeValue];
            }
            
            
           // Position the beginning of the blockcoins on the dropline
            if ([self isBitcoinDropLine]) {
                
                if (i!=0) {
                   // front_x -= blockObject.frame.size.width + (block_space);
                    front_x -= (previousBlockWidth + block_space);
                    //front_x -= blockObject.frame.size.width;
                    previousBlockWidth = blockObject.frame.size.width;
                }
                else {
                    previousBlockWidth = blockObject.frame.size.width;
                    front_x = 0 + self.frame.size.width/4;
                }
            
            }
                
            else {
                if (i!=0) {
                    front_x += blockObject.frame.size.width + 15;
                }
                else {
                    front_x = 0 - self.frame.size.width/4;
                }
            }
   
            CGPoint point = CGPointMake(front_x, start_y);
            [blockObject setPosition:point];
            
            [self addChild:blockObject];
            [blockObject resetDropLineRotation:dropLineRotateValue:dropLineRotationTime];

   
            
            stackBlockWidth += block_space/2;
            
            if (stackBlockWidth > 530) {
                tradeValueLimit_max = start+(i+1);
                tradeLabelValue_max = [blockObject getTradeValue];
                break;
            }
            
            if (start == 0 && i==0) {
                [self setBidBestPrice:[blockObject getTradeInstanceValue]];
            }
        }
    }
    
    [self attachDropLineTradeValueLabels];
    
    return tradeValueLimit_max;
}


/**
 *  @setBidBestPrice
 */
- (void) setBidBestPrice : (float) bestOrderBookTradeValue {
    
    if ([self isBitcoinDropLine]) {
        bitcoin_orderbook_bestprice = bestOrderBookTradeValue;
    }
    else {
        money_orderbook_bestprice = bestOrderBookTradeValue;
    }
}


/**
 *  @all physics related
 */
/*
- (void) setInteraction {
    
    // Physics
   
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:defaultSize];
    self.physicsBody.dynamic = YES;
    
    // It's own category.
    //self.physicsBody.categoryBitMask = dropline_category;
    
    // bucketSprite when colliding with these categories.
    self.physicsBody.contactTestBitMask = block_category;
    
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;

    
}
 */





@end
