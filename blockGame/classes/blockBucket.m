//
//  blockBucket.m
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "blockBucket.h"

@implementation blockBucket {
    
    
    
}

-(id) init {
    self = [super init];
    if(self)
    {
        
    }
    return self;
}


- (void) setup : (NSString *) uniqueName : (SKSpriteNode *) parentFrame_p {
    
    //do something
    parentFrame = parentFrame_p.frame;
    parentScene = parentFrame_p;
    [self initialSetup : uniqueName];
    [self setInteraction];
    
 
    bucketBlockHasIncompleteBlocks = FALSE;
    tradeToBlockHasIncompleteBlocks = FALSE;
  
}


/**
 *  @initialSetup
 */
- (void) initialSetup : (NSString *) uniqueName {
    
    initial_vertical_positions = [[NSMutableArray alloc]init];
    //blockCount = 0;
    
    bucketLimit = BLOCK_BUCKET_VISIBLE_QUANTITY;
    bucket_height = (BLOCK_BUCKET_HEIGHT * BLOCK_BUCKET_VISIBLE_QUANTITY) + (BLOCK_BUCKET_SPACE * BLOCK_BUCKET_VISIBLE_QUANTITY);
    
    addedBlocksCount = 0;
    //currentBucketNetValue = 0;
    currentLiquidValue = 0;
    fullBlockCount = 0;

    [self setSize:CGSizeMake(BLOCK_BUCKET_WIDTH, bucket_height)];

    SKSpriteNode * colorPilar = [[SKSpriteNode alloc]init];
    [colorPilar setSize:CGSizeMake(BLOCK_PILAR_WIDTH, bucket_height)];
    [colorPilar setColor:[UIColor whiteColor]];
    [colorPilar setAlpha:0.7];
    [colorPilar setPosition:CGPointMake(0, 0)];
    
    [self setAnchorPoint:CGPointMake(0.5, 0.5)];
    [self setName:uniqueName];
    
    [self addChild:colorPilar];

}


/**
 *  @setInteraction
 */
- (void) setInteraction {
    
    // Physics
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(BLOCK_BUCKET_WIDTH, bucket_height)];
    //self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(BLOCK_WIDTH,bucket_height) center:CGPointMake(self.frame.origin.x,self.frame.origin.y)];
    
    self.physicsBody.dynamic = YES;
    // It's own category.
    self.physicsBody.categoryBitMask = bucket_category;
    
    // bucketSprite when colliding with these categories.
    self.physicsBody.contactTestBitMask = block_category;
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;
    
    // self.anchorPoint = CGPointZero;

}


/**
 *  @Sets bucket on the rigth position
 */
-(void) setMode : (NSString *) bucketType_p {
    
    bucketType = bucketType_p;
    
    if ([bucketType isEqualToString:BITCOIN_BUCKET_ID]) {
        bitcoin = TRUE;
        isBitCoinBucket = TRUE;
        [self setRightPosition];
    }
    
    else if ([bucketType isEqualToString:MONEY_BUCKET_ID]) {
        bitcoin = FALSE;
        isBitCoinBucket = FALSE;
        [self setLeftPosition];
        
    }
  
    [self setBlocksPositions];
    [self setBucketWallet];
    
}


/**
 *  @Set Blocks Positions
 */
-(void) setBlocksPositions {
    
    float block_space = BLOCK_BUCKET_SPACE;
    float start_y = 0 - self.frame.size.height/2 + BLOCK_BUCKET_HEIGHT/2;
   
    for (int i = 0; i < BLOCK_BUCKET_VISIBLE_QUANTITY; i++) {
        
        if (i!=0) {
            start_y += BLOCK_BUCKET_HEIGHT + block_space;
        }

        NSNumber *num = [NSNumber numberWithFloat:start_y];
        [initial_vertical_positions addObject:num];
    }

}



/**
 *  @GetMode
 */
-(NSString *) getMode {
    
    return bucketType;
}



/**
 *  @closeBlockBucketHeatMap
 */
-(void) closeBlockBucketHeatMap {
    
    NSArray * children = [self getChildBlocks];
    int children_size;
    int i;
    if (children != nil) {
        children_size = (int)[children count];
    }

    
    for (i=0; i < [children count]; i++) {
        block * coin = [children objectAtIndex:i];
        [coin hideHeatMap];
    }
}

/**
 *  @runDropLineHeatMap
 */
- (void) runBlockBucketHeatMap : (block *) betterBidCoin {
    NSArray * children = [self getChildBlocks];
    
    int children_size;
    int i;
    if (children != nil) {
        children_size = (int)[children count];
    }
    
    float betterBidValue = [betterBidCoin getTradeInstanceValue];
    NSLog(@"SEEING FOR %f", betterBidValue);
    
    for (i=0; i < [children count]; i++) {
        
        block * coin = [children objectAtIndex:i];
        [coin showHeatMapBlockBucket:betterBidValue];
        
        /*
        if ([coin getBitcoinTradePercentalValue] < betterBidValue) {
            
        }
        
        else if ([coin getBitcoinTradePercentalValue] > betterBidValue) {
            [coin setHeatMapValueTrade:FALSE];
        }
         */
        
        //NSLog(@"----------------------");
        // NSLog(@" %f", [coin getTradeInstanceValue]); // real value even if it's incomplete
        // NSLog(@" %f", [coin getPercentualValue]); // 1.0, 0.28//
        // NSLog(@" %f", [coin getTradeValue]); // real value even if it's incomplete
        // NSLog(@" %f", [coin getBitcoinTradePercentalValue]); // full bitcoin value even if it's incomplete
 
        
    }

}


/**
 *  @addWalletCoin
 */
-(void) addWalletCoin : (block *) walletBlockCoin{
    
    fullBlockCount = [self getChildBlocksCount];
    [self addTradeCoins:1:[walletBlockCoin getTradeInstanceValue]];
    [[self getBucketWallet] removeCoin];
    
}


/**
 *  @getLowestBitcoin
 */
- (block *) getLowestBitcoin {
    
    NSArray * children = [self getChildBlocks];
    block * lowestReferenceCoin;
    int i;
    int children_size;
    
    
    if (children != nil) {
        children_size = (int)[children count];
    }
    
    for (i=0; i < [children count]; i++) {
        block * blockTemp = [children objectAtIndex:i];
        [blockTemp hideHeatMap];
        if (i==0) {
            lowestReferenceCoin = blockTemp;
        }
        
        else {
            
            if ([blockTemp getBitcoinTradePercentalValue] < [lowestReferenceCoin getBitcoinTradePercentalValue]) {
                lowestReferenceCoin = blockTemp;
            }
            else if ([blockTemp getBitcoinTradePercentalValue] == [lowestReferenceCoin getBitcoinTradePercentalValue]) {
                if (![blockTemp isIncompleteBlock] && [lowestReferenceCoin isIncompleteBlock]) {
                   lowestReferenceCoin = blockTemp; 
                }
            }
        }
    }
    
    return lowestReferenceCoin;
    
}

/**
 * @sets bucket on the rigth position
 */
-(void) setRightPosition {
    
    float padding = self.frame.size.width * 5;
    float position_x = parentFrame.size.width - padding;
    float position_y = parentFrame.origin.y + parentFrame.size.height/2 + self.frame.size.height/2 + 20;


    [self setPosition:CGPointMake(position_x, position_y)];
    
}

/**
 *  @Sets bucket on the left position
 */
-(void) setLeftPosition {
    
    float padding = self.frame.size.width * 5;
    float position_x = parentFrame.origin.x + padding;
    float position_y = parentFrame.origin.y + parentFrame.size.height/2 + self.frame.size.height/2 + 20; 
    
    [self setPosition:CGPointMake(position_x, position_y)];

}



/**
 *  General settings for value Wallet
 */
-(void) setBucketWallet {
    
    valueWallet = [[wallet alloc]init];
    [valueWallet setMode:bucketType];
    [self addChild:valueWallet];
    
    float wallet_position_x = 0 - 90;
    if ([self isBitCoinBucket]) {
        wallet_position_x = 0 +90;
    }
    
    float wallet_position_y = 0 - self.frame.size.height/2 + 30;

    [valueWallet setPosition:CGPointMake(wallet_position_x, wallet_position_y)];
    
    valueWallet.zPosition = 3;
    
}


/**
 *  Returns the wallet attached to the block.
 */
-(wallet *) getBucketWallet {
    
    return valueWallet;
    
}



/**
 *  @playWalletAnimation
 */
-(void) playWalletAnimation {
    
    float start_x = valueWallet.frame.origin.x;
    float start_y = valueWallet.frame.origin.y;
    
    for (int i = 0; i < BLOCK_BUCKET_VISIBLE_QUANTITY; i++) {
        
        block * blockObject = [[block alloc]init];
        
        CGPoint point = CGPointMake(start_x, start_y);
        NSMutableString * uniqueName;
        
        if ([bucketType isEqualToString:BITCOIN_BUCKET_ID]) {
            uniqueName = [NSMutableString stringWithFormat:@"%@_%@_%d", BLOCK_BUCKET_PREFIX, BITCOIN_BUCKET_ID, i];
        }
        else {
            uniqueName = [NSMutableString stringWithFormat:@"%@_%@_%d", BLOCK_BUCKET_PREFIX, MONEY_BUCKET_ID, i];
        }
        
        [blockObject setup:uniqueName];
        [blockObject setPosition:point];
        [blockObject setInsideContainer:[self name]];
        [blockObject setCoinType:bucketType];
        [blockObject removeCollisionInteraction];
        [blockObject updateCoinValueSettings:BLOCK_BUCKET_TRADE_UNIQUE_VALUE :BLOCK_BUCKET_TRADE_QUANTITY_VALUE];
 
        //blockCount++;
        fullBlockCount++;
        currentLiquidValue += BLOCK_BUCKET_TRADE_UNIQUE_VALUE;
        
        [blockObject setAlpha:0];
   
        [self addChild:blockObject];
        
    }
    
    SKAction * fadeIn = [SKAction fadeInWithDuration:0.8];
    NSArray * childBlocks = [self getChildBlocks];
    
    for (int i=0; i< [childBlocks count]; i++) {
        block * temp_block = [childBlocks objectAtIndex:i];
        [temp_block runAction:fadeIn completion:^ {
            [temp_block setCollisionInteraction];
        }];
    }
    
    
    [self arrangeBlocks];
    [valueWallet removeQuantity:20];
   
    
}


/**
 *  @isBitCoinBucket
 */
-(BOOL) isBitCoinBucket {
    return isBitCoinBucket;
}

/**
 *  @isMoneyBucket
 */
-(BOOL) isMoneyBucket {
    if (isBitCoinBucket == FALSE) {
        return TRUE;
        
    }
    return FALSE;
}


/**
 *  @generateBucketBlockUniqueName
 */
-(NSString *) generateBucketBlockUniqueName: (int) index {
    
    NSMutableString * uniqueName;

    if ([bucketType isEqualToString:BITCOIN_BUCKET_ID]) {
        uniqueName = [NSMutableString stringWithFormat:@"block_bucket_bitcoin_%d",index];
    }
    else {
        uniqueName = [NSMutableString stringWithFormat:@"block_bucket_money_%d",index];
    }

    return uniqueName;
    
}


/**
 *  @getBlockReferenceValue
 */
-(float) getBlockReferenceValue {
    
    if ([self isBitCoinBucket]) {
        return bitcoin_reference_lowest_value;
    }
    else {
        return money_reference_highest_value;
    }
    
}


/**
 *
 */
-(block *) getIncompleteBlockOfBucket {
    
    NSArray * children = [self getChildBlocks];
    int children_size = 0;
    int i;
    
  
    
    float referenceValue = [self getBlockReferenceValue];
    
    if (children != nil) {
        children_size = (int)[children count];
    }
    
    for (i=0; i < [children count]; i++) {
        block * blockTemp = [children objectAtIndex:i];
        if ([blockTemp getTradeInstanceValue] != referenceValue) {
            return blockTemp;
        }
    }
    
    return FALSE;
}

/**
 *  @bucketIsFull
 */
-(BOOL) bucketIsFull {
    
    NSArray * children = [self getChildBlocks];
    int children_size = 0;
    int i;
    
    BOOL bucketIsFull;
    
    float referenceValue = [self getBlockReferenceValue];
    
    if (children != nil) {
        children_size = (int)[children count];
    }
    
    if (children_size >= BLOCK_BUCKET_VISIBLE_QUANTITY) {
        bucketIsFull = TRUE;
        for (i=0; i < [children count]; i++) {
            block * blockTemp = [children objectAtIndex:i];
            if ([blockTemp getTradeInstanceValue] != referenceValue) {
                bucketIsFull = FALSE;
            }
        }
    }
    
    else {
        bucketIsFull = FALSE;
    }

    return bucketIsFull;
}


/**
 *  @addBlockToBucket
 */
- (void) addBlockToBucket : (float) blockValue : (int) unique_index {
    
    block * blockObject = [[block alloc]init];
    float blockQuantity = 1;
    int start_x = 0;
    
    [blockObject setup:[self generateBucketBlockUniqueName:unique_index+1]];
    [blockObject setInsideContainer:[self name]];
    [blockObject setCoinType:bucketType];
    [blockObject updateCoinValueSettings:blockValue:blockQuantity];
    
    //[blockObject attachPriceLabel];
    [blockObject setPosition:CGPointMake(start_x, 20)];
    
    [self addChild:blockObject];
    
    fullBlockCount = [self getChildBlocksCount];
    [self arrangeBlocks];
    
}


/**
 *  @getBucketBlockByIndex
 */
- (block *) getBucketBlockByIndex : (int) index {
    
    NSArray * childBlocks = [self getChildBlocks];
    return [childBlocks objectAtIndex:[childBlocks count]-1];

}



/**
 *  @stackTradedBlockToBucketBlock
 */
- (float) stackTradedBlockToBucketBlock : (float) tradedBlockValue {

    NSArray * childBlocks = [self getChildBlocks];
    block * lastBlock = (block *)[childBlocks objectAtIndex:[childBlocks count] -1];
    float howMuchICanStackValue = [self getBlockReferenceValue] - [lastBlock getTradeInstanceValue];

    if (lastBlock == nil) {
        NSLog(@"ERROR  ::  -- :: last block on stackTradedBlock in bucket method does not exist");
    }
    
    // Cannot stack over this block.
    [lastBlock stackTradeValueAndSize:howMuchICanStackValue:1];

    /*
    if (howMuchICanStackValue <= 0) {
        // Add any more smaller blocks
        float remainingValue = tradedBlockValue - howMuchICanStackValue;
        
        if (remainingValue > 0 && ![self bucketIsFull]) {
            [self addBlockToBucket:remainingValue :1];
        }
        
    }
    else {
        
        
    }
     */
    
    return howMuchICanStackValue;
    
}


/**
 *  @addBlockAfterTrade
 *  Main final operation
 */
- (void) addBlockAfterTrade : (float) blockValue {
    
    NSArray * children = [self getChildBlocks];
    int children_size = 0;
    
    if (children != nil) {
        children_size = (int)[children count];
    }
    else {
        children_size = 666;
    }

    if (bucketBlockHasIncompleteBlocks == FALSE) {
			[self addBlockToBucket:blockValue:children_size];
			if (tradeToBlockHasIncompleteBlocks == TRUE) {
            bucketBlockHasIncompleteBlocks = TRUE;
            tradeToBlockHasIncompleteBlocks = FALSE; // reset it.
        }
    }
            
    else {
			NSLog(@"HAS INCOMPLETE BLOCKS ?? : %hhd", bucketBlockHasIncompleteBlocks);
			[self stackTradedBlockToBucketBlock:blockValue];
    }
            
    
  

}



/**
 *  @blockBucketFinalTradeOperations
 */
- (float) blockBucketFinalTradeOperations {
    
    NSArray * childBlocks = [self getChildBlocks];
    float currentBucketNetValue_temp = 0;
    
    for (int i = 0; i< [childBlocks count]; i++) {
        block * tempBlock = [childBlocks objectAtIndex:i];
        currentBucketNetValue_temp += [tempBlock getTradeValue];
    }
    
    currentLiquidValue = currentBucketNetValue_temp;
    fullBlockCount = [self getChildBlocksCount];
    
    if ([childBlocks count] ==0) {
        bucketBlockHasIncompleteBlocks = FALSE;
    }
    
    if ([self isBitCoinBucket]) {
        bitcoin_bucket_current_net_value = currentLiquidValue;
        return bitcoin_bucket_current_net_value;
    }
    
    else {
        money_bucket_current_net_value = currentLiquidValue;
        return money_bucket_current_net_value;
    }
    
    return 0;

}


/**
 *  @bucketCannotFillFullTrade
 */
-(BOOL) bucketCannotFillFullTrade : (float) numberOfBlocksTradedValue {

   
    BOOL bucketCannotFillTrade = FALSE;
    float limitBucketValue = BLOCK_BUCKET_VISIBLE_QUANTITY *  [self getBlockReferenceValue];
    float newValue = currentLiquidValue + numberOfBlocksTradedValue;
 
    if (newValue > limitBucketValue) {
        bucketCannotFillTrade = TRUE;
    }
    else {
        bucketCannotFillTrade = FALSE;
    }

    return bucketCannotFillTrade;
    
}


/**
 *  @releaseCoinsFromBucketToWallet
 */
- (void) releaseCoinsFromBucketToWallet : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue {
    
    NSLog(@"RELESE COINS FROM BUCKET TO WALLET");
    float sumValue = 0;
    int i;
    
    NSMutableArray * blocksToRemove = [[NSMutableArray alloc] init];
    NSArray * childrenBlockCoins = [self getChildBlocks];
    
    for (i=0; i<[childrenBlockCoins count]; i++) {
        block * bitcoin_temp = [childrenBlockCoins objectAtIndex:i];
        
        if (![bitcoin_temp isIncompleteBlock]) {
            sumValue += [bitcoin_temp getTradeInstanceValue];
            [blocksToRemove addObject:bitcoin_temp];
            fullBlockCount--;
        
        }
        if (sumValue > numberOfBlocksTradedValue) {
            break;
        }
    }
    
    wallet * tempWallet = [self getBucketWallet];
    CGPoint walletPoint = CGPointMake(tempWallet.frame.origin.x, tempWallet.frame.origin.y);
    
    
    /**
     *  1. Moves Necessary Coins from BUCKET to WALLET
     *  2. Adds the other coins to the BUCKET from the previous TRADE
     *  3.
     */
    
    for (i=0; i<[blocksToRemove count]; i++) {
        block * bitcoin_temp = [blocksToRemove objectAtIndex:i];
        [bitcoin_temp runAction:[SKAction moveTo:walletPoint duration:0.5] completion:^ {
            [bitcoin_temp runAction:[SKAction fadeOutWithDuration:0.5] completion:^ {
                [bitcoin_temp removeFromParent];
                [[self getBucketWallet] addCoin:bitcoin_temp];
                if (i == ([blocksToRemove count]-1)) {
                    currentLiquidValue -= sumValue;
                    [self addTradeCoins:numberOfBlocksTraded :numberOfBlocksTradedValue];
                }
            }];
        }];
    }
    

}



/**
 *  @addTradeCoins
 */
- (void) addTradeCoins : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue {
	
		fullBlockCount = [self getChildBlocksCount];
    float decreaseValue = numberOfBlocksTradedValue;
    float blockReferenceValue =[self getBlockReferenceValue];
    float valueToFillIncompleteBlock = 0;
    
    BOOL cannotFillTrade =[self bucketCannotFillFullTrade:numberOfBlocksTradedValue];
    

    if (cannotFillTrade) {
        NSLog(@"----------CANNOT FILL TRADE");
        [self releaseCoinsFromBucketToWallet:numberOfBlocksTraded:numberOfBlocksTradedValue];
    }

    else {

        //fullBlockCount += numberOfBlocksTraded;
        //currentLiquidValue += numberOfBlocksTradedValue;
        
        BOOL keepBuilding = TRUE;
        
        if (bucketBlockHasIncompleteBlocks) {
            valueToFillIncompleteBlock = [self stackTradedBlockToBucketBlock:numberOfBlocksTradedValue];
            bucketBlockHasIncompleteBlocks = false;
        }
        else {
            NSLog(@"NO INCOMPLETE BLOCKS???");
        }
     
        decreaseValue -= valueToFillIncompleteBlock;
 
        while(keepBuilding && decreaseValue > 0) {
 
            decreaseValue -= blockReferenceValue;
            
            if (decreaseValue > 0) {
                [self addBlockAfterTrade : blockReferenceValue];
            }
            
            else {
                tradeToBlockHasIncompleteBlocks = TRUE;
                float lastSmallBlockValue = decreaseValue + blockReferenceValue;
                [self addBlockAfterTrade:lastSmallBlockValue];
                keepBuilding = FALSE;
            }
        }
    }
}


/**
 *  @refreshTradeStatus
 */
-(void) refreshTradeStatusRemove : (block *) blockObject_p {
    
    SKAction * fadeOut = [SKAction fadeOutWithDuration:1];
    [blockObject_p runAction:fadeOut completion:^{
        NSMutableArray * removeChild = [[NSMutableArray alloc] initWithCapacity:1];
        [removeChild addObject:blockObject_p];
        [self removeChildrenInArray:removeChild];
        [self arrangeBlocks];
        [self blockBucketFinalTradeOperations];
    }];
    
}


/**
 *  @removeCoinFromBucket
 */
-(void) removeCoinFromBucket : (block *) blockObject_p {
   
    NSMutableArray * removeChild = [[NSMutableArray alloc] initWithCapacity:1];
    [removeChild addObject:blockObject_p];
    [self removeChildrenInArray:removeChild];
    [self arrangeBlocks];
    [self blockBucketFinalTradeOperations];
}


/**
 *  @closestCoin
 */
-(block *) closestCoin : (block *) targetBlockCoin {
    
    NSArray * childBlocks = [self getChildBlocks];
    float compare_y = 0;
    float store_y = 0;
    block * closestBlock;
    BOOL firstTime = true;
    
    for (int i=0 ; i< [childBlocks count]; i++) {
        block * tempBlock = (block *) [childBlocks objectAtIndex:i];
    
        if ([[tempBlock name] isEqualToString:[targetBlockCoin name]]) {

        }
        else {
            compare_y = abs(tempBlock.frame.origin.y-targetBlockCoin.frame.origin.y);
            if (firstTime) {
                store_y = compare_y;
                firstTime = FALSE;
            }
            
            if (compare_y <= store_y) {
                store_y = compare_y;
                closestBlock = tempBlock;
            }
            
        }
    }
    

    return closestBlock;
    
}


/**
 *  @splitUpCoins
 */
- (void) splitUpCoin:(block *) _selectedNode {
    
    // 1. Make sure there's more space to coins
    if ([self getChildBlocksCount] >= BLOCK_BUCKET_VISIBLE_QUANTITY) {
        [[gameEventZone singleton] displayInfoTime:@"No space to split coin" :1];
    }
    
    else {
        //[[gameEventZone singleton] displayInfoTime:@"Spliting up coin.." :1];
        float block_value = [_selectedNode getTradeInstanceValue]/2;
        [_selectedNode updateCoinValueSettings:block_value :1];
        [self addBlockToBucket:block_value:[self getChildBlocksCount]];
        [[gameEventZone singleton] displayInfoTime:@"Coin split." :1];
    }
}


/**
 *  @bundleUpCoins
 */
-(void) bundleUpCoins:(block *)_selectedNode {

    block * blockToMerge = _selectedNode;
    blockToMerge.zPosition = 50;
    // 1. Make sure there's coins to bundle
    
    if ([self getChildBlocksCount] <= 1) {
        [[gameEventZone singleton] displayInfoTime:@"No coins to merge." :1.5];
    }
    
    // 2. Can merge
    else {
        
        // 2.1 Get closest Y of the closest coin
        [[gameEventZone singleton] displayInfoTime:@"Merging coin." :1.5];
        block * closestBlock = [self closestCoin:blockToMerge];
        
        float position_y = closestBlock.frame.origin.y + blockToMerge.frame.size.height/2;
        if (blockToMerge.frame.origin.y < closestBlock.frame.origin.y) {
            position_y = closestBlock.frame.origin.y + blockToMerge.frame.size.height/2;
        }
        
        // 2.2 Move coin there, scale down and fade
        SKAction * moveToClose = [SKAction moveToY:position_y duration:1.0];
        SKAction * scaleDown = [SKAction scaleTo:0 duration:0.5];
        SKAction * fadeOut = [SKAction fadeOutWithDuration:0.5];
        SKAction * mergeAction = [SKAction sequence:@[moveToClose,scaleDown]];
        
        [blockToMerge runAction:fadeOut];
        [blockToMerge runAction:mergeAction completion:^ {
            [self removeCoinFromBucket:blockToMerge];
            [self addValueToCoins:[blockToMerge getTradeInstanceValue]];
        }];
        
    }

}


/**
 *  @addValueToCoins
 */
- (void) addValueToCoins : (float) tradeValue {
    
    NSArray * childBlocks = [self getChildBlocks];
    //SKAction * scaleUp = [SKAction scaleTo:1.3 duration:0.2];
    //SKAction * scaleDown = [SKAction scaleTo:1.0 duration:0.3];
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.2 duration:0.3];
    SKAction *fadeIn = [SKAction fadeAlphaTo:1 duration:0.3];
    

    SKAction * mergeAction = [SKAction sequence:@[fadeOut,fadeIn]];
    
    int lastBlock = (int)[childBlocks count]-1;
    float tradeSplit = tradeValue/fullBlockCount;
    float newTradeValue;
    
    for (int i=0 ; i < [childBlocks count]; i++) {
        
        block * tempBlock = (block *) [childBlocks objectAtIndex:i];
        newTradeValue = [tempBlock getTradeInstanceValue] + tradeSplit;
        [tempBlock updateCoinValueSettings:newTradeValue :1];
        
        [tempBlock runAction:mergeAction completion:^ {
          
        }];
        if (i == lastBlock) {
            [[gameEventZone singleton] displayInfoTime:@"Coin merged" :1.0];
        }
        
    }
}

/**
 *  @fullBlockCount.
 */
-(int) fullBlockCount {
    
    return fullBlockCount;
}

/**
 *  @blocksInsideBucket
 */
-(int) blocksInsideBucket {
    return blockCount;
}



/**
 * @getChildBlocks
 */
- (NSMutableArray *) getChildBlocks {
    
    NSArray * children = [self children];
    NSMutableArray * blockChildren = [[NSMutableArray alloc]initWithCapacity:50]; //BLOCK_BUCKET_VISIBLE_QUANTITY
    int i;

    for (i=0; i < [children count]; i++) {
        if([children objectAtIndex:i] == nil ){
            
        }
        else {
            if([[children objectAtIndex:i] isKindOfClass:[block class]]) {
                [blockChildren addObject:[children objectAtIndex:i]];
            }

            
        }
   }
    
    if ([blockChildren count] > 0) {
        return blockChildren;
    }
    
    return nil;
    
}


/**
 * @getChildBlocksCount
 */
- (int) getChildBlocksCount {
    
    NSArray * children = [self children];
    int countBlocks = 0;
    int i;
    
    for (i=0; i < [children count]; i++) {
        
        if([[children objectAtIndex:i] isKindOfClass:[block class]]) {
            countBlocks++;
        }
    }
    
    return countBlocks;
}


/**
 * @arrangeBlocks
 */
-(BOOL) arrangeBlocks { // : (block *) affectedBlock 
    
    int i = 0;
    float point_x = 0;
    NSNumber * point_y;

    NSMutableArray * children = [self getChildBlocks];
    int children_count = 0;
    
    if (children !=nil) {
        children_count = (int)[[self getChildBlocks]count];
    }
    
    for (i=0; i< children_count; i++) {
        block * tempBlock = [children objectAtIndex:i];
        point_y = (NSNumber *)[initial_vertical_positions objectAtIndex:(NSUInteger)i];
        SKAction *shiftAction = [SKAction moveTo:CGPointMake(point_x,[point_y floatValue]) duration:0.3];
        
        [tempBlock runAction:shiftAction];
    }
 
 
    return TRUE;
}




@end
