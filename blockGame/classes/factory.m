//
//  factory.m
//  blockGame
//
//  Created by daniel.oliveira on 19/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "factory.h"
#import "block.h"



@implementation factory {
    
    
}

static factory *singleton = nil;

+ (factory *)singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
    }
    return singleton;
}

- (id)init {
    if ( (self = [super init]) ) {
        // your custom initialization
    }
    return self;
}


/**
 *
 */
- (SKLabelNode *) buildBlockLabel : (NSString *) stringName_p : (CGPoint) position_p : (UIColor *) label_font_color;  {
    
    SKLabelNode *skLabel = [SKLabelNode labelNodeWithFontNamed:BLOCK_LABEL_FONT];
    skLabel.text = stringName_p;
    
    skLabel.fontColor = label_font_color;
    skLabel.position = position_p;
    skLabel.fontSize = BLOCK_LABEL_FONTSIZE;

    
    return skLabel;
}

/**
 *
 */
- (SKLabelNode *) buildLabel : (NSString *) stringName_p : (CGPoint) position_p : (UIColor *) color  {
    
    SKLabelNode *skLabel = [SKLabelNode labelNodeWithFontNamed:LABEL_FONT];
    skLabel.text = stringName_p;
    /*
     *   CGRectGetMidX
     *   CGRectMidY
     */
    
    skLabel.position = position_p;
    skLabel.fontSize = LABEL_FONTSIZE;
    skLabel.color = color;
    skLabel.fontColor = color;
    
    return skLabel;
}

/**
 *  @builds a Block
 */

/*
- (block *) buildBlock : (NSString *)format_p : (CGRect *) size_p {
    
    block * newBlock = [[block alloc]init];
    [newBlock buildObject:format_p :size_p];
    
    
    if (newBlock != nil) {
        return newBlock;
    }
    
    return nil;
}
 */

/**
 *  @buildGameContainer
 */
- (SKSpriteNode *) buildGameContainer : (CGRect) parentSize {

    SKSpriteNode * gameContainer;
    
    //_background = [SKSpriteNode spriteNodeWithImageNamed:@"blue-shooting-stars"];
    
    
    int background_width = parentSize.size.width;
    int background_height = parentSize.size.height - (parentSize.size.height/6);
    UIColor * color = [UIColor darkGrayColor];
    
    // anchorPoint on the middle of the frame more useful.
    
    gameContainer = [SKSpriteNode spriteNodeWithColor:color size:CGSizeMake(background_width, background_height)];
    [gameContainer setName:GAME_CONTAINER];
    [gameContainer setSize:CGSizeMake(background_width, background_height)];
    gameContainer.anchorPoint =  CGPointMake(0.5,0.5);
    
    // Physics
    
    gameContainer.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:gameContainer.frame];
    
    // Who is my physic body.
    gameContainer.physicsBody.categoryBitMask = gamecontainer_category;
    
    // Who should it contact with.
    gameContainer.physicsBody.contactTestBitMask = block_category;
    
    //setPosition is according to the parent frame
    int start_x = parentSize.origin.x + gameContainer.frame.size.width/2;
    int start_y = parentSize.origin.y + gameContainer.frame.size.height/2;
    [gameContainer setPosition:CGPointMake(start_x,start_y)];
   

    return gameContainer;
}


/**
 *  @Build Bucket
 */
- (SKSpriteNode *) buildBucket : (CGRect) parentFrame : (NSString *) uniqueName {
    
    
    // Basically the height of all the blocks plus a little bit.
    //int bucket_height = (BLOCK_BUCKET_HEIGHT * BLOCK_BUCKET_VISIBLE_QUANTITY) + (BLOCK_BUCKET_HEIGHT) + 20;
    
    //int bucket_height = (COIN_DEFAULT_SIZE * BLOCK_BUCKET_VISIBLE_QUANTITY) + (COIN_DEFAULT_SIZE) + 20;
    
    UIColor * color = BUCKET_BACKGROUND_COLOR;
    SKSpriteNode * bucketSprite;
    
    bucketSprite = [SKSpriteNode spriteNodeWithColor:color size:CGSizeMake(COIN_DEFAULT_SIZE, COIN_DEFAULT_SIZE)];
    [bucketSprite setName:uniqueName];
    [bucketSprite setAnchorPoint:CGPointZero];
    [bucketSprite setPosition:CGPointMake(parentFrame.origin.x + 20, parentFrame.origin.y + parentFrame.size.height/3)];
    
    
    // Physics
    bucketSprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bucketSprite.size];
    bucketSprite.physicsBody.dynamic = YES;
    
    // It's own category.
    bucketSprite.physicsBody.categoryBitMask = bucket_category;
    
    // bucketSprite when colliding with these categories.
    bucketSprite.physicsBody.contactTestBitMask = block_category;
    
    // Collision mask set to zero for no bouncing or such effect.
    bucketSprite.physicsBody.collisionBitMask = 0;

 

    int block_up_y =0;
    int block_space = 5;
    // float offsetFraction = ((float)(i + 1)) / (BLOCK_BUCKET_LIMIT + 1);
    
    for (int i = 0; i < BLOCK_BUCKET_VISIBLE_QUANTITY; i++) {
        if (i!=0) {
            //block_up_y += BLOCK_BUCKET_HEIGHT + block_space;
            block_up_y += COIN_DEFAULT_SIZE + block_space;
        }

        CGPoint point = CGPointMake(parentFrame.origin.x, block_up_y);
        [bucketSprite addChild:[self buildBlock:bucketSprite.frame :BLOCK_NAME : point]];
       
    }
  

    return bucketSprite;
    
}

/**
 *  @blockZoomUpAction
 */
-(SKAction *) blockZoomUpAction {
    
    SKAction * zoom = [SKAction scaleTo:1.5 duration:0.25];
    
    
    return zoom;
    
}

/**
 *  @blockZoomDownAction
 */
-(SKAction *) blockZoomDownAction {
    
    SKAction * zoom = [SKAction scaleTo:1.0 duration:0.25];
    
    return zoom;
    
}


/**
 *  @Build Block
 */
- (SKSpriteNode *) buildBlock : (CGRect) parentFrame : (NSString *) uniqueName : (CGPoint) point {
    
    
    SKSpriteNode * blockSprite;
    
    UIColor * color = [UIColor greenColor];
    //blockSprite = [SKSpriteNode spriteNodeWithColor:color size:CGSizeMake(BLOCK_BUCKET_WIDTH, BLOCK_BUCKET_HEIGHT)];
    blockSprite = [SKSpriteNode spriteNodeWithColor:color size:CGSizeMake(COIN_DEFAULT_SIZE, COIN_DEFAULT_SIZE)];
    
    [blockSprite setName:uniqueName];
    [blockSprite setAnchorPoint:CGPointZero]; // LOWER LEFT CORNER
 
    [blockSprite setPosition:point];
    
    
    // Physics
    blockSprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:blockSprite.size];
    blockSprite.physicsBody.dynamic = YES;
    
    // It's own category.
    blockSprite.physicsBody.categoryBitMask = block_category;
    
    // Notify when colliding with these categories.
    blockSprite.physicsBody.contactTestBitMask = bucket_category;
    
    // Collision mask set to zero for no bouncing or such effect.
    blockSprite.physicsBody.collisionBitMask = 0;

 
    
    return blockSprite;
}


/**
 *  @sequenceAction
 */
- (SKAction *) sequenceAction {
    
    
    SKAction *sequence = [SKAction sequence:@[[SKAction rotateByAngle:degToRad(-4.0f) duration:0.1],
                                              [SKAction rotateByAngle:0.0 duration:0.1],
                                              [SKAction rotateByAngle:degToRad(4.0f) duration:0.1]]];
    
    return sequence;
}


/**
 *  @degToRad
 */
float degToRad(float degree) {
	return degree / 180.0f * M_PI;
}


/**
 *  @rotateAction
 */
- (SKAction *) rotateAction {
    
    SKAction *rotateAction = [SKAction rotateByAngle:M_1_PI duration:1];
    
    
    return rotateAction;
    //[sprite runAction:[SKAction repeatActionForever:action]];
    
}


/**
 *  @getRandomNumber
 */
-(CGFloat) getRandomNumber : (CGFloat) low : (CGFloat) high {
    
    return skRandf() * (high - low) + low;
}


static inline CGFloat skRandf() {
    return rand() / (CGFloat) RAND_MAX;
}




@end
