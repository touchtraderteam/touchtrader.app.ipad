//
//  dropLineBucket.m
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "dropLineBucket.h"

@implementation dropLineBucket {
    
    
}

-(id) init {
    self = [super init];
    if(self)
    {
        //do something
        

    }
    return self;
}



/**
 *  @initialSetup
 */
- (void) initialSetup : (NSString *) uniqueName : (SKSpriteNode *) parentFrame_p {
    
    
    parentScene = parentFrame_p;
    defaultColor = DROPLINE_BUCKET_COLOR;
    dropLineCollection = [[NSMutableDictionary alloc] init];
    initial_horizontal_positions = [[NSMutableArray alloc]init];
    
    dropLineBucket_left_limit = self.frame.origin.x - self.frame.size.width/3;
    
    dropLineBucket_width = parentScene.frame.size.width;
    //dropLineBucket_height = parentScene.frame.size.height/5;
    
    dropLineBucket_height = 180;
    
    hitLastDropLine = FALSE;
    
    [self setSize:CGSizeMake(dropLineBucket_width, dropLineBucket_height)];
    //[self setColor:defaultColor];
    
    
    [self setAnchorPoint:CGPointMake(0.5, 0.5)];
    [self setName:uniqueName];
    
    
}


/**
 *  @resetDropLineTrade
 */
-(void)resetDropLineTrade {
    
    NSArray * children = [self children];
    int i;
    
    for (i=0; i<[children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            dropLine * tempDropLine = (dropLine *) [children objectAtIndex:i];
            if ([tempDropLine isDropLineDoingTrade]) {
                [tempDropLine resetBlockTrade];
            }
            
        }
    }
}


/**
 *  @Goes for the droplines and see if a trade can be done.
 */
- (void) doTotalDropLineTrade: (blockBucket *) sourceBlockBucket : (block *) blockBucketObject : (blockBucket *) targetBlockBucket {
    
    NSArray * children = [self children];
    int i;
    
    for (i=0; i<[children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            dropLine * tempDropLine = (dropLine *) [children objectAtIndex:i];
            if( [tempDropLine isDropLineDoingTrade]) {
                [tempDropLine doDropLineTrade:sourceBlockBucket:blockBucketObject:targetBlockBucket];
            }
        }
    }
}


/**
 *  @checkDropLineTrade
 */
-(BOOL) checkDropLineTrade : (block *) blockObject {
    
    NSArray * children = [self children];
    
    float buildTradeValue = 0;
    float blockTradeValue = [blockObject getTradeValue];
    int i =0;
    
    for (i=0; i<[children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            
            dropLine * tempDropLine = (dropLine *) [children objectAtIndex:i];
            if (i==0) {
                buildTradeValue = [tempDropLine checkBlockTrade:blockTradeValue];
            }
            else {
                buildTradeValue = [tempDropLine checkBlockTrade:buildTradeValue];
            }
            
            
            if(buildTradeValue <= 0) {
                break;
            }
    
        }
    }
    

    return TRUE;

}



/**
 *  @checkPossibleDropLineTrade
 *
 */
-(BOOL) checkPossibleDropLineTrade : (block *) blockBucketObject  {
    
    NSArray * children = [self children];
    
    float buildTradeValue = 0;
    float blockTradeValue = [blockBucketObject getFullQuantityTradeValue];
    int i =0;
    
    BOOL tradeBitcoinForMoney = FALSE;
    
    if ([blockBucketObject isBitCoinBlock]) {
        tradeBitcoinForMoney = TRUE;
    }
    
    
    for (i=0; i<[children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            
            dropLine * tempDropLine = (dropLine *) [children objectAtIndex:i];

            if (tradeBitcoinForMoney) {
                
                block * highestMoneyBid =  [tempDropLine getFirstTradeBlock];
                float valueToTradeBitcoin = [highestMoneyBid getTradeValue];
                [tempDropLine checkBlockTrade:valueToTradeBitcoin];
                [blockBucketObject showPotentialProfit:valueToTradeBitcoin];
            
                break;
                
            }
            
            else {
            
                if (i==0) {
                    buildTradeValue = [tempDropLine checkBlockTrade:blockTradeValue];
                }
                else {
                    buildTradeValue = [tempDropLine checkBlockTrade:buildTradeValue];
                }
                
                
                if(buildTradeValue <= 0) {
                    break;
                }
            }
        }
    }
    
    return TRUE;
    
}



/**
 *  @all physics related
 */
- (void) setInteraction {
    
    // Physics
       
    int dropLineBucketPhysics_height = self.frame.size.height;
    int dropLineBucketPhysics_width = self.frame.size.width;
    
   
    CGSize physicsSize = CGSizeMake(dropLineBucketPhysics_width,dropLineBucketPhysics_height);
    
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:physicsSize];
    self.physicsBody.dynamic = YES;
    
    // It's own category.
    self.physicsBody.categoryBitMask = droplinebucket_category;
    
    // bucketSprite when colliding with these categories.
    self.physicsBody.contactTestBitMask = block_category;
    
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;
    
    
}


/**
 *  @addDropLines
 */
-(void) addDropLines {
    
    // Droplines visible (default 3)
    NSString * fetchTradeData;
    int dropline_limit = DROPLINEBUCKET_DROPLINES_LIMIT;
    
    //int drop_y = 0 - DROPLINE_BETWEEN_SPACE*3 + self.frame.size.height/2;
    
    int drop_y = 0 - self.frame.size.height/2 + DROPLINE_BETWEEN_SPACE/2 ;
    int block_full_limit = 0;
    int i;
    int drop_x = 0;
    int fetch_start =0;

    // THIS IS COMPLICATED
    int fetch_limit = DROPLINE_BLOCK_QUANTITY;

    if (moneyBucket) {
        fetchTradeData = BID_KEY;
        block_full_limit = TRADE_BID_BLOCK_QUANTITY;
        drop_y = DROPLINE_BETWEEN_SPACE * 3 - self.frame.size.height/2 - 13;
    }
    else {
        block_full_limit = TRADE_ASK_BLOCK_QUANTITY;
        fetchTradeData = ASK_KEY;
    }
    

    for(i=0; i< dropline_limit; i++) {
        
        dropLine * dropLineInstance = [[dropLine alloc] init];
        
        if (i!=0) {
            if (moneyBucket) {
                drop_y -= DROPLINE_BETWEEN_SPACE;
            }
        
            else {
                drop_y += DROPLINE_BETWEEN_SPACE;
            }
        }
        
        if (fetch_limit > block_full_limit) {
            fetch_limit = block_full_limit;
        }

        [dropLineInstance initialSetup:self:i];
        fetch_start = [dropLineInstance buildBlocksFromTradeData:fetchTradeData :fetch_start];
    
        [dropLineInstance setPosition:CGPointMake(drop_x, drop_y)];
        [self addChild:dropLineInstance];
        
        lastFetchStart = fetch_start;


        // No More data, no need to add more dropLineInstances
        if ([dropLineInstance higherTradeValue] <= [self lowestValueWholeBucket]) {
            //break;
        }
       
    }
    
}


/**
 *
 */
-(BOOL) isMoneyBucket {
    
    return moneyBucket;
    
}


/**
 *
 */
-(BOOL) isBitcoinBucket {
    
    if (moneyBucket) {
        return FALSE;
    }
    
    return TRUE;

}

/**
 *
 */
- (NSString *) getType {
   
    if (moneyBucket) {
        return MONEY_BUCKET_ID;
    }
    else {
        return BITCOIN_BUCKET_ID;
    }
}


/**
 *  @addDropLine
 */
-(BOOL) addDropLine : (dropLine *) dropLine {
    
    return TRUE;
}


-(float) lowestValueWholeBucket {
    
    float lowest_value = LOWEST_ASK_VALUE;
    
    if([self isMoneyBucket]) {
        lowest_value = LOWEST_BID_VALUE;
    }
    
    return lowest_value;

}

/**
 *  @removeDropLine
 */
-(BOOL) refreshDropLineBucketStatus : (SKSpriteNode *) dropLineObject {
    
    // Remove the dropline.
    
    dropLine * dropLineObject_casted = (dropLine *) dropLineObject;
    float lowest_value = [self lowestValueWholeBucket];
    bool addDropLines = true;
    
    if ([dropLineObject_casted higherTradeValue] <= lowest_value) {
        addDropLines = false;
        // Dont remove this last line.
    }
    
    else {

        NSMutableArray * removeChilds = [[NSMutableArray alloc] initWithCapacity:1];
        [removeChilds addObject:dropLineObject];
        
        SKAction * fadeOut = [SKAction fadeOutWithDuration:0.5];
        
        [dropLineObject runAction:fadeOut completion:^{
            [self removeChildrenInArray:removeChilds];
            [self moveDropLines];
            
            // in case there's no more values to take.
            if (!hitLastDropLine) {
                [self checkInNextDropLine];
            }
            
            
        }];

        
        
    }
    
    
    
    return TRUE;

}


/**
 *  @checkInNextDropLine
 */
-(void) checkInNextDropLine {
    
    int drop_y = 0;
    int block_full_limit = 0;
    int drop_x = 0;
    
    NSString * fetchTradeData;
    
    if (moneyBucket) {
        fetchTradeData = BID_KEY;
        block_full_limit = TRADE_BID_BLOCK_QUANTITY;
        drop_y = DROPLINE_BETWEEN_SPACE * 3 - self.frame.size.height/2;
        drop_y = 0;
    }
    else {
        block_full_limit = TRADE_ASK_BLOCK_QUANTITY;
        fetchTradeData = ASK_KEY;
        drop_y = self.frame.origin.y + self.frame.size.height/2;
        drop_y = 0;
    }
    
    int fetch_limit = lastFetchStart + DROPLINE_BLOCK_QUANTITY;
    
    if (moneyBucket) {
        drop_y -= DROPLINE_BETWEEN_SPACE;
    }
        
    else {
        drop_y += DROPLINE_BETWEEN_SPACE;
    }
        
    if (fetch_limit > block_full_limit) {
        fetch_limit = block_full_limit;
    }
    
    dropLine * dropLineInstance = [[dropLine alloc] init];
    [dropLineInstance initialSetup:self:4];
    
    
    //[dropLineInstance buildBlocksFromTradeData:fetchTradeData :lastFetchStart :fetch_limit];
    [dropLineInstance buildBlocksFromTradeData:fetchTradeData :lastFetchStart];
    
    [dropLineInstance setPosition:CGPointMake(drop_x, drop_y)];

    SKAction * fadeIn = [SKAction fadeInWithDuration:1];
    [dropLineInstance setAlpha:0];
    [self addChild:dropLineInstance];
    
    [dropLineInstance runAction:fadeIn];
    
    lastFetchStart = fetch_limit;
    

}


/**
 *  @moveDropLines
 */
-(void) moveDropLines {
    
    // Move the other droplines
    NSArray * children = [self children];
    int move_y = - DROPLINE_BETWEEN_SPACE;
    if ([self isMoneyBucket]) {
        move_y =  DROPLINE_BETWEEN_SPACE;
    }
    
    float lowest_value = [self lowestValueWholeBucket];
   

    SKAction * move = [SKAction moveBy:CGVectorMake(0,move_y) duration:0.3];
    int i;
    
    for (i=0; i< [children count]; i++) {
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            [[children objectAtIndex:i] runAction:move];
            
            dropLine * checkDropLine = (dropLine *)[children objectAtIndex:i];
            if ([checkDropLine higherTradeValue] <= lowest_value) {
                hitLastDropLine = TRUE;
            }
            
        }
    }
    
}


/**
 *  @hasDropLineBlocksToTrade
 */
-(BOOL) hasDropLineBlocksToTrade {
    
    NSArray * children = [self children];
    int i;
    
    for (i=0; i< [children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            dropLine * tempDropLine = (dropLine *) [children objectAtIndex:i];
            if ([tempDropLine hasBlocksToTrade]) {
                return true;
            }
            
        }
    }

    return FALSE;
}


/**
 *  @getLowestDropLineBucketCoin
 */
-(block *) getLowestDropLineBucketCoin {
    
    NSArray * children = [self children];
    block * firstBlock;
    
    if ([[children objectAtIndex:0] isKindOfClass:[dropLine class]]) {
        dropLine * tempDropLine = (dropLine *) [children objectAtIndex:0];
        firstBlock = [tempDropLine getFirstTradeBlock];
    }

    if (firstBlock !=nil) {
        return firstBlock;
    }

    else {
        NSLog(@"getLowestDropLineBucketCoin could not retrieve first trade block");
        return nil;
    }

}


/**
 *  @runDropLineHeatMap
 */
-(void) runDropLineHeatMap : (block *) targetBlockCoin {

    NSArray * children = [self children];
    int i;

    for (i=0; i< [children count]; i++) {
    
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            dropLine * tempDropLine = (dropLine *) [children objectAtIndex:i];
            [tempDropLine runDropLineHeatMap:targetBlockCoin];        
        }
    }


}

-(void) hideDropLineHeatMap {
    
    NSArray * children = [self children];
    int i;
    
    for (i=0; i< [children count]; i++) {
        
        if ([[children objectAtIndex:i] isKindOfClass:[dropLine class]]) {
            dropLine * tempDropLine = (dropLine *) [children objectAtIndex:i];
            [tempDropLine hideDropLineHeatMap];
        }
    }

    
}


/**
 * @sets bucket on the rigth position
 */
-(void) setUpPosition {
    
    float padding = 30;
    float position_x = parentScene.frame.origin.x + parentScene.frame.size.width/2; //parentScene.frame.origin.x;
    float position_y = parentScene.frame.origin.y + parentScene.frame.size.height - padding; //parentScene.frame.origin.y + self.frame.size.height;
    [self setPosition:CGPointMake(position_x, position_y)];
    
    dropLineBucketType = @"up";
    moneyBucket = FALSE;

    [self addDropLines];
    // After size is defined.
    [self setInteraction];
}

/**
 *  @sets bucket on the left position
 */
-(void) setDownPosition {
    
    float position_x = parentScene.frame.origin.x + parentScene.frame.size.width/2;
    float position_y = parentScene.frame.origin.y + parentScene.frame.size.height/2 - parentScene.frame.size.height/7; //- parentScene.frame.size.height/2 + self.frame.size.height/2 -10;
    [self setPosition:CGPointMake(position_x, position_y)];

    dropLineBucketType = @"bottom";
    moneyBucket = TRUE;
    
    [self addDropLines];
    // After size is defined.
    [self setInteraction];
}




@end
