//
//  percentageBar.h
//  blockGame
//
//  Created by daniel.oliveira on 20/10/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

#import "Constant.h"

@interface percentageBar : SKSpriteNode {
    
    NSTimer * profitTimer;
    NSString * labelTotalText;
    int allBarWidth;
    
    NSString * positiveSignal;
    BOOL positivePortfolio;
    BOOL tradeMadeProfit;
    
    float moneyValue;
    
    float profitLabel_x_space;
    
    float portfolioInitialValue;
    float portfolioLastValue;
    float portfolioFloatingValue;
    
    float labelPercentageCash;
    float labelPercentageBitcoin;
    
    float profitPercentage;
    // For all percentage bar
    SKSpriteNode * moneyBar;
    SKSpriteNode * bitcoinBar;
    SKSpriteNode * profitBar;
    SKSpriteNode * lossBar;
    
    SKSpriteNode * moneyContainer;
    SKSpriteNode * bitcoinContainer;
    SKSpriteNode * profitContainer;
    
    
    
  
    SKLabelNode * percentageLabel;
    
    SKLabelNode * profitLabel;
    SKLabelNode * cashLabel;
    SKLabelNode * bitcoinLabel;
    
    SKSpriteNode * percentageBarInside;
    SKSpriteNode * breakBar;
    
    // limits up to 100 blocks
    int numberOfUnits;
    int percentageLimit;
    float percentageAdd;
    
    int scaleValue;
    float currentTradeValueScale;
    
    
    //Actions
    SKAction *fadeOut;
    SKAction *fadeIn;
    SKAction *pulse;
    SKAction *pulseThreeTimes;
    
    // key position
    int label_position_y;
    
}

-(void) addLabel;
-(void) setMenuBar;
-(void) refreshMenuBar;
-(void) setupPercentageBarForMenu: (NSString *) uniqueName;
//-(void) addUnit;
//-(void) removeUnit;
-(void) fillBar : (float) initialNumberOfUnits;

@end
