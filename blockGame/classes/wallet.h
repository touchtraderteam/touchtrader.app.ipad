//
//  block.h
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constant.h"

#import "block.h"
#import "blockBucket.h"

@class block;

@interface wallet : SKSpriteNode {
    

    NSString * moneyImageName;
    NSString * bitcoinImageName;
    SKSpriteNode * walletImage;
    SKSpriteNode * walletOpenFrame;
    
    NSString * walletType;
    NSMutableArray * walletBitcoins;
    /*NSMutableArray * displayWalletBitcoins; */
    
    NSArray * breakOptions;
    
    BOOL isWalletOpen;
    BOOL isMoneyWallet;
    BOOL isBitcoinWallet;
    int openframe_width;

    
    //wallet money value
    float currentWalletValue;
    float currentWalletCoinsQuantity;
    
    
    //int line_break_y;
    int blockCoinSize;
    
    block * currentCoin;
    SKLabelNode * quantityLabel;
    SKSpriteNode * breakOptionsNode;
    
    float coin_quantity_value;
    float coin_unique_value;
    float money_wallet_start_value;
    float bitcoin_wallet_start_value;
    
    SKSpriteNode * walletHeatMap;
    SKShapeNode *shapeNode;
    
}


- (BOOL) isMoneyWallet;
- (BOOL) isBitcoinWallet;

-(void) runHeatMap : (block *) targetBlockCoin;
-(void) hideHeatMap;

- (void) setMode :  (NSString *) modeDefinition;
- (void) walletBreakSelected : (SKNode *) optionTouched;

//- (void) openWallet;
- (void) closeWallet;
- (BOOL) isWalletOpen;
- (void) fadeoutCoin;
- (void) addCoin : (block *) blockCoin;
- (block *) getCoin;
- (void) popupCoin;
- (void) removeCoin;
- (void) removeQuantity : (float) coinQuantity;


@end
