//
//  block.h
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constant.h"
#import "factory.h"
#import "dropLine.h"
#import "blockCoin.h"

#import "gameContainer.h"

@class blockBucket;

@interface block : SKSpriteNode {

    // Style related.
    UIColor * defaultColor;
    CGSize defaultSize;
    
    // tempClonedCoins for trade
    NSMutableArray * clonedCoins;
    
    // Bitcoin inside Values
    float originalBlockValue;
    float bitcoinValuePercentage;
    float blockInstanceValue;
    float blockQuantity;
    float fullBlockValue;
    
    // All the values (instance value and quantity)
    float fullBlockQuantityTradeValue;
    float blockSizeOriginal;
    
    // State and control related
    
    // Quantity 3d bar
    SKSpriteNode * quantityLongBar;
    // IS state
    BOOL isIncompleteBlock;
    BOOL isBitCoin;
    BOOL isDropLineBlock;
    BOOL isWalletCoin;
    BOOL isBucketBlock;
    BOOL isBidLineBlock;
    
    // Is condition
    BOOL insideBucket;
    BOOL insideDropLine;
    BOOL isDoingTrade;
    BOOL isLastDoingTrade;
    BOOL firstSetValue;
    
    SKLabelNode * priceLabel;
    SKSpriteNode * potentialProfitWrapper;
    SKLabelNode * potentialProfit;
    SKSpriteNode * quantityBreakLine;
    SKSpriteNode * heatMapEffect;
    
    float isDoingTradeForValue;
    float numberOfBlocksCoinsBeingTraded;
    
    // Control related
    NSString * currentContainer;
    NSString * blockType;

    // Textures
    SKSpriteNode * coinAlphaBackground;
    SKCropNode * coinCropNode;
    SKSpriteNode * coinTextureImage;
    SKSpriteNode * coinTextureMask;
    BOOL hasShadows;
 
    //
    SKAction * blockCoinFullSlideAction;
    SKAction * slideDropLine;
    
    
    SKSpriteNode * quantityEffectQuantityLabelBackground;
    SKLabelNode * quantityEffectQuantityLabel;
    
    // Interspread trading
    SKLabelNode * bidLabel;
    SKLabelNode * fixedBidLabel;
    
    
    float startBidContactPrice;
    float dynamicBidPrice;
    
    /* When the user releases the bitcoin , this is the price assesed */
    float fixedBidPrice;
    
}


/**
 *  Initial value.
 */
-(void) setup :  (NSString *) uniqueName;

/**
 *  Interspread Trading
 */
- (void) setBidMode: (float) topPoint : (float) bottomPoint : (float) currentContactPoint;
- (void) continueBidMode : (float) topPoint : (float) bottomPoint : (float) currentContactPoint;
- (void) dropBidMode;

- (float) getDynamicBidPrice;
- (float) getFixedBidPrice;
- (void) setFixedBidMode : (float) fixedBidPrice;

- (float) getBitcoinTradePercentalValue;
- (void) showHeatMap : (float) compareTargetBlockCoinValue;
- (void) hideHeatMap;

// get Method
- (float) getTradeValue;
- (float) getTradeQuantityValue;
- (float) getPercentualValue;
- (float) getTradeInstanceValue;
- (float) getOriginalBlockValue;
- (float) getBlockTradeDifference : (float) lastTradeDone;
- (float) getFullQuantityTradeValue;

// get a bitcoin/money image
-(SKSpriteNode *) getBlockCoinImage;


// Trade related.
- (float) getNumberOfBlockCoinsBeingTraded;
//- (void) resetBlockTradeStatus;
- (float) getBlockBeingTradeValue;

// State methods
- (BOOL) isInside: (NSString *) currentContainer_p;
- (BOOL) isBucketBlock;
- (BOOL) isBitCoinBlock;
- (BOOL) isMoneyBlock;
- (BOOL) isIncompleteBlock;
- (BOOL) isWalletCoin;
- (BOOL) isDropLineBlock;
- (BOOL) isBidLineBlock;

- (NSString *) getType;

- (void) showPotentialProfit  : (float) valueToTradeCoin;
- (void) hidePotentialProfit;

-(SKSpriteNode *) getQuantityLongBar;


- (void) showHeatMapBlockBucket : (float) compareTargetBlockCoinValue;
/**
 * Set related methods
 */
//- (void) setTradeValueAndSize : (float) blockInstanceValue_p : (float) blockQuantity_p;
- (void) updateCoinValueSettings : (float) blockInstanceValue_p : (float) blockQuantity_p;

- (void) stackTradeValueAndSize : (float) blockInstanceValue_p : (float) blockQuantity_p;
- (void) setCoinType : (NSString *) blockType_p;
- (void) setInsideContainer : (NSString *) currentContainer_p;
//- (void) setGameColorFade : (float) duration;
- (void) setDoingTrade : (BOOL) tradeStatus : (float) blockBucketTradeValue;
- (void) setLastDoingTrade : (BOOL) lastDoingTrade_p;
//- (void) setTappedColorFade;
- (void) setDropLineBlock : (BOOL) isDropLineBlock_p;
- (void) setBidLineBlock : (BOOL) isBidLineBlock_p : (NSString *) type ;
- (void) setBucketBlock : (BOOL) isBucketBlock_p;

// Action on values.
- (void) doFullTrade;
- (void) doPartialTrade;
- (void) doPartialMoneyTrade: (float) percentage;

// Status of contact with dropline
- (BOOL) isDoingTrade;
- (BOOL) isLastDoingTrade;


// Collisions.
- (void) setCollisionInteraction;
- (void) setWalletCoinCollisionInteraction;
- (void) setDropLineCoinCollisionInteraction;
- (void) removeCollisionInteraction;
//-(void) setBundleCollisionInteraction;

// Animations.
- (void) pulseBlockForever;
- (void) pulseBlockTimes : (int) numberOfTimes : (float) timeDuration;
- (void) resetPulse;
- (void) attachPriceLabel;
- (void) resetSpin;

- (void) blockHeadCoinZoomUpAction;
- (void) blockHeadCoinZoomDownAction;

- (void) blockZoomUpAction;
- (void) blockZoomDownAction;
- (void) resetDropLineRotation : (float) rotateValue : (float) rotateTime;

- (void) addOneCoin : (block *) oneCoin;

@end
