//
//  walletInterface.h
//  touchTrader
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constant.h"


@interface timer : SKSpriteNode {
  
    SKSpriteNode * timerBar;
    SKSpriteNode * timerBarInside;
    
    SKSpriteNode * timerCircle;
    SKAction * reduceBar;
    
    CGSize defaultSize;
    
    float seconds;
}

/**
 *  @singleton
 */
+ (timer *)singleton;


/**
 *  @startTimer : (NSString *) type : (float) seconds
 */
- (timer *) getTimer : (NSString *) timerType : (float) seconds : (CGPoint) position_p;

/**
 *  @startTimer
 */
- (void) startTimer : (float) timer_duration;
- (void) stopTimer;

@end
