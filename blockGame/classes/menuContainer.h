//
//  menuContainer.h
//  blockGame
//
//  Created by daniel.oliveira on 29/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "percentageBar.h"
#import "factory.h"
#import "mainScene.h"
#import "menuButton.h"

@class mainScene;




@interface menuContainer : SKSpriteNode {
    
    percentageBar * performanceBar;
    UIColor * defaultColor;
    CGSize defaultSize;
    NSString * uniqueName;
    SKLabelNode * block_info;
    
    
    menuButton * bidAskButton;
    menuButton * bitcoinTrendButton;
    menuButton * heatmapButton;
    menuButton * otherButton;
    menuButton * helpButton;

    
    SKSpriteNode * splitBar;
    
    SKSpriteNode * topBar;
    
    // menuContainers
    SKSpriteNode * userTable;
    SKSpriteNode * performanceTable;
    
    CGSize defaultMenuInfoSize;
}


+ (menuContainer *)singleton;
- (menuContainer *) initialSetup: (SKScene *) parentFrame;


-(void) displayMenuSection : (NSString *) option;
- (void) setBlockInfo : (block *) selectedBlock;
- (percentageBar *) getPortfolioBar;


-(void) setPerfomanceBar;
-(void) refreshPerfomanceBar;

@end
