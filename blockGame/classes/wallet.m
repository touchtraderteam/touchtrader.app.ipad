//
//  block.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "wallet.h"
#import <SpriteKit/SpriteKit.h>



@implementation wallet {
    
    
}

-(id) init {
    self = [super init];
    
    if(self)
    {
        moneyImageName = @"moneyWalletImage";
        bitcoinImageName = @"bitcoinWalletImage";
        isWalletOpen = false;
        coin_unique_value = BLOCK_BUCKET_TRADE_UNIQUE_VALUE;
        
    }
    return self;
}


/**
 * @walletBreakSelected
 */
- (void) walletBreakSelected : (SKNode *) optionTouched {
    
    if ([optionTouched isKindOfClass:[SKLabelNode class]]) {
        SKLabelNode * selectedLabel = (SKLabelNode *) optionTouched;
        coin_unique_value = [selectedLabel.text intValue] * 1000;
        [self updateMoneyQuantityLabel];
    }
}

/**
 *  @isWalletOpen
 */
-(BOOL) isWalletOpen {
    return isWalletOpen;
}

/**
 *  @closeWallet
 */
-(void) closeWallet {
    
    isWalletOpen = false;
    [breakOptionsNode runAction:[SKAction fadeOutWithDuration:0.1] completion:^ {
       [walletOpenFrame runAction:[SKAction scaleYTo:2 duration:0.2]];
    }];
}


/**
 *  @hasHeatMapValue
 */
-(BOOL) hasHeatMapValue {
    
    if (bitcoin_wallet_start_value > 0) {
        if ([self isBitcoinWallet]) {
            return TRUE;
        }
    }
    
    return FALSE;
    
}


/**
 *  @updateHeatMap
 */
-(void) updateHeatMap {
     if (![self hasHeatMapValue]) {
         [self hideHeatMap];
     }
}

/**
 *  @runHeatMap
 */
-(void) runHeatMap : (block *) blockTargetCoin {
    
    BOOL profit;
    
   
    
    if (![self hasHeatMapValue]) {
        if (coin_unique_value > [blockTargetCoin getTradeInstanceValue]) {
            profit = FALSE;
        }
        else {
            profit = TRUE;
        }
    }

    if (walletHeatMap == nil) {
        
        float width = self.frame.size.width;
        float height = self.frame.size.height;
        
        CGRect circle = CGRectMake(0 - self.frame.size.width/2, 0-self.frame.size.height/2, width,height);
        shapeNode = [[SKShapeNode alloc] init];
        
        shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
        walletHeatMap.zPosition = -10;
        shapeNode.zPosition = -10;
        shapeNode.lineWidth = 10;
        
        shapeNode.antialiased = TRUE;
        
        walletHeatMap = [[SKSpriteNode alloc]init];
        walletHeatMap.alpha = 0;
        walletHeatMap.size = CGSizeMake(shapeNode.frame.size.width,shapeNode.frame.size.height);
        walletHeatMap.zPosition = HEATMAP_CIRCLE_ZPOSITION;
        walletHeatMap.position = CGPointMake(0, 0);
        walletHeatMap.anchorPoint = CGPointMake(0,0);
        NSString * uniqueName = [NSMutableString stringWithFormat:@"%@_heatmap", WALLET_PREFIX];
        walletHeatMap.name = uniqueName;

        [walletHeatMap addChild:shapeNode];
        [self addChild:walletHeatMap];
    }
    
    if (profit) {
        shapeNode.strokeColor = HEATMAP_CIRCLE_PROFIT_COLOR;
        shapeNode.fillColor = HEATMAP_CIRCLE_PROFIT_COLOR;
    }
    else {
        shapeNode.strokeColor = HEATMAP_CIRCLE_LOSS_COLOR;
        shapeNode.fillColor = HEATMAP_CIRCLE_LOSS_COLOR;
    }
    
    if (coin_unique_value > 0) {
        [walletHeatMap runAction:[SKAction fadeAlphaTo:0.8 duration:0.1]];
    }
    
    else {
        
    }
    
}


/**
 *  @hideHeatMap
 */
-(void) hideHeatMap {
    
    [walletHeatMap runAction:[SKAction fadeOutWithDuration:0.2]];

}

/**
 *  @addMoneyQuantityLabel
 */
- (void) addMoneyQuantityLabel {
    
    money_wallet_start_value = MONEY_WALLET_START_VALUE;
    coin_quantity_value = money_wallet_start_value/BLOCK_BUCKET_TRADE_UNIQUE_VALUE;
    quantityLabel = [[SKLabelNode alloc]init];
    
    float money_wallet_start_value_temp = money_wallet_start_value/1000;
    
    quantityLabel.fontName = SLICK_OPTIMA_BOLD;
    quantityLabel.text = [NSString stringWithFormat:@"%@ %.1f K",MONEY_CURRENCY, money_wallet_start_value_temp];
    quantityLabel.fontColor = SLICK_WALLET_LABEL_COLOR;
    quantityLabel.fontSize = 16;
    quantityLabel.position = CGPointMake(0, -40);
    
    [self addChild:quantityLabel];
}


/**
 *  @addBitcoinQuantityLabel
 */
-(void) addBitcoinQuantityLabel {
    
    bitcoin_wallet_start_value = BITCOIN_WALLET_START_VALUE;
    coin_quantity_value = BITCOIN_WALLET_START_VALUE;
    
    quantityLabel = [[SKLabelNode alloc]init];
    quantityLabel.fontName = SLICK_OPTIMA_BOLD;
    quantityLabel.text = [NSString stringWithFormat:@"%d x %@", BITCOIN_WALLET_START_VALUE, BITCOIN_CURRENCY];
    quantityLabel.fontColor = SLICK_WALLET_LABEL_COLOR;
    quantityLabel.fontSize = 16;
    quantityLabel.position = CGPointMake(0, -40);
    
    [self addChild:quantityLabel];
    
}


/**
 *  @updateBitcoinQuantityLabel
 */
- (void) updateBitcoinQuantityLabel {

    quantityLabel.text = [NSString stringWithFormat:@"%.2f x %@", bitcoin_wallet_start_value, BITCOIN_CURRENCY];
}

/**
 *  @updateMoneyQuantityLabel
 */
-(void) updateMoneyQuantityLabel {

    float money_wallet_start_value_temp = money_wallet_start_value/1000;
    quantityLabel.text = [NSString stringWithFormat:@"%@ %.1f K",MONEY_CURRENCY, money_wallet_start_value_temp];
   
}



/**
 *  @setCollisionInteraction
 */
-(void) setWalletCollison :  (NSString *) modeDefinition {
    
    // Physics
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
    self.physicsBody.dynamic = YES;
    
    // It's own category.
    self.physicsBody.categoryBitMask = wallet_money_category;
    
    // Notify when colliding with these categories.
    self.physicsBody.contactTestBitMask = bucket_category;
    self.physicsBody.contactTestBitMask = block_category;
    
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;
    self.anchorPoint = CGPointMake(0.5,0.5);
    
}



/**
 * @setMode
 */
-(void) setMode : (NSString *) modeDefinition {
    
    if ([modeDefinition isEqualToString:MONEY_BUCKET_ID]) {
        [self setMoneyMode];
        [self addMoneyQuantityLabel];
    }
    
    else if ([modeDefinition isEqualToString:BITCOIN_BUCKET_ID]) {
        [self setBitcoinMode];
        [self addBitcoinQuantityLabel];
    }

    else {
        NSLog(@"Invalid mode for wallet setMode function.");
    }
    
    [self setWalletCollison:modeDefinition];
    
}


/**
 *
 */
-(float) getPopupCoinValue {
    
    blockBucket * tempBucket = (blockBucket *) [self parent];
    
    if (![tempBucket bucketIsFull] && [[tempBucket getChildBlocks] count] == BLOCK_BUCKET_VISIBLE_QUANTITY) {
        block * tempBlock = [tempBucket getIncompleteBlockOfBucket];
        return [tempBlock getTradeInstanceValue];
    }
    
    return 0;
}

/**
 *  @popupCoin
 */
-(void) popupCoin {
    
    currentCoin = [[block alloc]init];
    NSMutableString * uniqueName;
    
    BOOL walletEmpty = FALSE;
    if (isBitcoinWallet) {
        if (bitcoin_wallet_start_value <= 0) {
            walletEmpty = TRUE;
        }
        uniqueName = [NSMutableString stringWithFormat:@"%@_bitcoin_%d", BLOCK_WALLET_PREFIX, 1];
        if (bitcoin_wallet_start_value < 1) {
            coin_unique_value = bitcoin_wallet_start_value * bitcoin_reference_lowest_value;
        }
        else {
            coin_unique_value = bitcoin_reference_lowest_value;
        }
    }
    
    else {
        if (money_wallet_start_value <=0) {
            walletEmpty = TRUE;
        }
        
        float oneCoinValue = [self getPopupCoinValue];
        
        if (coin_unique_value <= 0) {
            coin_unique_value = BLOCK_BUCKET_TRADE_UNIQUE_VALUE;
        }
        
        if (oneCoinValue != 0) {
            coin_unique_value = coin_unique_value - oneCoinValue;
        }
        else {
            coin_unique_value = BLOCK_BUCKET_TRADE_UNIQUE_VALUE;
        }
        uniqueName = [NSMutableString stringWithFormat:@"%@_money_%d", BLOCK_WALLET_PREFIX, 1];
    }
    
    if (walletEmpty) {
        [[gameEventZone singleton] displayInfoTime:@"Your wallet is empty" :1];
    }
    
    else {
        
        [currentCoin setup:uniqueName];
        [currentCoin setCoinType:walletType];
        [currentCoin updateCoinValueSettings:coin_unique_value :1];
        [currentCoin setWalletCoinCollisionInteraction];
        
        CGPoint point = CGPointMake(0, 10);
        [currentCoin setPosition:point];
        currentCoin.zPosition = 200;
        [currentCoin setAlpha:0];
        
        [self addChild:currentCoin];
        [currentCoin runAction:[SKAction fadeInWithDuration:0.2]];
        
    }
    
}


/**
 *
 */
-(void) fadeoutCoin {
    
    [currentCoin runAction:[SKAction fadeOutWithDuration:0.4] completion:^{
        [currentCoin removeFromParent];
    }];
}


/**
 *  @addCoin
 */
-(void) addCoin : (block *) blockCoin {
    
   
    if ([self isBitcoinWallet]) {
        float bitcoin_percentage = [blockCoin getTradeInstanceValue] / bitcoin_reference_lowest_value;
        bitcoin_wallet_start_value = bitcoin_wallet_start_value + bitcoin_percentage;
        [self updateBitcoinQuantityLabel];
    }
    
    else if ([self isMoneyWallet]) {
        money_wallet_start_value = money_wallet_start_value + [blockCoin getTradeInstanceValue];
        coin_quantity_value = money_wallet_start_value/coin_unique_value;
        [self updateMoneyQuantityLabel];
    }
    
   
    [blockCoin runAction:[SKAction fadeOutWithDuration:1] completion:^ {
        if ([[blockCoin parent] isKindOfClass:[blockBucket class]]) {
            blockBucket * bucketTemp = (blockBucket *) [blockCoin parent];
            [bucketTemp removeCoinFromBucket:blockCoin];
            [self updateHeatMap];
        }
    }];

}


/**
 *  @removeQuantity
 */
- (void) removeQuantity : (float) coinQuantity {
    
    if (isBitcoinWallet) {
        quantityLabel.text = [NSString stringWithFormat:@"%d x %@", BITCOIN_WALLET_START_VALUE, BITCOIN_CURRENCY];
    }
    
    else if (isMoneyWallet) {
        money_wallet_start_value = money_wallet_start_value - (coinQuantity * 1000);
        coin_quantity_value = money_wallet_start_value/coin_unique_value;
        float money_wallet_start_value_temp = money_wallet_start_value/1000;
        quantityLabel.text = [NSString stringWithFormat:@"%@ %.1f K",MONEY_CURRENCY, money_wallet_start_value_temp];
    }

}


/**
 *  @removeCoin
 */
- (void) removeCoin {

    if (currentCoin !=nil) {
        
        
        if ([self isBitcoinWallet]) {
            float coin_percentage = [currentCoin getTradeInstanceValue] /bitcoin_reference_lowest_value;
            bitcoin_wallet_start_value = bitcoin_wallet_start_value - coin_percentage;
            [self updateBitcoinQuantityLabel];
            
            
        }
        
        else if ([self isMoneyWallet]) {
          money_wallet_start_value = money_wallet_start_value - [currentCoin getTradeInstanceValue];
          coin_quantity_value = money_wallet_start_value/coin_unique_value;
            [self updateMoneyQuantityLabel];
        }
        
        
        

        [currentCoin runAction:[SKAction fadeOutWithDuration:0.4] completion:^{
            [currentCoin removeFromParent];
            [self updateHeatMap];
            
        }];
        
    }
 
}


/**
 *
 */
-(block *) getCoin {
    
    if (currentCoin != nil) {
       return currentCoin;
    }
    return FALSE;
}


/**
 *  @isMoneyWallet
 */
-(BOOL) isMoneyWallet {
    return isMoneyWallet;
}

/**
 *  @isBitcoinWallet
 */
-(BOOL) isBitcoinWallet {
    return isBitcoinWallet;
}


/**
 *  @setMoneyMode
 */
-(void) setMoneyMode {
    
    isMoneyWallet = TRUE;
    isBitcoinWallet = FALSE;
    walletType = MONEY_WALLET_NAME;
    walletImage = [SKSpriteNode spriteNodeWithImageNamed:moneyImageName];
    walletImage.name =  MONEY_WALLET_NAME;
    walletImage.zPosition = 10;
    [self addChild:walletImage];
    CGSize size = CGSizeMake(60,45);
    [walletImage setSize:size];
    [self setSize:size];

}



/**
 *  @setBitcoinMode
 */
-(void) setBitcoinMode {
    
    isBitcoinWallet = TRUE;
    isMoneyWallet = FALSE;
    walletType = BITCOIN_WALLET_NAME;
    walletImage = [SKSpriteNode spriteNodeWithImageNamed:bitcoinImageName];
    walletImage.name = BITCOIN_WALLET_NAME;
    walletImage.zPosition = 10;
    CGSize size = CGSizeMake(60,45);
    [walletImage setSize:size];
    [self setSize:size];
    [self addChild:walletImage];
   
}


@end
