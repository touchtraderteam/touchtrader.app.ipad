//
//  dataManager.h
//  TouchTrader
//
//  Created by Mac on 21/4/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dataManager : NSObject {
    
    NSString * tradeDataFile;
    NSString * tradeDataPath;
    
    NSMutableDictionary * tradeData;
    
    NSMutableDictionary * bid_extreme_values;
    NSMutableDictionary * ask_extreme_values;
    
    
    // JSON And Data related
    NSMutableArray * bidData;
    NSMutableArray * askData;
    
    //convert object to data
    NSData* jsonAskData;
    NSData* jsonBidData;
    
    NSString * jsonBidEncodedData;
    NSString * jsonAskEncodedData;

    
}

// Unique singleton
+ (dataManager *)singleton;

-(void) setupTradeDataJson;




// Generic functions
//- (BOOL) loadTradeData;

- (NSMutableArray *) getBidTradeData;
- (NSMutableArray *) getAskTradeData;

-(int) getAskEventsQuantity;
-(int) getBidEventsQuantity;

- (NSDictionary *) getTradeValue: (NSString *) tradeKey : (NSString *) tradeType;

//- (NSDictionary *) getTradeDataWithInterval: (NSString *) tradeKey : (int) start : (int) limit;
- (NSMutableArray *) getTradeDataWithInterval: (NSString *) tradeKey : (int) start : (int) limit;


-(float) getExtremeValues : (NSString *) tradeKey : (NSString *) option;
-(void) setFirstBlocksReferenceValue;





@end
