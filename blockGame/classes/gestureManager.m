//
//  gestureManager.m
//  blockGame
//
//  Created by daniel.oliveira on 26/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "gestureManager.h"

@implementation gestureManager {
    
    
}

static gestureManager *singleton = nil;

+ (gestureManager *)singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
    }
    return singleton;
}

- (id)init {
    if ( (self = [super init]) ) {
        ready = FALSE;
    }
    return self;
}


/**
 *
 */
- (void) initialSetup : (SKScene *) parentScene_p {
    
    parentScene = parentScene_p;
    ready = TRUE;
    


}




// Pinching in and out (zoom or view)
- (void)pinchGesture:(UIPinchGestureRecognizer *)recognizer {
    
}

// Tapping (any number of taps)
-(void) tapGesture:(UITapGestureRecognizer *) recognizer {
    
}

// Swiping (in any direction)
-(void) swipeGesture:(UISwipeGestureRecognizer *) recognizer {
    
}

// Rotate (fingers moving in opposite directions)
-(void) rotateGesture:(UIRotationGestureRecognizer *) recognizer {
    
}

// Long press ('also know as touch and hold')
-(void) longPressGesture:(UILongPressGestureRecognizer *) recognizer {
    
}




/**
 *  @Gesture recognizers: pan gesture (DRAG)
 */
- (void)panGesture:(UIPanGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        NSLog(@"Gesture state began");
        //CGPoint touchLocation = [recognizer locationInView:recognizer.view];
        
        //touchLocation = [pare convertPointFromView:touchLocation];
        
       // [self selectNodeForTouch:touchLocation];
        
    }
    
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        
        NSLog(@"Gesture state changed");
        /*
         CGPoint translation = [recognizer translationInView:recognizer.view];
         translation = CGPointMake(translation.x, -translation.y);
         [self panForTranslation:translation];
         [recognizer setTranslation:CGPointZero inView:recognizer.view];
         */
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        NSLog(@"Gesture state ended");
        //[_selectedNode removeAllActions];
        
    }
}

@end
