//
//  soundPlayer.h
//  TouchTrader
//
//  Created by Mac on 21/4/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>

@import AVFoundation;

@interface soundPlayer : SKNode {
    
    NSURL * tradeSound;
    NSURL * cancelSound;
    NSURL * positiveSound;
    NSURL * negativeSound;
    NSString * backgroundSoundURL;
    
    
    SKAction * tradeSoundAction;
    SKAction * cancelSoundAction;
    SKAction * positiveSoundAction;
    SKAction * negativeSoundAction;
    

    
    BOOL soundsLoaded;
    
}

@property (nonatomic) AVAudioPlayer * backgroundMusicPlayer;
@property (nonatomic) AVAudioPlayer * actionPlayer;

// Unique singleton
+ (soundPlayer *)singleton;

-(void) loadSounds;
-(void) playBackgroundMusic;
-(void) playTradeSuccess;


@end
