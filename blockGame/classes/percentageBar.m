//
//  percentageBar.m
//  blockGame
//
//  Created by daniel.oliveira on 20/10/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "percentageBar.h"

@implementation percentageBar {
    
    
}

-(id) init {
    self = [super init];
    
    if(self) {

    }
    
    return self;
}


/**
 *  @setupPercentageBarForMenu
 */
-(void) setupPercentageBarForMenu: (NSString *) uniqueName {
    
    CGPoint locationPoint = CGPointMake(0-260, 0-10);
    
    allBarWidth = 400;
    CGSize barSize = CGSizeMake(allBarWidth,35);
    
    [self setName:uniqueName];
    [self setPosition:locationPoint];
    [self setSize:barSize];
    [self setAnchorPoint:CGPointZero];
    
    labelPercentageCash = 100;
    labelPercentageBitcoin = 0;
    
    labelTotalText = @"TOTAL ACTIVE VALUE (HKD): ";
    
    // Setup inside bitcoin and money bars
    
    moneyContainer = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:barSize];
    bitcoinContainer = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:barSize];
    profitContainer = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:barSize];
    
    
    moneyBar = [SKSpriteNode spriteNodeWithColor:MONEY_GREEN_COLOR size:barSize];
    bitcoinBar = [SKSpriteNode spriteNodeWithColor:BITCOIN_BLUE_COLOR size:barSize];
    
    
    lossBar = [SKSpriteNode spriteNodeWithColor:LOSS_COLOR size:CGSizeMake(allBarWidth,20)];
    profitBar = [SKSpriteNode spriteNodeWithColor:PROFIT_YELLOW_COLOR size:CGSizeMake(0, 35)];
    
    [moneyBar setName:@"moneyBar"];
    [bitcoinBar setName:@"bitcoinBar"];
    [profitBar setName:@"profitBar"];
    [lossBar setName:@"lossBar"];
    
    [moneyBar setAnchorPoint:CGPointZero];
    [bitcoinBar setAnchorPoint:CGPointZero];
    [profitBar setAnchorPoint:CGPointZero];
    [lossBar setAnchorPoint:CGPointZero];
    
    fadeOut = [SKAction fadeOutWithDuration: 0.4];
    fadeIn = [SKAction fadeInWithDuration: 0.4];
    pulse = [SKAction sequence:@[fadeOut,fadeIn]];
    pulseThreeTimes = [SKAction repeatAction:pulse count:2];
    
    [moneyBar setPosition:CGPointMake(0, -15)];
    [bitcoinBar setPosition:CGPointMake(0, -15)];
    
    //
    [profitBar setPosition:CGPointMake(0 + profitContainer.frame.size.width, -15)];
    [lossBar setPosition:CGPointMake(0, -8)];
   
    
    /** LABELS **/
    cashLabel = [[SKLabelNode alloc]init];
    bitcoinLabel = [[SKLabelNode alloc]init];
    cashLabel.text = @"CASH";
    bitcoinLabel.text = @"BITCOIN";
    [self setLabelConf:cashLabel];
    [self setLabelConf:bitcoinLabel];
    
    bitcoinLabel.hidden = TRUE;
    
    // -- ZPositions ---
    moneyContainer.zPosition = 100;
    bitcoinContainer.zPosition = 50;
    profitContainer.zPosition = 150;
    
    moneyBar.zPosition = moneyContainer.zPosition + 1;
    bitcoinBar.zPosition = bitcoinContainer.zPosition +1;
    profitBar.zPosition = profitContainer.zPosition + 1;
    lossBar.zPosition = - 100;
    
    cashLabel.zPosition = moneyContainer.zPosition + 2;
    bitcoinLabel.zPosition = moneyContainer.zPosition + 100;

    label_position_y = self.frame.origin.y + self.frame.size.height;
    
    [self addChild:cashLabel];
    cashLabel.zPosition = 200;
    [self addChild:bitcoinLabel];

    [bitcoinContainer addChild:moneyBar];
    [bitcoinContainer addChild:bitcoinBar];

    
    [bitcoinContainer setAlpha:1];
    [moneyContainer setAlpha:1];
    

    
    [profitContainer addChild:lossBar];
    [profitContainer addChild:profitBar];


    [self addPerformanceBarBreakLabel];
    [self addLabel];
    [self addChild:moneyContainer];
    [self addChild:bitcoinContainer];
    [self addChild:profitContainer];
    [self addProfitBarLabel];
    
    //Positions
    [bitcoinLabel setPosition:CGPointMake(0+self.frame.size.width - (bitcoinLabel.frame.size.width/2 + 10), -5)];
  
}


/**
 *  @setLabelConf
 */
-(void) setLabelConf : (SKLabelNode *) label {
    
    [label setColor:[UIColor whiteColor]];
    [label setFontSize:20];
    [label setAlpha:0.7];
    [label setFontName:SLICK_OPTIMA_REGULAR];
    [label setPosition:CGPointMake(0+label.frame.size.width/2+3,-5)];
    
    [label setUserInteractionEnabled:false];

}


/**
 *  @refreshMenuBar
 */
-(void) setMenuBar {
    
    float total_value = money_bucket_current_net_value + bitcoin_bucket_current_net_value;
    float money_width = self.frame.size.width * money_bucket_current_net_value / total_value;
    CGSize moneySize = CGSizeMake(money_width,self.frame.size.height);
    
    [moneyBar runAction:fadeIn];
    [bitcoinBar runAction:fadeIn];
    [moneyBar setSize:moneySize];
    
}


/**
 *  @refreshMenuBar
 */
-(void) refreshMenuBar {
    
    NSLog(@"----- refresh Menu Bar");
    NSLog(@"MONEY CURRENT VALUE %f", money_bucket_current_net_value);
    NSLog(@"BITCOIN CURRENT VALUE %f", bitcoin_bucket_current_net_value);
    
    
    // CALCULATIONS ---------------
    float total_value = money_bucket_current_net_value + bitcoin_bucket_current_net_value;
    float money_width_scale =  money_bucket_current_net_value / total_value;
    portfolioFloatingValue = money_bucket_current_net_value + bitcoin_bucket_current_net_value;
    float profitPercentage_label = 100 - (portfolioFloatingValue * 100 / portfolioInitialValue);
    float profit_width_scale = allBarWidth * profitPercentage_label/100;
    
    if (portfolioFloatingValue >= portfolioInitialValue) {
        profitLabel.fontColor = SLICK_PROFIT_GREEN;
        positiveSignal = @"+ ";
        positivePortfolio = TRUE;
        profitPercentage_label = fabs(profitPercentage_label);
    }
    else {
        profitLabel.fontColor = SLICK_PROFIT_RED;
        positiveSignal = @"- ";
        positivePortfolio = FALSE;
    }
    
    tradeMadeProfit = portfolioFloatingValue < portfolioLastValue ? FALSE:TRUE;
    
    
    
    // UI INTERFACE ----------------------------
    SKAction * slide = [SKAction scaleXTo:money_width_scale duration:1];
    [moneyBar runAction:slide];
    [self refreshLabel:total_value];
    profitLabel.text = [NSString stringWithFormat:@"%@%.2f%%",positiveSignal, profitPercentage_label];
    
    if (positivePortfolio) {
        
        if (moneyContainer.frame.size.width < allBarWidth) {
            [moneyContainer runAction:[SKAction scaleXTo:1  duration:1]];
            [bitcoinContainer runAction:[SKAction scaleXTo:1 duration:1]];
        }
        
        float profitNewWidth;
        
        if (profitNewWidth > allBarWidth/2) {
            profitNewWidth = allBarWidth/2;
        }
        else {
            profitNewWidth = profitPercentage_label * allBarWidth / 100;
        }
       
        SKAction * profitBarSlide = [SKAction resizeToWidth:profitNewWidth duration:1];
        [profitBar runAction:profitBarSlide];
        [profitLabel runAction:[SKAction moveToX:(profitBar.frame.origin.x + profitNewWidth + profitLabel_x_space) duration:1]];
     
    }
    
    else {
        
        // In case there was a profit bar before.
        if (profitBar.frame.size.width > 0) {
            [profitBar runAction:[SKAction resizeToWidth:0 duration:1]];
        }
        
        float newBarWidth = (allBarWidth - profit_width_scale);
        float decrease_scale;
        
        if (newBarWidth < (allBarWidth/2)) {
            decrease_scale = 0.5;
        }
        
        else {
            decrease_scale = newBarWidth / allBarWidth;
        }
        
        SKAction * lossBarSlide = [SKAction scaleXTo:decrease_scale duration:1];
        [moneyContainer runAction:lossBarSlide];
        [bitcoinContainer runAction:lossBarSlide];
        
    }
 
}


/**
 *  @updateProfitLabel
 */
-(void) updateProfitLabel : (NSTimer *)timer {
    
    profitPercentage ++;
    profitLabel.text =[NSString stringWithFormat:@"+ %.1f%%",profitPercentage];
    
    if (profitPercentage > 5) {
        [profitTimer invalidate];
    }
    
}


/**
 *  @refreshProfitLabel
 */
- (void) refreshProfitLabel {
    
    
    profitTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProfitLabel:)
                                            userInfo:nil
                                                  repeats:true];
}


/**
 *  @addProfitBarLabel
 */
-(void) addProfitBarLabel {
    
    profitPercentage = 0;
    profitLabel = [[SKLabelNode alloc]init];
    profitLabel.text = [NSString stringWithFormat:@"+ %.1f%%",profitPercentage];

    profitLabel_x_space = profitLabel.frame.size.width/2+3;
    [profitLabel setPosition:CGPointMake(profitBar.frame.origin.x + profitLabel_x_space, -4)];
    profitLabel.fontName = SLICK_OPTIMA_REGULAR;
    profitLabel.fontSize = 20;
    [profitLabel setColor:SLICK_PROFIT_GREEN];
    profitLabel.fontColor = SLICK_PROFIT_GREEN;
    profitLabel.zPosition = profitBar.zPosition + 10;
    
    [self addChild:profitLabel];
    
}


/**
 *  @addPerformanceBarBreakLabel
 */
-(void) addPerformanceBarBreakLabel {
    
    int position_x = 0 + moneyBar.frame.size.width;
    
    breakBar = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(5,self.frame.size.height+8)];
    [breakBar setPosition:CGPointMake(position_x, 0+breakBar.frame.size.height/3 - 11)];
    breakBar.zPosition = 200;
    [profitContainer addChild:breakBar];

}


/**
 *  @addLabel.
 */
-(void) addLabel  {
    
    percentageLabel = [[SKLabelNode alloc]init];
    moneyValue = MONEY_BUCKET_START_VALUE;
    portfolioInitialValue =  MONEY_BUCKET_START_VALUE;
    portfolioLastValue = MONEY_BUCKET_START_VALUE;
    
    NSString * totalValueString = [NSString stringWithFormat:@" %.2f",portfolioInitialValue];

    percentageLabel.text = [labelTotalText stringByAppendingString:totalValueString];
    percentageLabel.fontName = SLICK_OPTIMA_BOLD;
    percentageLabel.fontColor = [UIColor whiteColor];
    percentageLabel.fontSize = 18;


    int label_x = 0 + percentageLabel.frame.size.width/2;
    [self addChild:percentageLabel];
    [percentageLabel setPosition:CGPointMake(label_x,label_position_y)];

}


/**
 *  @refreshLabel
 */
-(void) refreshLabel : (float) value {
    
    NSString * totalValueString = [NSString stringWithFormat:@" %.2f",value];
    percentageLabel.text = [labelTotalText stringByAppendingString:totalValueString];
    
}


/**
 *  @fillBar
 */
-(void) fillBar : (float) initialNumberOfUnits {
    [percentageBarInside setSize:CGSizeMake(self.frame.size.width,self.frame.size.height)];
}



@end
