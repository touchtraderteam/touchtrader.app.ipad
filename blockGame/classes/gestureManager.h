//
//  gestureManager.h
//  blockGame
//
//  Created by daniel.oliveira on 26/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface gestureManager : NSObject {
    
    NSObject * parentScene;
    BOOL ready;
    
    UIPanGestureRecognizer *panGestureRecognizer;
    
}


- (void) initialSetup : (NSObject *) parentScene_p;

@property (nonatomic, strong) SKSpriteNode *selectedNode;

// Unique singleton
+ (gestureManager *)singleton;





// Panning or Dragging
- (void)panGesture:(UIPanGestureRecognizer *)recognizer;

// Pinching in and out (zoom or view)
- (void)pinchGesture:(UIPinchGestureRecognizer *)recognizer;

// Tapping (any number of taps)
-(void) tapGesture:(UITapGestureRecognizer *) recognizer;

// Swiping (in any direction)
-(void) swipeGesture:(UISwipeGestureRecognizer *) recognizer;

// Rotate (fingers moving in opposite directions)
-(void) rotateGesture:(UIRotationGestureRecognizer *) recognizer;

// Long press ('also know as touch and hold')
-(void) longPressGesture:(UILongPressGestureRecognizer *) recognizer;


@end

