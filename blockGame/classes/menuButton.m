//
//  block.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "menuButton.h"
#import <SpriteKit/SpriteKit.h>



@implementation menuButton {
    
    
    
}


-(id) init {
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

/**
 *  @position
 */
- (void) startButton : (NSString *) uniqueName : (NSString *) iconPath : (float) position_x  { //(CGPoint) position {
   
    button_width = 45;
    button_height = 45;
    isSelected = FALSE;
    
    menuButtonImage = [SKSpriteNode spriteNodeWithImageNamed:iconPath];
    [menuButtonImage setSize:CGSizeMake(35, 35)];
    [self setSize:CGSizeMake(button_width, button_height)];
    
    /*tile = [SKShapeNode node];
    [tile setPath:CGPathCreateWithRoundedRect(CGRectMake(-20, -20, button_width, button_height), 1, 1, nil)];
    
    tile.fillColor = SLICK_BLACK;
    tile.strokeColor = [UIColor whiteColor];
    tile.antialiased = true;
    */
    self.zPosition =200;
    
    
    int position_y = 0;

    [menuButtonImage setAnchorPoint:CGPointMake(0.5, 0.5)];
    //[tile addChild:menuButtonImage];

    [self addChild:menuButtonImage];
    
    [self setColor:SLICK_BLACK];
    
    [self setName:uniqueName];
    [tile setName:uniqueName];
    [menuButtonImage setName:uniqueName];
    
    
    [self setPosition:CGPointMake(position_x,position_y)];

  
}


/**
 *
 */
- (BOOL) isTapped {
    return tapped;
}

/**
 *
 */
-(void) tapButton {
    
    if (tapped == false) {
        [self setColor:[UIColor darkGrayColor]];

        tapped = TRUE;
    }
    
    else {
        [self setColor:SLICK_BLACK];

        tapped = FALSE;
    }
}

-(void) setButtonClicked : (BOOL) value {
    isSelected =  value;
}

- (BOOL) buttonIsClicked {
    return isSelected;
}


@end
