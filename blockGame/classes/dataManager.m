//
//  dataManager.m
//  TouchTrader
//
//  Created by Mac on 21/4/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import "dataManager.h"
#import "Constant.h"


@implementation dataManager {
    
       
}

static dataManager *singleton = nil;

+ (dataManager *)singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
        
        
    }
    return singleton;
}


/**
 *
 */
-(void) initialSetup {
    
    
}


/**
 *  @getBidTradeData
 */
-(NSMutableArray *) getBidTradeData {
    
    if ([bidData count] >0) {
      return bidData;
    }
    
    return nil;
    
}

/**
 *  @getAskTradeData
 */
-(NSMutableArray *) getAskTradeData {
    
    if ([askData count] >0) {
        return askData;
    }
    
    return nil;
    
}

/**
 *  @setupTradeDataJson
 */
-(void) setupTradeDataJson {
    
    bidData = [[NSMutableArray alloc]init];
    askData = [[NSMutableArray alloc]init];
    
    NSError * error;
    //build an info object and convert to json
    
    // MONEY
    bidData = [NSMutableArray arrayWithObjects:
               
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3110],@"hkd",
                [NSNumber numberWithFloat:8.3],@"btc",
                nil],
               
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3000.000],@"hkd",
                [NSNumber numberWithFloat:1.3],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2999.000],@"hkd",
                [NSNumber numberWithFloat:3.3],@"btc",
                nil],
               
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2998.8200],@"hkd",
                [NSNumber numberWithFloat:1.3],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2984.97900],@"hkd",
                [NSNumber numberWithFloat:3.3],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2980.52887],@"hkd",
                [NSNumber numberWithFloat:1.3],@"btc",
                nil],


               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2980.37845],@"hkd",
                [NSNumber numberWithFloat:3.3],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2969.0000],@"hkd",
                [NSNumber numberWithFloat:1.3],@"btc",
                nil],


               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2968.123],@"hkd",
                [NSNumber numberWithFloat:2],@"btc",
                nil],

               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2963.44],@"hkd",
                [NSNumber numberWithFloat:1],@"btc",
                nil],
               
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2962.11],@"hkd",
                [NSNumber numberWithFloat:5],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:2961.11],@"hkd",
                [NSNumber numberWithFloat:5],@"btc",
                nil],

    
    nil];
    
    // BITCOINS
    
    askData = [NSMutableArray arrayWithObjects:
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3120.49080],@"hkd",
                [NSNumber numberWithFloat:14.5],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3151.69500],@"hkd",
                [NSNumber numberWithFloat:22.00],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3151.69571],@"hkd",
                [NSNumber numberWithFloat:3.130999],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3168.00027],@"hkd",
                [NSNumber numberWithFloat:4.1088],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3176.23500],@"hkd",
                [NSNumber numberWithFloat:8.69720],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3213.423],@"hkd",
                [NSNumber numberWithFloat:0.5],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3240.88300],@"hkd",
                [NSNumber numberWithFloat:6.0],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3241.0977],@"hkd",
                [NSNumber numberWithFloat:4.0],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3244.0977],@"hkd",
                [NSNumber numberWithFloat:2],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3245.112],@"hkd",
                [NSNumber numberWithFloat:4.5],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3246.11],@"hkd",
                [NSNumber numberWithFloat:4.7],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3248.138],@"hkd",
                [NSNumber numberWithFloat:4.9],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3254.567],@"hkd",
                [NSNumber numberWithFloat:5.3],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3256.237],@"hkd",
                [NSNumber numberWithFloat:2],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3260.167],@"hkd",
                [NSNumber numberWithFloat:2],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3467],@"hkd",
                [NSNumber numberWithFloat:1],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3590],@"hkd",
                [NSNumber numberWithFloat:4],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3592.54],@"hkd",
                [NSNumber numberWithFloat:0.5],@"btc",
                nil],

               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3598.23],@"hkd",
                [NSNumber numberWithFloat:1.4],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3602.23],@"hkd",
                [NSNumber numberWithFloat:1.5],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3608],@"hkd",
                [NSNumber numberWithFloat:0.3],@"btc",
                nil],
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3613],@"hkd",
                [NSNumber numberWithFloat:1.5],@"btc",
                nil],
               
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3623],@"hkd",
                [NSNumber numberWithFloat:0.3],@"btc",
                nil],

               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3631],@"hkd",
                [NSNumber numberWithFloat:1.5],@"btc",
                nil],
               
               
               [NSDictionary dictionaryWithObjectsAndKeys:
                @"bid",@"type",
                [NSNumber numberWithFloat:3633],@"hkd",
                [NSNumber numberWithFloat:0.3],@"btc",
                nil],

               
   nil];
    
    
    bid_extreme_values = [self setupExtremeValues:bidData];
    ask_extreme_values = [self setupExtremeValues:askData];


    //convert object to data
    jsonAskData = [NSJSONSerialization dataWithJSONObject:askData options:NSJSONWritingPrettyPrinted error:&error];
    
    //convert object to data
    jsonBidData = [NSJSONSerialization dataWithJSONObject:bidData options:NSJSONWritingPrettyPrinted error:&error];
    

    jsonBidEncodedData = [[NSString alloc] initWithData:jsonBidData
                                             encoding:NSUTF8StringEncoding];
    

    jsonAskEncodedData = [[NSString alloc] initWithData:jsonAskData
                                                       encoding:NSUTF8StringEncoding];
    
 
}


/**
 *  @setupExtremeValues
 */
- (NSMutableDictionary *) setupExtremeValues : (NSMutableArray *) tradeData_p {
    
    int count = (int)[tradeData_p count];
    int i;
    
    float low_value = 0;
    float high_value = 0;
    
    for(i=0; i< count; i++) {
        NSDictionary * temp = [tradeData_p objectAtIndex:i];
        float hkd_value = [[temp objectForKey:@"hkd"] floatValue];
        //float btc_value = [[temp objectForKey:@"btc"] floatValue];
        
        float compare_value = hkd_value;//* btc_value;
        
        if (compare_value > high_value) {
            high_value = compare_value;
        }
        
        if(i==0) {
            low_value = compare_value;
        }
        
        if(compare_value < low_value) {
            low_value = compare_value;
        }
    }
    
    NSMutableDictionary * extremeValues = [[NSMutableDictionary alloc]initWithCapacity:2];
    
    [extremeValues setObject:[NSNumber numberWithFloat:high_value] forKey:@"high"];
    [extremeValues setObject:[NSNumber numberWithFloat:low_value] forKey:@"low"];

    return extremeValues;
}

/**
 *  @Loads the trading data
 */
/*
- (BOOL) loadTradeData {
    
    tradeDataFile = TRADE_DATA_FILE_NAME;
    
    tradeDataPath = [[NSBundle mainBundle] pathForResource:tradeDataFile ofType:@"plist"];

    if ([[NSFileManager defaultManager] fileExistsAtPath:tradeDataPath]) {
        tradeData = [[NSMutableDictionary alloc] initWithContentsOfFile:tradeDataPath];
        
        
        bid_extreme_values = [[NSMutableDictionary alloc]initWithCapacity:2];
        ask_extreme_values = [[NSMutableDictionary alloc]initWithCapacity:2];

        [self getTradeHigherValue:BID_KEY];
        [self getTradeHigherValue:ASK_KEY];
        
    }
    
    else {
    
    }
    
    return TRUE;
}
 */



/**
 *  @getExtremeValues
 */
-(float) getExtremeValues : (NSString *) tradeKey : (NSString *) option {
    

    if ([tradeKey isEqualToString:BID_KEY]) {
        return [[bid_extreme_values objectForKey:option] floatValue];
    }
    
    else if ([tradeKey isEqualToString:ASK_KEY]) {
        return [[ask_extreme_values objectForKey:option] floatValue];
    }
    
    return 0;

}


/**
 *  @getTradeData:
 */
- (NSMutableDictionary *) getFullTradeData {
    
    if ([tradeData count] >0) {
        return tradeData;
    }
    
    return nil;
}


/**
 *  @getTradeData:
 */
/*
- (NSMutableDictionary *) getBidTradeData {
    
    if ([tradeData count] >0) {
       return [tradeData objectForKey:BID_KEY];
    }
 
    return nil;
}
 */

/**
 *  @returns getasktrade data
 */
/*
- (NSMutableDictionary *) getAskTradeData {
    
     if ([tradeData count] >0) {
          return [tradeData objectForKey:ASK_KEY];
     }
    

    return nil;

}
*/

/**
 *
 */
/*
- (NSMutableDictionary *) sortTradeData {
    
    NSArray *myArray;
    
    myArray = [tradeData keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        
        if ([obj1 integerValue] > [obj2 integerValue]) {
            
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] < [obj2 integerValue]) {
            
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    return tradeData;
}
 */


/**
 *
 */
-(int) getAskEventsQuantity {
    
    return (int)[askData count];
}


/**
 *
 */
-(int) getBidEventsQuantity {
    
    return (int)[bidData count];
}

/**
 *
 */
- (NSMutableArray *) getTradeDataWithInterval: (NSString *) tradeKey : (int) start : (int) limit {
    
   // NSUInteger size = limit - start;
    
    NSMutableArray * outputTrade = [[NSMutableArray alloc]init];
    NSMutableArray * tempTrade = [[NSMutableArray alloc]init];
    
    if ([tradeKey isEqualToString:BID_KEY]) {
        tempTrade = bidData;
    }
    else if ([tradeKey isEqualToString:ASK_KEY]) {
        tempTrade = askData;
    }
    
    if ([tempTrade count] > 0) {
        
        int i=0;
        int index;
        
        for(index=start; index < limit; index++) {
            if(tempTrade[index]!=nil) {
                outputTrade[i] = tempTrade[index];
                i++;
            }
            else {

            }
            
        }
   
    }

    
    
    if ([outputTrade count] > 0) {
        return outputTrade;
    }
    
    return nil;
    
}

/**
 *
 */

/** NSDICTIONARY VERSION
- (NSDictionary *) getTradeDataWithInterval: (NSString *) tradeKey : (int) start : (int) limit {
    
    NSUInteger size = limit - start;
    
    NSMutableDictionary * outputTrade = [[NSMutableDictionary alloc]initWithCapacity:size];
    
    if ([tradeKey isEqualToString:BID_KEY] || [tradeKey isEqualToString:ASK_KEY]) {
        
        NSMutableDictionary * tempTrade = [tradeData objectForKey:tradeKey];

        if ([tempTrade count] >0) {
            
            int index = 0;
            
            for(id key in tempTrade) {
                if(index > start && index <= limit) {
                    id value = [tempTrade objectForKey:key];
                    [outputTrade setObject:value forKey:key];
                }
                index++;
               
            }
            
        }
    
    }


    if ([outputTrade count] > 0) {
      return outputTrade;
    }
    
    return nil;
    
}
 
 */


/**
 *  @Gets a specific trading value data block.
 */
- (NSDictionary *) getTradeValue: (NSString *) tradeKey : (NSString *) tradeType  {
    
    if ([tradeType isEqualToString:BID_KEY] || [tradeType isEqualToString:ASK_KEY]) {
        
        NSMutableDictionary * tempTrade = [tradeData objectForKey:tradeType];
        NSDictionary * tempTradeBlock = [tempTrade objectForKey:tradeKey];
        
        return tempTradeBlock;
        
    }
    
    return NULL;
}



/**
 *  @ Used to set a value of reference  for
 */
-(void) setFirstBlocksReferenceValue {
    
    //money_reference_highest_value = [[bid_extreme_values valueForKey:@"high"] floatValue];
    
    // Actually this should come after in some kind of interface setup
    money_reference_highest_value = BLOCK_BUCKET_TRADE_UNIQUE_VALUE;
    
    bitcoin_reference_lowest_value = [[ask_extreme_values valueForKey:@"low"] floatValue];
    
}


/**
 *
 */
-(void) getTradeHigherValue : (NSString *) tradeKey  {
    
    float higherValue = 0;
    float compare_value = 0;
    
    float lowestValue = 100000;
    BOOL firstCycle = TRUE;
    
    if ([tradeKey isEqualToString:BID_KEY] || [tradeKey isEqualToString:ASK_KEY]) {
        
        NSMutableDictionary * tempTrade = [tradeData objectForKey:tradeKey];
        
        if ([tempTrade count] >0) {
            
            
            for(id key in tempTrade) {
                id value = [tempTrade objectForKey:key];
                compare_value = [[value objectForKey:@"hkd"] floatValue] * [[value objectForKey:@"btc"] floatValue];
                if (firstCycle) {
                    lowestValue =compare_value;
                    firstCycle = FALSE;
                }
                
                if (compare_value > higherValue) {
                    higherValue = compare_value;
                }
                
                if (compare_value <= lowestValue) {
                    lowestValue = compare_value;
                }
                
                
            }
        }
        
    }
    
    if ([tradeKey isEqualToString:BID_KEY]) {
        [bid_extreme_values setObject:[NSNumber numberWithFloat:higherValue] forKey:@"high"];
        [bid_extreme_values setObject:[NSNumber numberWithFloat:lowestValue] forKey:@"low"];
    }
    
    else if ([tradeKey isEqualToString:ASK_KEY]) {
        [ask_extreme_values setObject:[NSNumber numberWithFloat:higherValue] forKey:@"high"];
        [ask_extreme_values setObject:[NSNumber numberWithFloat:lowestValue] forKey:@"low"];
    }
   

}







@end
