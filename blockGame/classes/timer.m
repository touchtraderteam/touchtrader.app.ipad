//
//  block.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "timer.h"
#import <SpriteKit/SpriteKit.h>

@implementation timer {
    
    
    
}


static timer *singleton = nil;

+ (timer *)singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
    }
    return singleton;
}


/**
 *  @SKSpriteNode
 */
- (timer *) getTimer : (NSString *) timerType : (float) seconds_p : (CGPoint) position_p {
	
	if (timerBar == nil) {
		
		seconds = seconds_p;
		
		defaultSize = CGSizeMake(200,20);
		
		timerBar = [[SKSpriteNode alloc]init];
		timerBarInside = [[SKSpriteNode alloc]init];
		
		[timerBar setName:@"timerBar"];
		[timerBar setPosition:CGPointMake(0,0)];
		[timerBar setSize:defaultSize];
		[timerBar setAnchorPoint:CGPointZero];
		timerBar.color = [UIColor blackColor];
		
		[timerBarInside setName:@"timerBarInside"];
		
		[timerBarInside setSize:defaultSize];
		[timerBarInside setAnchorPoint:CGPointZero];
		[timerBarInside setColor:[UIColor whiteColor]];
		[timerBarInside setPosition:CGPointMake(0,0)];
		
		[timerBar addChild:timerBarInside];
		
		[timerBar setHidden:TRUE];
		[timerBar setAlpha:0];
		[self addChild:timerBar];
		[self setPosition:position_p];
		
	}
	
	return self;

}


/**
 *	@startTimer
 */
- (void) startTimer : (float) timer_duration {
	
	//float scaleX = timerBarInside.frame.size.width
	[timerBar setHidden:FALSE];
	[timerBar runAction:[SKAction fadeAlphaBy:0.8 duration:0.2]];
	
	[timerBarInside runAction:[SKAction resizeToWidth:0 duration:timer_duration]  completion:^ {
		
		[timerBar runAction:[SKAction fadeOutWithDuration:0.2] completion:^ {
			[timerBar setHidden:TRUE];
			[timerBarInside setSize:defaultSize];
		}];
	
	}];
	
}

/**
 *	@stopTimer
 */
- (void) stopTimer {
	
	
	[timerBarInside removeAllActions];

	[timerBar runAction:[SKAction fadeOutWithDuration:0.2] completion:^ {
		[timerBarInside setSize:defaultSize];
	}];

	
}

/**
 *	@pauseTimer
 */
- (void) pauseTimer {
	
}


@end
