//
//  blockCoin
//  TouchTrader
//
//  Created by Mac on 21/12/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "blockCoin.h"

@implementation blockCoin {
    
}



-(id) init {
    self = [super init];
    if(self)
    {
      
        
    }
    return self;
}


/**
 * option: @money or @bitcoin
 */
-(blockCoin *) setMode : (NSString *) option : (int) index {

    if ([option isEqualToString:MONEY_BUCKET_ID] || [option isEqualToString:MONEY_WALLET_NAME]) {
        textureImage = @"dollar_texture.png";
    }
    
    else if ([option isEqualToString:BITCOIN_BUCKET_ID] || [option isEqualToString:BITCOIN_WALLET_NAME]) {
        textureImage = @"bitcoin_texture.png";
    }

    isBeingTraded = false;
    blockCoin * tempCoin = [blockCoin spriteNodeWithImageNamed:textureImage];
    [tempCoin setName:[NSString stringWithFormat:@"%@_%d", BLOCK_BLOCKCOIN_PREFIX, index]];
    tempCoin.userInteractionEnabled = FALSE;
  
    return tempCoin;
}


/**
 *  Returns the blockcoin trade value
 */
- (float) getBlockCoinTradeValue {
    
    return blockCoinTradeValue;
    
}



/**
 *  @getBlinkAction
 */
-(SKAction *) getBlinkAction {
    
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.4 duration:0.5];
    SKAction *fadeIn = [SKAction fadeInWithDuration: 0.5];
    SKAction *pulse = [SKAction sequence:@[fadeOut,fadeIn]];
    
    //SKAction *pulseThreeTimes = [SKAction repeatAction:pulse count:3];
    SKAction * blinkAction = [SKAction repeatActionForever:pulse];
    
    return blinkAction;
    
}


/**
 *  @returns if the block coin is on a trading live status
 */
- (BOOL) getIsBeingTraded {
    return isBeingTraded;
}


/**
 *
 */
- (void) removeIsBeingTraded {
    
    if ([self hasActions]) {
        isBeingTraded = false;
        [self removeAllActions];
        [self runAction:[SKAction fadeInWithDuration:0.3]];
        
    }
    
}

/**
 *
 */
- (void) setShadow : (BOOL) hasShadow {
    
    if (hasShadow) {
        if (shadow !=nil) {
            [shadow runAction:[SKAction fadeInWithDuration:0.2]];
        }
        else {
            
            float x_position = 0 - self.frame.size.width/4;
            float y_position = 0 -  self.frame.size.height/4;
            
            CGRect circle = CGRectMake(x_position, y_position, self.frame.size.width/2 ,self.frame.size.height/2);
            
            //circle.origin = CGPointZero;
            shadow = [[SKShapeNode alloc] init];
            shadow.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
            //shadow.fillColor = [UIColor colorWithRed:1.00 green:0.76 blue:0.53 alpha:1.0];
            shadow.fillColor = [UIColor blackColor];
            shadow.lineWidth = 0;
            shadow.position = CGPointMake(0.5,0.5);
            
            // shadow.position = self.position;
            shadow.zPosition = -1;
            shadow.alpha = 0.7;
            shadow.userInteractionEnabled = false;
            shadow.antialiased = TRUE;
            shadow.lineWidth = 3;
            [self addChild:shadow];
            
        }
    }
    
    else {
        if (shadow!=nil) {
            [shadow runAction:[SKAction fadeOutWithDuration:0.2]];
        }
    }
}


/**
 *
 */
-(void) setIsBeingTraded {
    
    isBeingTraded = TRUE;
    [self runAction:[self getBlinkAction]];

}

/**
 *  Sets the blockcoin trade value
 */
- (void) setBlockCoinTradeValue : (float) newBlockCoinTradeValue {
    
    blockCoinTradeValue = newBlockCoinTradeValue;
    
}

@end
