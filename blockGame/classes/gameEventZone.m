//
//  gameEventZone.m
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "gameEventZone.h"

@implementation gameEventZone {
    
    
}

static gameEventZone *singleton = nil;

+ (gameEventZone *)singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
        
        
        
    }
    return singleton;
}


/**
 *  @setupGameEvent
 */
- (void) setupGameEvent {
 
    int width = [self parent].frame.size.width/2;
    int height = [self parent].frame.size.height/3;
    
    [self setSize:CGSizeMake(width, height)];
    [self setColor:[UIColor clearColor]];
    
    float position_x = [self parent].frame.origin.x + [self parent].frame.size.width/2;
    float position_y = [self parent].frame.origin.y + [self parent].frame.size.height/2 + self.frame.size.height/2;
    
    [self setAnchorPoint:CGPointMake(0.5, 0.5)];
    [self setName:@"gameEventZone"];
    
    [self setPosition:CGPointMake(position_x, position_y)];
    
    fadeIn = [SKAction fadeInWithDuration:0.2];
    fadeOut = [SKAction fadeOutWithDuration:0.4];
    [self setupInfoLabel];
    
}


/**
 *
 */
-(void) setupInfoLabel {
    
    
    gameEventInfo = [SKLabelNode labelNodeWithFontNamed:SLICK_OPTIMA_REGULAR];
    [gameEventInfo setName:@"gameEventZoneLabel"];
    gameEventInfo.text = @"";
    
    gameEventInfo.alpha = 0;
    
    gameEventInfo.zPosition = 200;
    gameEventInfo.fontColor = GAME_INFO_LABEL_COLOR;
    [gameEventInfo setPosition:CGPointMake(0 ,0)];
    gameEventInfo.fontSize = 30;
    [self addChild:gameEventInfo];

}


/**
 *  @removeInfo
 */
-(void) removeInfo {
    
    [gameEventInfo runAction:fadeOut];
    
}

/**
 *  @displayInfo
 */
-(void) displayInfo : (NSString *) info {
    
    gameEventInfo.text = info;
    [gameEventInfo runAction:fadeIn];
    
}

/**
 *  @displayInfoTime
 */
- (void) displayInfoTime : (NSString *) info : (int) displayTime {
    
    gameEventInfo.text = info;
    SKAction * hold = [SKAction waitForDuration:displayTime];
    SKAction * sequence = [SKAction sequence:@[fadeIn,hold,fadeOut]];
    [gameEventInfo runAction:sequence];
}



@end
