//
//  menuContainer.m
//  blockGame
//
//  Created by daniel.oliveira on 29/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "menuContainer.h"

@implementation menuContainer {
    
    
    
}

static menuContainer *singleton = nil;

+ (menuContainer *) singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
        
    }
    return singleton;
}

- (id)init {
    if ( (self = [super init]) ) {
        // your custom initialization
        
    }
    return self;
}


/**
 *  @originCoordinates
 */


/*
- (CGRect) originCoordinates {
    
    // Assuming CGAnchor as 0,5: 0,5
    // Note: future anchor point should be calculated automatically
    
    float origin_width = self.frame.size.width;
    float origin_height = self.frame.size.height;
    float origin_x = self.frame.origin.x - (origin_width/2);
    float origin_y = self.frame.origin.y - (origin_height/2);
    
    return CGRectMake(origin_x, origin_y, origin_width, origin_height);
}
 */

/**
 *  @setBlockInfo
 */
-(void) setBlockInfo : (block *) selectedBlock {
    
    NSString * myString = [NSString stringWithFormat:@"%.2f",[selectedBlock getTradeValue]];
    block_info.text = [@"Trade value:" stringByAppendingString:myString];
    
}

/**
 *  @initialSetup
 */
-(menuContainer *) initialSetup: (SKScene *) parentFrame {
    
    
    int background_width = parentFrame.size.width;
    //int background_height = parentFrame.size.height/6-10;
    
    uniqueName = @"menucontainer";
    defaultColor = SLICK_BLACK;
    
    defaultMenuInfoSize = CGSizeMake(self.frame.size.width - self.frame.size.width/6, MAIN_MENU_HEIGHT + 20);
    defaultSize = CGSizeMake(background_width,MAIN_MENU_HEIGHT);
    
    //setPosition is according to the parent frame
    [self setName:uniqueName];
    [self setSize:defaultSize];
    self.anchorPoint =  CGPointMake(0.5,0.5);
    [self setName:uniqueName];
    [self setColor:SLICK_BLACK];
    self.zPosition = 100;
    
    int start_x = parentFrame.frame.origin.x + self.frame.size.width/2;
    int start_y = parentFrame.frame.origin.y + parentFrame.frame.size.height - MAIN_MENU_HEIGHT/2 - 20;  //parentFrame.frame.origin.y + self.frame.size.height/2;
    
    
    /*
    topBar = [[SKSpriteNode alloc]init];
    [topBar setPosition:CGPointMake(-10, 700)];
    [topBar setColor:[UIColor whiteColor]];
    [topBar setSize:CGSizeMake(1100, 30)];
    [topBar setColor:[UIColor grayColor]];
    [self addChild:topBar];
     */
    
    [self setPosition:CGPointMake(start_x ,start_y)];
    [self addMenuButtons];
    //[self setupPerformanceBar];
    return self;
    
}

/**
 *  @displayUserInfo
 */
-(void) displayUserInfo {
    
    if (userTable !=nil) {
        userTable = [[SKSpriteNode alloc]init];
        [userTable setHidden:TRUE];
    }
    
    else {
        
        
        
    }
}


/**
 *
 */
-(void) setupPerformanceInfo {
    
    if (performanceTable == nil) {
        
        performanceTable = [[SKSpriteNode alloc]init];
        [performanceTable setSize:defaultMenuInfoSize];
   
        
        // performanceBar
        performanceBar = [[percentageBar alloc]init];
        [performanceTable addChild:performanceBar];
        
        [performanceBar setAnchorPoint:CGPointZero];
        [performanceBar setupPercentageBarForMenu:@"menu_performance_bar"];
        
        [performanceTable setAlpha:0];
        [performanceTable setPosition:CGPointMake(0, 0)];
        [performanceTable setHidden:TRUE];
        [self addChild:performanceTable];
    }

    
}
/**
 *  @displayPerformanceInfo
 */
-(void) displayPerformanceInfo {
    
    
    if (performanceTable.isHidden) {
        [performanceTable setAlpha:0];
        [performanceTable setHidden:FALSE];
        [performanceTable runAction:[SKAction fadeInWithDuration:1]];

    }
    
    else {
        [performanceTable runAction:[SKAction fadeOutWithDuration:1] completion:^ {
            [performanceTable setHidden:TRUE];
        }];
        
    }
    

}


/**
 *  @displayMenuSection
 */
-(void) displayMenuSection : (NSString *) option {
    
    if ([option isEqualToString:@"menuButton_bidask"]) {
        mainScene * father =  (mainScene *) [self parent];
        [[father gameContainer] displayBidAskGraphic];
        [bidAskButton tapButton];
       
    }
    else if ([option isEqualToString:@"menuButton_performance"]) {
        mainScene * father =  (mainScene *) [self parent];
        [[father gameContainer] displayBitcoinTrend];
        [bitcoinTrendButton tapButton];
    }
    
    else if ([option isEqualToString:@"menuButton_heatmap"]) {
        mainScene * father =  (mainScene *) [self parent];
        [[father gameContainer] displayHeatmap];
        [heatmapButton tapButton];

    }
    else if ([option isEqualToString:@"menuButton_other"]) {
        [otherButton tapButton];
    }
    
    else if ([option isEqualToString:@"menuButton_help"]) {
        [helpButton tapButton];
    }
    

  
}

/**
 *  @addMenuButtons
 */
-(void) addMenuButtons {
   
    bidAskButton = [[menuButton alloc]init];
    bitcoinTrendButton = [[menuButton alloc]init];
    heatmapButton = [[menuButton alloc]init];
    otherButton = [[menuButton alloc]init];
    helpButton = [[menuButton alloc]init];
    
  
    splitBar = [[SKSpriteNode alloc]init];
    [splitBar setSize:CGSizeMake(1,self.frame.size.height-20)];
    [splitBar setPosition:CGPointMake(0- self.frame.size.width/2 + self.frame.size.width/6, 0)];
    [splitBar setColor:[UIColor whiteColor]];

    
   /* [bidAskButton startButton:@"menuButton_bidask" :@"bidaskButton.png":CGPointMake(1, 1)];
    [bitcoinTrendButton startButton:@"menuButton_performance" :@"arrowButton.png":CGPointMake(2, 1)];
    [heatmapButton startButton:@"menuButton_heatmap" :@"heatmapButton.png":CGPointMake(1, 2)];
    [otherButton startButton:@"menuButton_other" :@"userButton.png":CGPointMake(2, 2)];
    */
    float padding = 60;
    float start_x = 0 - self.frame.size.width/2 + 40;
    
    [bidAskButton startButton:@"menuButton_bidask" :@"bidaskButton.png":start_x];
    [bitcoinTrendButton startButton:@"menuButton_performance" :@"arrowButton.png":start_x+padding];
    [heatmapButton startButton:@"menuButton_heatmap" :@"heatmapButton.png":start_x+(padding*2)];
    [otherButton startButton:@"menuButton_other" :@"userButton.png":start_x+(padding*3)];
    [helpButton startButton:@"menuButton_help" :@"helpButton.png" :start_x+(padding * 4)];
    
    [self addChild:bitcoinTrendButton];
    [self addChild:bidAskButton];
    [self addChild:heatmapButton];
    [self addChild:otherButton];
    [self addChild:helpButton];
    [self addChild:splitBar];

    
    [self setupPerformanceInfo];
}


/**
 *  @setupPerformanceBar
 */
-(void)setupPerformanceBar {
    
    performanceBar = [[percentageBar alloc]init];
    [self addChild:performanceBar];
    [performanceBar setupPercentageBarForMenu:@"menu_performance_bar"];

}


/**
 *
 */
-(void) setPerfomanceBar {
    [performanceBar setMenuBar];
}


/**
 *
 */
-(void) refreshPerfomanceBar {
    [performanceBar refreshMenuBar];
}

/**
 *
 */
-(percentageBar *) getPortfolioBar {
    return performanceBar;
}




/**
 *  @addInfo
 */
/*
-(void) addInfo {
    
    int position_x = 0 + self.frame.size.width/5;
    UIColor * fontColor = [UIColor whiteColor];
    
    block_info = [[factory singleton]buildBlockLabel:@"menu" :CGPointMake(position_x, self.frame.origin.y):[UIColor blackColor]];
    [block_info setName:@"menu_label"];
    [block_info setFontColor:fontColor];
    block_info.fontSize = 30;
    
    [self addChild:block_info];

    
}
 */



@end
