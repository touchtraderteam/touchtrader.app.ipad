//
//  blockBucket.h
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "block.h"
#import "Constant.h"
#import "percentageBar.h"
#import "wallet.h"

@class wallet;
@class block;


@interface blockBucket : SKSpriteNode {
    
    NSMutableDictionary * blockCollection;
    
    percentageBar * blockCounter;
    wallet * valueWallet;
    
    
    
    // Used to reorder the blocks back.
    NSMutableArray * initial_vertical_positions;
    
    int bucketLimit;
    int bucket_height;
    CGRect parentFrame;
    SKSpriteNode * parentScene;
    
    //BOOL whiteMode;
    BOOL bitcoin;
    NSString * bucketType;
    BOOL isBitCoinBucket;
    
    // Keeps track of blocks added but not visible.
    int addedBlocksCount;
    
    
    // For better trading.
    float blockCount;
    float fullBlockCount;
    
    float currentLiquidValue;
    //float currentBucketNetValue;
    
    
    BOOL bucketBlockHasIncompleteBlocks;
    BOOL tradeToBlockHasIncompleteBlocks;
    
}

// Initial setup.
- (void) setup : (NSString *) uniqueName : (SKSpriteNode *) parentFrame;
- (float) blockBucketFinalTradeOperations;


- (block *) getLowestBitcoin;
- (void) playWalletAnimation;
- (wallet *) getBucketWallet;


// This function should take care of the whole trade.
- (void) addTradeCoins : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue;

-(void) removeCoinFromBucket : (block *) blockObject_p;
-(void) refreshTradeStatusRemove : (block *) blockObject_p;
//-(void) refreshTradeStatusAdd : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue;




- (void) runBlockBucketHeatMap : (block *) betterBidCoin;
-(void) closeBlockBucketHeatMap;

// Bitcoin or Money mode.
-(BOOL) isBitCoinBucket;
-(BOOL) isMoneyBucket;

-(void) setMode : (NSString *) bucketType_p;
-(NSString *) getMode;

// Split up coins
- (void) splitUpCoin:(block *) _selectedNode;
// Bundle up coins
-(void) bundleUpCoins:(block *)_selectedNode;

// Trade
-(BOOL) bucketIsFull;
-(int) blocksInsideBucket;

// arrangeblocks when a block is dropped or removed.
-(BOOL) arrangeBlocks;
-(NSMutableArray *) getChildBlocks;

-(void) addWalletCoin: (block *) walletBlockCoin;
-(block *) getIncompleteBlockOfBucket;


@end
