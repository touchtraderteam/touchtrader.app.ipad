//
//  walletInterface.h
//  touchTrader
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>


#import "Constant.h"


@interface menuButton : SKSpriteNode {
  
    SKShapeNode * tile;
    SKSpriteNode * menuButtonImage;
    BOOL isSelected;
    int button_width;
    int button_height;
    BOOL tapped;
    
    
    float start_x;
}

- (void) startButton : (NSString *) uniqueName : (NSString *) iconPath : (float) position_x;
-(void) tapButton;
- (BOOL) isTapped;

-(void) setButtonClicked : (BOOL) value;
- (BOOL) buttonIsClicked;

@end
