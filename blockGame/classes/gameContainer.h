//
//  block.h
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constant.h"
#import "gameEventZone.h"

#import "blockBucket.h"
#import "block.h"

@class block;


@interface gameContainer : SKSpriteNode {
    
    
    UIColor * defaultColor;
    CGSize defaultSize;
    
    
    NSString * uniqueName;

  
}



+ (gameContainer *)singleton;

-(gameContainer *) initialSetup: (SKScene *) parentFrame_p;


-(void) displayInstanceHeatMap : (block *) coin;
- (void) loadIndicators;

- (void) displayHeatmap;
-(void) displayBidAskGraphic;
-(void) displayBitcoinTrend;


@end
