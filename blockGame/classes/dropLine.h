//
//  dropLine.h
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "dataManager.h"
#import "Constant.h"

#import "factory.h"
#import "dropLineBucket.h"

#import "blockBucket.h"
#import "menuContainer.h"

#import "soundPlayer.h"


@class block;
@class blockBucket;


@interface dropLine : SKSpriteNode {
    
    
    float appendPreviousFullTradeBlockCount;
    float appendPreviousFullTradeBlockValue;
    
    // DropLine trade objects
    blockBucket * sourceBlockBucket_trade;
    blockBucket * targetBlockBucket_trade;
    block * blockBucketObject_trade;
    
    // General settings
    NSString * uniqueName;
    UIColor * defaultColor;
    CGSize defaultSize;


    NSMutableArray * initial_horizontal_positions;
    NSMutableDictionary * blockCollection;
    
    int dropLine_block_start_x;
    int bitcoin_start_x;
    int money_start_x;
    
    BOOL isMoneyDropLine;
    BOOL isDropLineDoingTrade;
    NSString * blockType;
    
    // droplines quantity intervals
    float tradeValueLimit_min;
    float tradeValueLimit_max;
    
    float tradeLabelValue_min;
    float tradeLabelValue_max;
    
    
    float lastBlockTradeDifference;
    
    float dropLineRotateValue;
    float dropLineRotationTime;
    
    
    float usableDropLineWidth;
    


}


- (void) dropLineAppendTrade_callback : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue;
- (void) dropLineFinishTrade_callback : (float) numberOfBlocksTraded : (float) numberOfBlocksTradedValue;
//-(float) getBlockTradeDifference;

-(float) lowerTradeValue;
-(float) higherTradeValue;

- (int) startPointX;

- (BOOL) hasBlocksToTrade;

// Trade related.
-(void) resetBlockTrade;
-(float) checkBlockTrade : (float) blockTradeValue;


- (block *) getFirstTradeBlock;

-(BOOL) isDropLineDoingTrade;
-(void) setDropLineDoingTrade : (BOOL) isDropLineDoingTrade_p;

- (void) doDropLineTrade: (blockBucket *) sourceBlockBucket : (block *) blockBucketObject  : (blockBucket *) targetBlockBucket;

-(void) initialSetup : (SKSpriteNode *) parentFrame_p : (int) index;




//-(void) buildBlocksFromTradeData : (NSString *) tradeTypeKey : (int) start : (int) end;

-(int) buildBlocksFromTradeData : (NSString *) tradeTypeKey : (int) start;

-(BOOL) isMoneyDropLine;
-(BOOL) isBitcoinDropLine;

-(void) runDropLineHeatMap: (block *) targetBlockCoin;
-(void) hideDropLineHeatMap;



@end
