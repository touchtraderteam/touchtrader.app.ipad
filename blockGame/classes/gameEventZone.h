//
//  gameEventZone.h
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

#import "gameContainer.h"
#import "Constant.h"

@interface gameEventZone : SKSpriteNode {
    
    SKLabelNode *gameEventInfo;
    SKAction * fadeIn;
    SKAction * fadeOut;
}


// Unique singleton
+ (gameEventZone *)singleton;



- (void) setupGameEvent;
- (void) removeInfo;
- (void) displayInfo : (NSString *) info;
- (void) displayInfoTime : (NSString *) info : (int) displayTime;
    

@end
