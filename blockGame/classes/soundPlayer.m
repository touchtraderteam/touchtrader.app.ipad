//
//  soundPlayer.m
//  TouchTrader
//
//  Created by Mac on 21/4/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//


#import "soundPlayer.h"


@implementation soundPlayer {
    
    

}

static soundPlayer *singleton = nil;

+ (soundPlayer *)singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
        [[soundPlayer singleton]initVariables];
        [[soundPlayer singleton] loadSounds];
    }
    return singleton;
}



/**
 *
 */
-(void) loadBackgroundPlayer {
    
    NSError *error;
    NSURL * backgroundMusicURL = [[NSBundle mainBundle] URLForResource:backgroundSoundURL withExtension:@"mp3"];
    self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    self.backgroundMusicPlayer.numberOfLoops = -1;

    
}


/**
 *
 */
-(void) initVariables {
    soundsLoaded = TRUE;
}


/**
 *
 */
-(void) playBackgroundMusic {
    
    [self.backgroundMusicPlayer prepareToPlay];
    [self.backgroundMusicPlayer play];

}

/**
 *  @loadSounds:
 */
-(void) loadSounds {
    
    if (soundsLoaded) {
        
        soundsLoaded = FALSE;
        /*
        cancelSound = @"cancelSound";
        positiveSound = @"positiveSound";
        negativeSound = @"negativeSound";
        */
        backgroundSoundURL = @"raindrops_env";
        
        //tradeSoundAction = [SKAction playSoundFileNamed:tradeSound waitForCompletion:NO];
        //cancelSoundAction = [SKAction playSoundFileNamed:cancelSound waitForCompletion:NO];
        //positiveSoundAction = [SKAction playSoundFileNamed:positiveSound waitForCompletion:NO];
        //negativeSoundAction = [SKAction playSoundFileNamed:negativeSound waitForCompletion:NO];
    
    }
    
}


/**
 *
 */
-(void) playTradeSuccess {
  
    NSError * error;
    tradeSound = [[NSBundle mainBundle] URLForResource:@"tradeSuccess" withExtension:SOUND_EXTENSION];
    
    self.actionPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:tradeSound error:&error];
    self.actionPlayer.numberOfLoops = 0;
    [self.actionPlayer prepareToPlay];
    //[self.actionPlayer play];
    
    //tradeSoundAction = [SKAction playSoundFileNamed:tradeSound waitForCompletion:NO];
    //[self runAction:tradeSoundAction];
   
}


/**
 *
 */
- (BOOL) playTradeSound {

   
    return TRUE;
}

@end
