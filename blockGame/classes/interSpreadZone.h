//
//  gameEventZone.h
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

#import "gameContainer.h"
#import "Constant.h"
#import "block.h"
#import "blockBucket.h"
#import "timer.h"

@class block;
@class timer;

@interface interSpreadZone : SKSpriteNode {
    
    SKShapeNode * bidLine;
    SKLabelNode * bidLineLabel;
    
    SKShapeNode * bidMirrorLine;
    
    float spreadBlockStartPrice;
    CGPoint spreadBlockPosition;
    
    // collection
    NSMutableArray * spreadBlocks;
    block * spreadBlock;
    blockBucket * spreadBucket;
    
    // Money
    block * moneyBidBlock;
    blockBucket * moneyBidBucket;
    CGPoint moneyBlockPosition;
    SKShapeNode * moneyMirrorLine;
    
    
    float fadeDuration;
    
    float mainLineBegin_x;
    float mainLineBegin_y;
    
    float mainLineEnds_x;
    float mainLineEnds_y;
    
    
    SKSpriteNode * timeSwipeLine;
}


// Unique singleton
+ (interSpreadZone *)singleton;

- (SKShapeNode *) getBidLine;
- (void) setupInterSpread;

- (void) generateInterBid:(block *) blockCoin : (blockBucket *) blockBucket;
- (void) attachMoneyTrade:(block *) blockCoin : (blockBucket *) blockBucket;


- (void) stopBid;


-(void) switchInterSpreadDisplay : (BOOL) display;

-(void) finishInterSpreadSuccess;

-(BOOL) isDoingBid;

-(BOOL) canMakeBid : (block *) moneyCoin;


@end
