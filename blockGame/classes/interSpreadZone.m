//
//  gameEventZone.m
//  blockGame
//
//  Created by daniel.oliveira on 18/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "interSpreadZone.h"

@implementation interSpreadZone {
    
    
}

static interSpreadZone *singleton = nil;

+ (interSpreadZone *)singleton {
    
    if (singleton == nil) {
        singleton = [[super allocWithZone:NULL] init];
        
    }
    return singleton;
}


/**
 *
 */
-(void) drawQuadCurve {
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    CGPathAddQuadCurveToPoint(path, NULL, 50, 100, 100, 0);
    CGPathAddLineToPoint(path, NULL, 50, -100);
    CGPathCloseSubpath(path);
    
    SKShapeNode *shape = [[SKShapeNode alloc]init];
    shape.path = path;
    [shape setStrokeColor:[UIColor whiteColor]];
    
    
    [self addChild:shape];
}


/**
 *  @setupInterSpread
 */
- (void) setupInterSpread {
    
    int width = [self parent].frame.size.width/3;
    int height = [self parent].frame.size.height/4;
    fadeDuration = 0.2;
    [self setSize:CGSizeMake(width, height)];
    
    float position_x = [self parent].frame.origin.x + [self parent].frame.size.width/2 + 10;
    float position_y = [self parent].frame.origin.y + [self parent].frame.size.height/2 + self.frame.size.height/2+30;
    
    [self setAnchorPoint:CGPointMake(0.5, 0.5)];
    [self setName:@"interSpreadZone"];
    [self setPosition:CGPointMake(position_x, position_y)];
    //[self drawCurvedLine];
	
	[self drawStraightLine];
	
	//[self drawQuadCurve];
	[self addLabel];
    [self addChild:[[timer singleton] getTimer:@"bar":TRADING_BID_TIMER_VALUE:CGPointMake(0 - 180,0+ 45)]];
    
  
}

/**
 *  @addLabel
 */
-(void) addLabel {
    
 
    if (bidLineLabel == nil) {
 
        int start_x = 0-self.frame.size.width/2;
        int start_y = 0+self.frame.size.height/2 - 5;
        
        bidLineLabel = [SKLabelNode labelNodeWithFontNamed:SLICK_OPTIMA_REGULAR];
        [bidLineLabel setName:@"bidLineLabel"];
        bidLineLabel.fontColor = SLICK_BLACK;
        bidLineLabel.fontSize = 18;
        bidLineLabel.zPosition = 101;
        bidLineLabel.text = @"Start Interspread ?";
        [bidLineLabel setPosition:CGPointMake(start_x+70, start_y)];
        [bidLineLabel setAlpha:0];
        [self addChild:bidLineLabel];

    }
}


/*
 *	@displayLabel
 */
-(void) displayLabel : (BOOL) display {
	
	if (display) {
		[bidLineLabel runAction:[SKAction fadeInWithDuration:0.3]];
	}
	else {
		[bidLineLabel runAction:[SKAction fadeOutWithDuration:0.3]];
	}
    
}


/**
 *
 */
- (void) stopBid {

}

/**
 *
 */
-(BOOL) isDoingBid {
	
	if (spreadBlock == nil ) {
		return FALSE;
	}
	else {
		return TRUE;
	}
	
}

/**
 *  @generateInterBid
 */
-(void) generateInterBid:(block *) blockCoin : (blockBucket *) blockBucket {
    
	spreadBlock = blockCoin;

	[self displayLabel:FALSE];
	spreadBlockStartPrice = [spreadBlock getDynamicBidPrice];
	spreadBlockPosition = spreadBlock.position;
	[spreadBlock setFixedBidMode:spreadBlockStartPrice];

	
	
	[self drawPathFromPointX];

}



/**
 *	@attachMoneyTrade
 */
-(void) attachMoneyTrade:(block *) blockCoin : (blockBucket *) blockBucket {

	moneyBidBlock = blockCoin;
	moneyBidBucket = blockBucket;
	moneyBlockPosition = moneyBidBlock.position;
	[self drawMoneyPathFromPoint];
	
}


/**
 *
 */
-(BOOL) canMakeBid : (block *) moneyCoin {
	
	if (spreadBlock.position.y > moneyCoin.position.y) {
		return TRUE;
	}

	return FALSE;
}


/**
 *  @getBidLine
 */
- (SKShapeNode *) getBidLine {
    return bidLine;
}

/**
 *  @drawCurvedLine
 */
- (void) drawCurvedLine {
    
    bidLine = [SKShapeNode node];
    CGMutablePathRef pathToDraw = CGPathCreateMutable();
    
    mainLineBegin_x = 0 + self.frame.size.width/2;
    mainLineBegin_y = 0 + self.frame.size.height/2;

    CGPathMoveToPoint(pathToDraw, NULL, mainLineBegin_x, mainLineBegin_y);
    
    mainLineEnds_x = 0 - self.frame.size.width/2;
    mainLineEnds_y = 0 - self.frame.size.height/2;
    
    CGPathAddCurveToPoint(pathToDraw, NULL, mainLineBegin_x, mainLineBegin_y, 140, 0, mainLineEnds_x, mainLineEnds_y);
    bidLine.path = pathToDraw;
    [bidLine setStrokeColor:[UIColor whiteColor]];

    bidLine.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:bidLine.path];
    bidLine.physicsBody.dynamic = NO;
    bidLine.physicsBody.categoryBitMask = bidline_category;
    bidLine.physicsBody.contactTestBitMask = block_category;
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;
    [bidLine setStrokeColor:[UIColor whiteColor]];
    [self addChild:bidLine];
    
}


/**
 *	@drawStraightLine
 */
- (void) drawStraightLine {
	

	bidLine = [SKShapeNode node];
	CGMutablePathRef path = CGPathCreateMutable();
	
	mainLineBegin_x = 0 + self.frame.size.width/2;
	mainLineBegin_y = 0 + self.frame.size.height/2;
	mainLineEnds_x = 0 - self.frame.size.width/2;
	mainLineEnds_y = 0 - self.frame.size.height/2;
	
	CGPathMoveToPoint(path, NULL, mainLineBegin_x, mainLineBegin_y);
	CGPathAddLineToPoint(path, NULL, mainLineEnds_x, mainLineEnds_y);
	
	bidLine.path = path;
	[bidLine setStrokeColor:[UIColor whiteColor]];
	
	
	bidLine.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:bidLine.path];
	bidLine.physicsBody.dynamic = NO;
	bidLine.physicsBody.categoryBitMask = bidline_category;
	bidLine.physicsBody.contactTestBitMask = block_category;
	self.physicsBody.collisionBitMask = 0;
	
	[self addChild:bidLine];
	
}


/**
 *	@drawMoneyPathFromPoint
 */
- (void) drawMoneyPathFromPoint {
	
	moneyMirrorLine = [SKShapeNode node];
	
	CGMutablePathRef path = CGPathCreateMutable();
	
	mainLineBegin_x = 0 + self.frame.size.width/2;
	mainLineBegin_y = 0 + self.frame.size.height/2;
	
	mainLineEnds_x = 0 - self.frame.size.width/2;
	mainLineEnds_y = 0 - self.frame.size.height/2;
	
	CGPathMoveToPoint(path, NULL, moneyBlockPosition.x - self.frame.size.width, moneyBlockPosition.y);
	CGPathAddLineToPoint(path, NULL, mainLineEnds_x, mainLineEnds_y);
	
	moneyMirrorLine.path = path;
	[moneyMirrorLine setStrokeColor:[UIColor redColor]];
	
	[moneyBidBlock setPosition:CGPointMake(moneyBlockPosition.x - self.frame.size.width, moneyBlockPosition.y)];
	//moneyBidBlock.position.x = moneyBlockPosition.x - self.frame.size.width;
	
	moneyMirrorLine.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:moneyMirrorLine.path];
	moneyMirrorLine.physicsBody.dynamic = NO;
	moneyMirrorLine.physicsBody.categoryBitMask = bidline_category;
	moneyMirrorLine.physicsBody.contactTestBitMask = block_category;
	self.physicsBody.collisionBitMask = 0;
	
	[self addChild:moneyMirrorLine];
	[moneyMirrorLine addChild:moneyBidBlock];
	//[moneyBidBlock setBidLineBlock:TRUE:@"money"];
}


/**
 *	@drawPathFromPointX
 */
- (void) drawPathFromPointX {
	
	bidMirrorLine = [SKShapeNode node];
	
	CGMutablePathRef path = CGPathCreateMutable();
	
	mainLineBegin_x = 0 + self.frame.size.width/2;
	mainLineBegin_y = 0 + self.frame.size.height/2;
	
	mainLineEnds_x = 0 - self.frame.size.width/2;
	mainLineEnds_y = 0 - self.frame.size.height/2;
	
	CGPathMoveToPoint(path, NULL, spreadBlockPosition.x + self.frame.size.width, spreadBlockPosition.y);
	CGPathAddLineToPoint(path, NULL, mainLineEnds_x, mainLineEnds_y);
	
	bidMirrorLine.path = path;
	[bidMirrorLine setStrokeColor:[UIColor clearColor]];
	
	//bidLine.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(200, 200)];
	bidMirrorLine.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:bidLine.path];
	bidMirrorLine.physicsBody.dynamic = NO;
	bidMirrorLine.physicsBody.categoryBitMask = bidline_category;
	bidMirrorLine.physicsBody.contactTestBitMask = block_category;
	self.physicsBody.collisionBitMask = 0;
	
	[self addChild:bidMirrorLine];
	[bidMirrorLine addChild:spreadBlock];

	
	int proportional_time = [self getProportionalTime:mainLineBegin_y :mainLineEnds_y :spreadBlockPosition.y :TRADING_BID_TIMER_VALUE];
	SKAction *followTrack = [SKAction followPath:bidMirrorLine.path asOffset:NO orientToPath:NO duration:proportional_time];
	

	[[timer singleton] startTimer:proportional_time];

	[spreadBlock runAction:followTrack completion:^ {
		[self finishInterSpreadFail];
	}];
	
}


/**
 *	@getTimeLineSwipe
 */
-(SKSpriteNode *) getTimeLineSwipe {
	
	timeSwipeLine = [[SKSpriteNode alloc]init];
	[timeSwipeLine setSize:CGSizeMake(40, 2)];
	[timeSwipeLine setColor:[UIColor grayColor]];
	[timeSwipeLine setAlpha:0.5];
	[timeSwipeLine setPosition:CGPointMake(spreadBlock.position.x, spreadBlock.position.y)];
	return timeSwipeLine;
	
}

/**
 * abs(int), labs(long), llabs(long long), imaxabs(intmax_t), fabsf(float), fabs(double), or fabsl(long double).
 */
-(float) getProportionalTime : (float) begin : (float) end : (float) touch_point : (int) ref_value {
	
	float max_interval = fabsf(begin * 2);
	float max_touch_point = touch_point + begin;
	

	
	int seconds_to_go = (ref_value * max_touch_point ) /max_interval;
	
	return seconds_to_go;

}

/**
 *  @drawPathFromPointX
 */
-(void) drawPathFromPointX2 {
    
	bidMirrorLine = [SKShapeNode node];
    
	CGMutablePathRef pathToDraw = CGPathCreateMutable();
	CGPathMoveToPoint(pathToDraw, NULL, mainLineBegin_x, mainLineBegin_y);
	CGPathAddCurveToPoint(pathToDraw, NULL, mainLineBegin_x, mainLineBegin_y, 140, 0, mainLineEnds_x, mainLineEnds_y);
	bidMirrorLine.path = pathToDraw;
	[bidMirrorLine setStrokeColor:[UIColor redColor]];
    
	[self addChild:bidMirrorLine];
	[bidMirrorLine addChild:spreadBlock];
	SKAction *followTrack = [SKAction followPath:bidMirrorLine.path asOffset:NO orientToPath:NO duration:TRADING_BID_TIMER_VALUE];
	
	/** update the bitcoin bid price **/
	[spreadBlock runAction:followTrack completion:^ {
		[self finishInterSpreadFail];
	}];


}



/**
 *	@finishInterSpreadSuccess
 */
-(void) finishInterSpreadSuccess {
	
	[spreadBlock removeAllActions];
	spreadBlockStartPrice = 0;
	[[timer singleton] stopTimer];
	[[gameEventZone singleton] displayInfo:@"Traded!"];

		[spreadBlock runAction:[SKAction fadeOutWithDuration:1] completion:^ {
			
			[self addChild:[self getTimeLineSwipe]];
			[spreadBlock removeFromParent];
			[spreadBlock setBidLineBlock:FALSE :@"bitcoin"];
			
			
			[_globalBitcoinBucket addTradeCoins:1 :[spreadBlock getFixedBidPrice]];
			//[_globalMoneyBucket addTradeCoins:1:[spreadBlock getFixedBidPrice]];
		
			NSString * tradeInfo =[NSString stringWithFormat:@"Bitcoin traded for : %.1f", [spreadBlock getFixedBidPrice]];
			[[gameEventZone singleton] displayInfoTime:tradeInfo:3];
		
			[bidLine setStrokeColor:[UIColor greenColor]];
			[self bidLineFadeColorAction];
			
			interspreadOn = FALSE;
	}];

}


/**
 *	@finishInterSpread
 */
-(void) finishInterSpreadFail  {
	
	if ([spreadBlock isBitCoinBlock]) {
		
		[spreadBlock runAction:[SKAction fadeOutWithDuration:fadeDuration] completion:^ {
			[spreadBlock removeFromParent];
			[bidMirrorLine removeFromParent];
			[_globalBitcoinBucket addTradeCoins:1 :[spreadBlock getTradeInstanceValue]];
			[self displayLabel:false];
			
			
			[bidLine runAction:[SKAction fadeOutWithDuration:0.3] completion:^ {
				
			[[gameEventZone singleton] displayInfoTime:@"No Trade." :2];
				
				[bidLine setStrokeColor:[UIColor redColor]];
				[self bidLineFadeColorAction];
			
				interspreadOn = FALSE;
				
			}];
		}];
	}
}


/**
 *	@bidLineFadeColorAction
 */
-(void) bidLineFadeColorAction {
	
	[bidLine runAction:[SKAction fadeInWithDuration:0.3] completion:^ {
		
		[bidLine runAction:[SKAction waitForDuration:3] completion:^ {
			[bidLine runAction:[SKAction fadeOutWithDuration:0.3] completion:^ {
				[bidLine setStrokeColor:[UIColor whiteColor]];
				[bidLine runAction:[SKAction fadeInWithDuration:0.3] completion:^ {
					
				}];
				
			}];
			
		}];
	}];
}


/**
 *  @switchInterSpreadDisplay
 */
-(void) switchInterSpreadDisplay : (BOOL) display {
    
    if (display) {
        [self runAction:[SKAction fadeAlphaTo:1 duration:fadeDuration]];
        [bidLineLabel runAction:[SKAction fadeAlphaTo:1 duration:fadeDuration]];
        [bidLineLabel setColor:[UIColor blackColor]];
    }
    
    else {
        [self runAction:[SKAction fadeAlphaTo:0.3 duration:fadeDuration]];
        [bidLineLabel runAction:[SKAction fadeAlphaTo:0 duration:fadeDuration]];
        [bidLineLabel setColor:[UIColor whiteColor]];
    }
}




@end


