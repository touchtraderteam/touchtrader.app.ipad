//
//  block.h
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constant.h"


@interface blockCoin : SKSpriteNode {
    
    float blockCoinTradeValue;
    
    NSString * textureImage;
    //SKSpriteNode * bitcoinTextureImage;
    
    BOOL isBeingTraded;
    SKShapeNode * shadow;
}

-(blockCoin *) setMode : (NSString *) option : (int) index;

- (void) removeIsBeingTraded;
- (BOOL) getIsBeingTraded;
- (void) setIsBeingTraded;
- (float) getBlockCoinTradeValue;
- (void) setBlockCoinTradeValue : (float) newBlockCoinTradeValue;

- (void) setShadow : (BOOL) hasShadow;

@end
