//
//  block.m
//  blockGame
//
//  Created by daniel.oliveira on 17/9/14.
//  Copyright (c) 2014 webdaniel. All rights reserved.
//

#import "block.h"
#import <SpriteKit/SpriteKit.h>

@implementation block {
    
    
    
}


-(id) init {
    self = [super init];
    if(self)
    {
        //do something
       
        [self setup:@"block_"];
        
    }
    return self;
}




/**
 *  @setup
 */
-(void) setup :  (NSString *) uniqueName {
    
    defaultColor = [UIColor clearColor];
    defaultSize = CGSizeMake(COIN_DEFAULT_SIZE,COIN_DEFAULT_SIZE);

    isLastDoingTrade = FALSE;
    firstSetValue = TRUE;
    bitcoinValuePercentage = 0;
    
    clonedCoins = [[NSMutableArray alloc]init];
    
    [self setSize:defaultSize];
    [self setName:uniqueName];

    hasShadows = FALSE;
    isIncompleteBlock = FALSE;
}


/**
 *
 */
-(BOOL) isBucketBlock {
    
    if ([[self name] hasPrefix:BLOCK_BUCKET_PREFIX]) {
        return TRUE;
    }
    
    return false;
}

/**
 *
 */
- (BOOL) isDropLineBlock {
    return isDropLineBlock;
    
}


/**
 *
 */
- (BOOL) isBidLineBlock {
    return isBidLineBlock;
    
}

/**
 *
 */
- (void) setBidLineBlock : (BOOL) isBidLineBlock_p : (NSString *) type {
    
    
    isBidLineBlock = isBidLineBlock_p;
    
    // Physics
  
    
    /*
    // It's own category.
    if ([type isEqualToString:@"money"]) {
        self.physicsBody.categoryBitMask = block_money_bid_category;
        
        // Notify when colliding with these categories.
        self.physicsBody.contactTestBitMask = block_bitcoin_bid_category;

    
    }
    else if ([type isEqualToString:@"bitcoin"]) {
        
        self.physicsBody.categoryBitMask = block_bitcoin_bid_category;
        
        // Notify when colliding with these categories.
        self.physicsBody.contactTestBitMask = block_money_bid_category;
    }
     */
  
}

/**
 *
 */
- (void) setDropLineBlock : (BOOL) isDropLineBlock_p {
    isDropLineBlock = isDropLineBlock_p;
}

/**
 *
 */
- (void) setBucketBlock : (BOOL) isBucketBlock_p {
    
    isBucketBlock = isBucketBlock_p;
}


/**
 *
 */
/*
-(void) setGameColorFade : (float) duration {
    
    UIColor * tempColor;
    
    if (isBitCoin) {
        tempColor = BITCOIN_BLUE_COLOR;
    }
    else {
        tempColor = MONEY_GREEN_COLOR;
    }
    
    SKAction * colorize = [SKAction colorizeWithColor:tempColor colorBlendFactor:1 duration:duration];
    
    [self runAction:colorize completion:^{
        
    }];
    
    
}
 */
/**
 *  IMPORTANT VARIABLES
 *  blockQuantity;
 *  fullBlockQuantityTradeValue
 */


/**
 *
 */
- (void) hidePotentialProfit {
    
    if (potentialProfitWrapper != nil) {
        [potentialProfitWrapper runAction:[SKAction fadeOutWithDuration:0.2]];
    }
    
}


/**
 *  @hideMeatMap
 */
-(void) hideHeatMap {
    
    if (heatMapEffect != nil) {
        [heatMapEffect runAction:[SKAction fadeOutWithDuration:0.2]];
    }
    
}


/**
 *  @showHeatMap
 */
- (void) showHeatMapBlockBucket : (float) compareTargetBlockCoinValue {
    if (heatMapEffect == nil) {
        

        float width = coinTextureImage.frame.size.width;
        float height = coinTextureImage.frame.size.height;
        float position_x = 0-coinTextureImage.frame.size.width/2;
        float position_y = 0-coinTextureImage.frame.size.height/2;

        
        CGRect circle = CGRectMake(position_x, position_y, width,height);
        SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
        
        shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
        if ([self getBitcoinTradePercentalValue] < compareTargetBlockCoinValue) {
            shapeNode.strokeColor = HEATMAP_CIRCLE_PROFIT_COLOR;
        }
        
        else {
            shapeNode.strokeColor = HEATMAP_CIRCLE_LOSS_COLOR;
        }
        
        shapeNode.lineWidth = 10;
        shapeNode.fillColor = [UIColor clearColor];
        shapeNode.antialiased = TRUE;
        
        
        heatMapEffect = [[SKSpriteNode alloc]init];
        heatMapEffect.alpha = 1;
        heatMapEffect.size = CGSizeMake(shapeNode.frame.size.width,shapeNode.frame.size.height);
        heatMapEffect.zPosition = HEATMAP_CIRCLE_ZPOSITION;
        heatMapEffect.position = CGPointMake(0, 0);
        heatMapEffect.anchorPoint = CGPointMake(0.5,0.5);
        
        [heatMapEffect addChild:shapeNode];
        [self addChild:heatMapEffect];
        
    }
    

    [heatMapEffect runAction:[SKAction fadeInWithDuration:0.1]];
}

/**
 *  @showHeatMap
 */
- (void) showHeatMap : (float) compareTargetBlockCoinValue {
    
    if (heatMapEffect == nil) {
    
        float padding = 2;
        float width  = self.frame.size.width+padding;
        float height = self.frame.size.height+padding;
        
        float position_x = 0-self.frame.size.width/2-padding;
        float position_y = 0-self.frame.size.height/2-padding;
        
        float profit_pixels = 0;
        
        if ([self isBitCoinBlock]) {
            float profit = 100 - (compareTargetBlockCoinValue*100) / [self getBitcoinTradePercentalValue];
            if (profit > 20) {
                profit_pixels = 20;
            }
            else {
                profit_pixels = profit;
            }
        }
        
        float profit_circle_padding = 5;
        
        if (compareTargetBlockCoinValue != 0) {
            profit_circle_padding = (profit_pixels + profit_circle_padding);
        }

        
        if ([self isDropLineBlock]) {
            width = coinTextureImage.frame.size.width+profit_circle_padding;
            height = coinTextureImage.frame.size.height+profit_circle_padding;
            position_x = 0-coinTextureImage.frame.size.width/2 - (profit_circle_padding/2);
            position_y = 0-coinTextureImage.frame.size.height/2 - (profit_circle_padding/2);
        }
        
        CGRect circle = CGRectMake(position_x, position_y, width,height);
        SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
        
        shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
        shapeNode.fillColor = HEATMAP_CIRCLE_PROFIT_COLOR; 
        shapeNode.lineWidth = 0;
        shapeNode.antialiased = TRUE;
    
        
        heatMapEffect = [[SKSpriteNode alloc]init];
        heatMapEffect.alpha = 0.7;
        heatMapEffect.size = CGSizeMake(shapeNode.frame.size.width,shapeNode.frame.size.height);
        heatMapEffect.zPosition = HEATMAP_CIRCLE_ZPOSITION;
        heatMapEffect.position = CGPointMake(0, 0);
        heatMapEffect.anchorPoint = CGPointMake(0.5,0.5);

        [heatMapEffect addChild:shapeNode];
        [self addChild:heatMapEffect];

    }

    
    [heatMapEffect runAction:[SKAction fadeInWithDuration:0.2]];
    
}


/**
 *  @showPotentialProfit
 */
- (void) showPotentialProfit  : (float) valueToTradeCoin {
    
    
    
    if ([self isBitCoinBlock]) {
        
    if (potentialProfit == nil) {
        potentialProfitWrapper = [[SKSpriteNode alloc]init];
        potentialProfitWrapper.color = SLICK_GRAY;
        potentialProfitWrapper.size = CGSizeMake(80,20);
        potentialProfitWrapper.position = CGPointMake(self.frame.size.width + 20, 5);
        potentialProfitWrapper.alpha = 0;
        potentialProfitWrapper.zPosition = potentialProfitWrapper_ZPOSITION;

        
        potentialProfit = [[SKLabelNode alloc]init];
        potentialProfit.fontSize = 20;
        potentialProfit.fontName = SLICK_OPTIMA_BOLD;
        potentialProfit.fontColor = [UIColor blackColor];
        potentialProfit.position = CGPointMake(0, -7);

    
        [potentialProfitWrapper addChild:potentialProfit];
        [self addChild:potentialProfitWrapper];
    }
    
    // value to trade proportional to the bitcoin value.
    if ([self isIncompleteBlock]) {
        if ([self isBitCoinBlock]) {
            valueToTradeCoin = valueToTradeCoin * bitcoinValuePercentage;
        }
    }
    
    UIColor * profitColor;
    float possibleProfit = 0;
    NSString * sign;
    
    
    if (valueToTradeCoin < blockInstanceValue) {
        possibleProfit = blockInstanceValue - valueToTradeCoin;
        sign = @"-";
        profitColor = HEATMAP_LABEL_LOSS_COLOR; //[UIColor colorWithRed:111.0f/255.0f green:29.0f/255.0f blue:22.0f/255.0f alpha:1.0];
    }
    
    else if (valueToTradeCoin >= blockInstanceValue) {
        possibleProfit = valueToTradeCoin - blockInstanceValue;
        profitColor = HEATMAP_LABEL_PROFIT_COLOR;
        sign= @"+";
    }
    
    float possibleProfitPercentage = possibleProfit/blockInstanceValue;
    
    NSLog(@"VALUE TO TRADE COIN: %f", valueToTradeCoin);
    
    potentialProfit.color = profitColor;
    potentialProfit.fontColor = profitColor;
    potentialProfit.text =[NSString stringWithFormat:@"%@ %.2f",sign, possibleProfitPercentage];
    [potentialProfitWrapper runAction:[SKAction fadeInWithDuration:0.3]];
        
    }
}

/**
 *  @addOneCoin
 */
-(void) addOneCoin : (block *) oneCoin {
    
    if ([self isDropLineBlock]) {
 
        float blockQuantityToAdd = [oneCoin getTradeInstanceValue]/originalBlockValue;
        NSLog(@"block quantity before: %f", blockQuantity);
        
        blockQuantity += blockQuantityToAdd;
        NSLog(@"WILL ADD: %f", blockQuantityToAdd);
        fullBlockQuantityTradeValue += [oneCoin getTradeInstanceValue];
        [self updateQuantityEffectLabel:blockQuantity];
        [self updateQuantityEffect];
        [self resetPulse];
    }
    
    else {
        NSLog(@"ERROR: This is not a dropline block.");
    }
    
}

/**
 *
 */
-(float) getNumberOfBlockCoinsBeingTraded {
   
    if (isDoingTrade) {
        return numberOfBlocksCoinsBeingTraded;
    }
    
    else {
        NSLog(@"CANNOT CAll this function if the block is not being traded");
    }
    
    return 0;
}

/**
 *  @getOriginalBlockValue
 */
-(float) getOriginalBlockValue {

    return originalBlockValue;

}


/**
 *  @getBlockTradeDifference
 */
- (float) getBlockTradeDifference : (float) lastTradeDone {
    
    return (fabs(lastTradeDone) * 1) / originalBlockValue;

}



/**
 *  @getsAnActionThat makes the block slide
 */
- (void) runSlideDropLineAction : (SKSpriteNode *) blockCoinInstance : (BOOL) lastBlockCoin : (BOOL) fullTrade {

    CGPoint slideHorizontalPoint;

    if (blockCoinFullSlideAction != nil) {
    
    }
    
    else {
        
        if ([[self parent] isKindOfClass:[dropLine class]]) {
    
            SKAction * slideY;
            if([self isBitCoinBlock]) {
                slideY = [SKAction moveBy:CGVectorMake(0, -100) duration:BLOCK_TRADE_FADE_DURATION/2];
                slideHorizontalPoint = [self convertPoint:CGPointMake(365, 0) fromNode:[self parent]];
            }
        
            else {
                slideY = [SKAction moveBy:CGVectorMake(0, 100) duration:BLOCK_TRADE_FADE_DURATION/2];
                slideHorizontalPoint = [self convertPoint:CGPointMake(-363, 0.5) fromNode:[self parent]];
            }
        
            SKAction * fadeOut = [SKAction fadeOutWithDuration:0.2];
            slideDropLine = [SKAction moveTo:slideHorizontalPoint duration:BLOCK_TRADE_FADE_DURATION/2];
            blockCoinFullSlideAction = [SKAction sequence:@[slideY,fadeOut]];
         
        }
    }
    

    [blockCoinInstance runAction:slideDropLine completion:^ {
        blockCoinInstance.zPosition = 10;
        [blockCoinInstance runAction:blockCoinFullSlideAction completion:^ {
            
            if (lastBlockCoin) {
                if (fullTrade) {
                    [self coinDropLineFinalTrade];
                }
                else {
                    [blockCoinInstance removeFromParent];
                    [self coinDropLineFinishTrade];
                }
                
            }
        }];
    }];
    
}



/**
 *  coinDropLineFinalTrade
 */
-(void) coinDropLineFinalTrade {
    
    if ([[self parent] isKindOfClass:[dropLine class]]) {
        dropLine * dropLineFather = (dropLine *) [self parent];
        if (isLastDoingTrade) {
            [dropLineFather dropLineFinishTrade_callback:numberOfBlocksCoinsBeingTraded:fullBlockQuantityTradeValue];
            [self removeFromParent];
        }
        else {
            [dropLineFather dropLineAppendTrade_callback:numberOfBlocksCoinsBeingTraded:fullBlockQuantityTradeValue];
            [self removeFromParent];
        }
        
    }
    else {
        NSLog(@" coinDropLineFinalTrade ERROR");
    }
    
    [self runAction:[SKAction fadeOutWithDuration:1]];
    isDoingTrade = false;

    
}

/**
 *  @coinDropLineFinishTrade
 */
-(void) coinDropLineFinishTrade {
    
    float valueToDecrease = numberOfBlocksCoinsBeingTraded * blockInstanceValue;
    isDoingTradeForValue = 0;
    fullBlockQuantityTradeValue-=valueToDecrease;
    blockQuantity -= numberOfBlocksCoinsBeingTraded;

    [self updateQuantityEffectLabel:blockQuantity];
    [self blockHeadCoinZoomDownAction];
    [self updateQuantityEffect];
    [self setLastDoingTrade:FALSE];
    
    isDoingTrade = FALSE;
    
    if ([[self parent] isKindOfClass:[dropLine class]]) {
        dropLine * dropLineFather = (dropLine *) [self parent];
        [dropLineFather dropLineFinishTrade_callback:numberOfBlocksCoinsBeingTraded:valueToDecrease];
    }
    
    //1. update
    numberOfBlocksCoinsBeingTraded = 0;
}


/**
 *  @doFullTrade
 */
- (void) doFullTrade {
    
    numberOfBlocksCoinsBeingTraded = blockQuantity;
    fullBlockQuantityTradeValue = blockQuantity * originalBlockValue;
    [self runSlideDropLineAction:[self cloneBlockCoinImage]:TRUE:TRUE];
    
}


/**
 *  @doPartialMoneyTraded
 */
-(void) doPartialMoneyTrade: (float) percentage {
    
    numberOfBlocksCoinsBeingTraded = percentage;
    fullBlockQuantityTradeValue = blockQuantity * originalBlockValue;
    [self runSlideDropLineAction:[self cloneBlockCoinImage]:TRUE:FALSE];
    
}
/**
 *  @doPartialTrade is for a whole block that the trading couldn't "break"
 *
 */
-(void) doPartialTrade {
    
    float blocksToGenerate = numberOfBlocksCoinsBeingTraded;
    float i;
    BOOL lastBlock = FALSE;
    
    // Just a small bitcoin block.
    if (blocksToGenerate < 1) {
        [self runSlideDropLineAction:[self cloneBlockCoinImage]:TRUE:FALSE];
    }
    
    else {
        for (i=0; i < blocksToGenerate; i++) {
            if (i+1 > blocksToGenerate) {
                lastBlock = TRUE;
            }
            [self runSlideDropLineAction:[self cloneBlockCoinImage]:lastBlock:FALSE];
        }
    }
}


/**
 *  Retur
 */
- (float) getBlockBeingTradeValue {
    
    if (isDoingTrade) {
       return isDoingTradeForValue;
    }
    else {
        NSLog(@"Cannot call this function if block is not doing trade");
    }
    
    return 0;
}


/**
 *
 */
- (void) resetDropLineRotation : (float) rotateValue : (float) rotateTime {
    
    if (isBitCoin) {
        rotateValue = -(rotateValue);
    }
    SKAction *revertBlock = [SKAction sequence:@[[SKAction rotateByAngle:blockDegToRad(rotateValue) duration:rotateTime]]];
    [self runAction:revertBlock];
    
}



/**
 *  @blockZoomUpAction
 */
-(void) blockZoomUpAction {
    
    SKAction * zoom = [SKAction scaleTo:1.5 duration:0.25];

    [self runAction:zoom];
    
}

/**
 *  @blockHeadCoinZoomUpAction
 */
- (void) blockHeadCoinZoomUpAction {
    
    SKAction * zoom = [SKAction scaleTo:1.1 duration:0.25];
    [[self getBlockCoinImage] runAction:zoom];
    
}

/**
 *  @blockHeadCoinZoomUpAction
 */
- (void) blockHeadCoinZoomDownAction {
    
    SKAction * zoom = [SKAction scaleTo:1.0 duration:0.25];
    [[self getBlockCoinImage] runAction:zoom];
    
}

/**
 *  @blockZoomDownAction
 */
-(void) blockZoomDownAction {
    
    SKAction * zoom = [SKAction scaleTo:1.0 duration:0.25];
    
    [self runAction:zoom];
    
}


/**
 * @resetSpin
 */
-(void) resetSpin {
    
    SKAction *resetSpin = [SKAction sequence:@[[SKAction rotateByAngle:blockDegToRad(0.0f) duration:0.2]]];
    //SKAction *resetSpin = [SKAction sequence:@[[SKAction rotateByAngle:blockDegToRad(0.0f) duration:0.2]]];
    [self runAction:resetSpin completion:^{
        [self removeAllActions];
    }];

   
}


/**
 *  Sets a dropline block ready to trade
 */
-(void) setDoingTrade : (BOOL) tradeStatus : (float) blockBucketTradeValue {
    
    isDoingTrade = tradeStatus;
    isDoingTradeForValue = blockBucketTradeValue;
    [self setCountCoinsToBeTraded];
 
}

/**
 *
 */
-(BOOL) isDoingTrade {
    
    return isDoingTrade;
}



/**
 *  @countCoinsToBeTraded
 */
-(void) setCountCoinsToBeTraded {

    float valueToTrade = isDoingTradeForValue;

    NSLog(@"----- TOUCH-----");
    NSLog(@"EXCHANGING FOR : %f", isDoingTradeForValue);

    numberOfBlocksCoinsBeingTraded = 0;

    
    if (valueToTrade > fullBlockQuantityTradeValue) {
        numberOfBlocksCoinsBeingTraded = fullBlockQuantityTradeValue/[self getTradeValue];
    }
    else {
        float possibleBlockOfCoins = isDoingTradeForValue / [self getTradeValue];
        numberOfBlocksCoinsBeingTraded = possibleBlockOfCoins;
    }
   
    NSLog(@" setCountCoinsToBeTraded: numberOfBlocksCoinsBeingTraded: %f", numberOfBlocksCoinsBeingTraded);

}



/**
 *  @pulseBlockTimes
 */
-(SKAction *) pulseBlockTimesObject : (int) numberOfTimes : (float) timeDuration  {
    
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.2 duration:timeDuration];
    SKAction *fadeIn = [SKAction fadeInWithDuration: timeDuration];
    SKAction *pulse = [SKAction sequence:@[fadeOut,fadeIn]];
    
    //SKAction *pulseThreeTimes = [SKAction repeatAction:pulse count:3];
    SKAction *pulseNumberOfTimes = [SKAction repeatAction:pulse count:numberOfTimes];
    
    return pulseNumberOfTimes;
 
    
}

/**
 *  @pulseBlockTimes
 */
-(void) pulseBlockTimes : (int) numberOfTimes : (float) timeDuration {
    
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.2 duration:timeDuration];
    SKAction *fadeIn = [SKAction fadeInWithDuration: timeDuration];
    SKAction *pulse = [SKAction sequence:@[fadeOut,fadeIn]];
    
    //SKAction *pulseThreeTimes = [SKAction repeatAction:pulse count:3];
    SKAction *pulseNumberOfTimes = [SKAction repeatAction:pulse count:numberOfTimes];
    
    [self runAction:pulseNumberOfTimes];

}

/**
 * @pulseBlockForever
 */
-(void) pulseBlockForever {
    
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.2 duration:0.6];
    SKAction *fadeIn = [SKAction fadeInWithDuration: 0.6];
    SKAction *pulse = [SKAction sequence:@[fadeOut,fadeIn]];
    
    //SKAction *pulseThreeTimes = [SKAction repeatAction:pulse count:3];
    SKAction *pulseForever = [SKAction repeatActionForever:pulse];
    
    [self runAction:pulseForever];
    
}

/**
 *  @resetPulse
 */
-(void) resetPulse {
    
    [self removeAllActions];
    SKAction *fadeIn = [SKAction fadeInWithDuration: 0.3];
    [self runAction:fadeIn];
    
}


/**
 *  @rotateBlockFull
 */
/*
-(void) rotateBlockFull {
    SKAction *spinAction = [SKAction sequence:@[[SKAction rotateByAngle:blockDegToRad(-90.0f) duration:0.2]]];
    [self runAction:spinAction];
}
 */


/**
 *
 */
-(BOOL) isBitCoinBlock {
    return isBitCoin;
}

/**
 *
 */
-(BOOL) isMoneyBlock {

    if (isBitCoin == false) {
      return TRUE;
    }

    return FALSE;
}


/**
 *  @getBidModePrice
 */
- (float) getBidModePrice : (float) bottomPoint : (float) topPoint : (float) currentContactPoint {
    
  
    float newTopPoint = topPoint-bottomPoint;
    float newCurrentContactPoint = currentContactPoint - bottomPoint;
    
    float bidDifference = bitcoin_orderbook_bestprice - money_orderbook_bestprice;
    float newBidPrice = 0;
    
    if (bidDifference > 0) {
        float addValue = newCurrentContactPoint * bidDifference/newTopPoint;
        newBidPrice = money_orderbook_bestprice + addValue;
    }
    else {
        float addValue = newCurrentContactPoint * bidDifference/newTopPoint;
        newBidPrice = money_orderbook_bestprice + addValue;
        
    }
    
    // Make the average according to the bidline coordinates.
    return newBidPrice;
    
}

/**
 *  @setBidMode
 */
-(void) setBidMode : (float) topPoint : (float) bottomPoint : (float) currentContactPoint {
    
    if (bidLabel == nil) {
        bidLabel = [[SKLabelNode alloc]init];
        bidLabel.fontSize = 16;
        bidLabel.fontName = SLICK_OPTIMA_BOLD;
        bidLabel.position = CGPointMake(self.frame.size.width, self.frame.size.height - 20);
        bidLabel.color = SLICK_BLACK;
        bidLabel.fontColor = SLICK_BLACK;
        [self addChild:bidLabel];
    }
 
    
    dynamicBidPrice = [self getBidModePrice:bottomPoint:topPoint:currentContactPoint];
    if ([self isIncompleteBlock]) {
        dynamicBidPrice = dynamicBidPrice * bitcoinValuePercentage;
    }
    
    
    bidLabel.text =[NSString stringWithFormat:@"%.1f", dynamicBidPrice];
    [bidLabel runAction:[SKAction fadeInWithDuration:0.3]];
}


/**
 *  @continueBidMode
 */
-(void) continueBidMode : (float) topPoint : (float) bottomPoint : (float) currentContactPoint {
    
    dynamicBidPrice = [self getBidModePrice:bottomPoint:topPoint:currentContactPoint];
    if ([self isIncompleteBlock]) {
        dynamicBidPrice = dynamicBidPrice * bitcoinValuePercentage;
        float fullTempValue = fullBlockValue / bitcoinValuePercentage;
        bidLabel.text =[NSString stringWithFormat:@"%.1f / %.0f", dynamicBidPrice, fullTempValue];
    }
    
    else {
        bidLabel.text =[NSString stringWithFormat:@"%.1f", dynamicBidPrice];
    }
    
 
}

/**
 *  @getDynamicBidPrice
 */
-(float) getDynamicBidPrice {
    return dynamicBidPrice;
}


/**
 *  @getFixedBidPrice
 */
-(float) getFixedBidPrice {
    return fixedBidPrice;
}

/**
 *  @setFixedBidMode
 */
-(void) setFixedBidMode : (float) fixedBidPrice_p {
    
    fixedBidPrice = fixedBidPrice_p;
    
    if (fixedBidLabel == nil) {
        fixedBidLabel = [[SKLabelNode alloc]init];
        fixedBidLabel.fontSize = 16;
        fixedBidLabel.fontName = SLICK_OPTIMA_BOLD;
        fixedBidLabel.position = CGPointMake(self.frame.size.width, self.frame.size.height - 20);
        fixedBidLabel.fontColor = SLICK_BLACK;
        [self addChild:fixedBidLabel];
    }
    
    fixedBidLabel.text =[NSString stringWithFormat:@"%.1f", fixedBidPrice];
    [fixedBidLabel runAction:[SKAction fadeInWithDuration:0.3]];
    [bidLabel runAction:[SKAction fadeOutWithDuration:0.1]];
    [bidLabel setHidden:true];
    
    [self decreaseFixedBidPrice];
}

/**
 * @pulseBlockForever
 */
-(void) decreaseFixedBidPrice {
    
    
    //float fullTime = TRADING_BID_TIMER_VALUE; // 10 seconds
    
    float lowest_value = [[_globalMoneyDropLineBucket getLowestDropLineBucketCoin] getTradeInstanceValue];
    if ([self isIncompleteBlock]) {
        lowest_value = lowest_value * bitcoinValuePercentage;
    }
    
    float value_to_decrement;
    if (fixedBidPrice > lowest_value) {
       value_to_decrement = (fixedBidPrice - lowest_value) / TRADING_BID_TIMER_VALUE;
    }
    else {
        value_to_decrement = (lowest_value - fixedBidPrice ) / TRADING_BID_TIMER_VALUE;
    }
    
    
    
    
    id wait = [SKAction waitForDuration:1];
    id run = [SKAction runBlock:^ {
        fixedBidPrice = fixedBidPrice - value_to_decrement;
       fixedBidLabel.text =[NSString stringWithFormat:@"%.1f", fixedBidPrice];
    }];
    
    [self runAction:[SKAction sequence:@[wait, run]]];
    
    [self runAction:[SKAction repeatActionForever:[SKAction sequence:@[wait, run]]]];
 
    
}



/**
 *  @dropBidMode
 */
-(void) dropBidMode {

    if (bidLabel != nil) {
        [bidLabel runAction:[SKAction fadeOutWithDuration:0.3]];
        dynamicBidPrice = 0;
    }
    
}



/**
 *  @blockDegToRad
 */
float blockDegToRad(float degree) {
	return degree / 180.0f * M_PI;
}


/**
 *  @setCollisionInteraction
 */
-(void) setCollisionInteraction {
    
    // Physics
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
    self.physicsBody.dynamic = YES;
    
    // It's own category.
    self.physicsBody.categoryBitMask = block_category;
    
    // Notify when colliding with these categories.
    self.physicsBody.contactTestBitMask = bucket_category;
    //self.physicsBody.contactTestBitMask = gamecontainer_category;
    self.physicsBody.contactTestBitMask = droplinebucket_category;
    self.physicsBody.contactTestBitMask = wallet_money_category;
    self.physicsBody.contactTestBitMask = bidline_category;
    
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;
    //self.anchorPoint = CGPointMake(0.5,0.5);
    
    self.anchorPoint = CGPointZero;
    
}


/**
 *  @setCollisionInteractionWalletCoin
 */
-(void) setDropLineCoinCollisionInteraction {
    
    // Physics
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
    self.physicsBody.dynamic = YES;
    
    // It's own category.
    self.physicsBody.categoryBitMask = dropline_block_category;
    
    // Notify when colliding with these categories.
    self.physicsBody.contactTestBitMask = block_category;
    
    
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;
    //self.anchorPoint = CGPointMake(0.5,0.5);
    self.anchorPoint = CGPointZero;
}


/**
 *  @setCollisionInteractionWalletCoin
 */
-(void) setWalletCoinCollisionInteraction {
    
    // Physics
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
    self.physicsBody.dynamic = YES;
    
    // It's own category.
    self.physicsBody.categoryBitMask = block_category;
    
    // Notify when colliding with these categories.
    self.physicsBody.contactTestBitMask = bucket_category;

    
    // Collision mask set to zero for no bouncing or such effect.
    self.physicsBody.collisionBitMask = 0;
    self.anchorPoint = CGPointMake(0.5,0.5);
    
}


/**
 *  @removeCollisionInteraction
 */
-(void) removeCollisionInteraction {
    self.physicsBody.dynamic = NO;
    self.physicsBody.categoryBitMask = 0;
    self.physicsBody.contactTestBitMask = 0;
}


/**
 * @setInsideContainer
 */
-(void) setInsideContainer : (NSString *) currentContainer_p {
 
    currentContainer = currentContainer_p;
    
    // Remove collision of the block according if it's inside the bucket or not.
    if ([self isBucketBlock]) {
        [self setCollisionInteraction];
    }
    else {
        [self removeCollisionInteraction];
    }
}


/**
 *  @isInside
 */
-(BOOL) isInside: (NSString *) currentContainer_p {
   
    if([currentContainer isEqualToString:currentContainer_p]) {
        return TRUE;
    }
    return FALSE;
}


/**
 *  @Stacks the trade value of this block
 */
-(void) stackTradeValueAndSize : (float) blockInstanceValue_p : (float) blockQuantity_p {
    
    
    
    fullBlockValue = blockInstanceValue + blockInstanceValue_p; //* blockQuantity_p;
    blockInstanceValue = blockInstanceValue + blockInstanceValue_p;
    isIncompleteBlock = [self checkIncompleteBlock];
    
    [self setupGlassValueEffect:blockInstanceValue];
    [self attachPriceLabel];
}


/**
 * @checkIncompleteBlock
 */
-(BOOL) checkIncompleteBlock {
    
    BOOL value;
    
    if ([self isBitCoinBlock] && [self isBucketBlock]) {
        
        bitcoinValuePercentage = fullBlockValue /  bitcoin_reference_lowest_value;
        
        if (bitcoinValuePercentage < 1) {
            value = TRUE;
        }
        else {
            value = FALSE;
        }
        
        return value;
    }
    
    return FALSE;
}




/**
 *  @attachQuantityEffect
 */
-(void) attachQuantityEffect {
 
    int width_size = COIN_DEFAULT_SIZE;
    float newBlockWidth = COIN_DEFAULT_SIZE;
    quantityLongBar = [[SKSpriteNode alloc]init];
    if (blockQuantity == 1) {
        
    }
    
    if (hasShadows == FALSE) {
        
        float barSize = (COIN_DEFAULT_SIZE / 4) * blockQuantity;
        
        if (barSize > COIN_MAX_BAR_WIDTH) {
            barSize = COIN_MAX_BAR_WIDTH;
        }
        else if (barSize < COIN_MIN_BAR_WIDTH) {
            barSize = COIN_MIN_BAR_WIDTH;
        }
        
        
        width_size = barSize;
        newBlockWidth = self.frame.size.width + barSize;
        
        // Block has a different width now.
        
        SKSpriteNode * quantityLongBarInside = [[SKSpriteNode alloc]init];
        SKSpriteNode * quantityLongBarAlpha = [[SKSpriteNode alloc]init];
        quantityLongBarInside.name = @"barInside";
        quantityLongBarAlpha.name = @"barAlpha";
        
        [quantityLongBar setSize:CGSizeMake(width_size,COIN_DEFAULT_SIZE/2)];
        
        quantityLongBarInside.size = quantityLongBar.size;
        [quantityLongBarInside setAlpha:1];
        [quantityLongBarInside setColor:[UIColor blackColor]];
        quantityLongBarInside.zPosition = 1;
        
        //Alpha
        quantityLongBarAlpha.size = quantityLongBar.size;
        quantityLongBarAlpha.zPosition = 0;
        [quantityLongBarAlpha setColor:[UIColor blackColor]];
        [quantityLongBarAlpha setAlpha:0.3];
        
        [quantityLongBar addChild:quantityLongBarInside];
        [quantityLongBar addChild:quantityLongBarAlpha];
        
        
        if ([self isBitCoinBlock]) {
            [quantityLongBar setAnchorPoint:CGPointMake(1, 0)];
            [quantityLongBarInside setAnchorPoint:CGPointMake(1,0)];
            [quantityLongBarAlpha setAnchorPoint:CGPointMake(1,0)];
        }
        else {
            [quantityLongBar setAnchorPoint:CGPointMake(0, 0)];
            [quantityLongBarInside setAnchorPoint:CGPointZero];
            [quantityLongBarAlpha setAnchorPoint:CGPointZero];

        }
        
        
        [self addChild:quantityLongBar];
        [self quantityLongBarSettings];
        
        
        if (blockQuantity > 1) {
          //  newBlockWidth += priceLabel.frame.size.width;
        }
        
        [self setSize:CGSizeMake(newBlockWidth, COIN_DEFAULT_SIZE)];
        [self attachQuantityEffectLabel];
        
        [self setAnchorPoint:CGPointMake(1, 1)];
        //[self setCollisionInteraction];
        
    }
    
    else {
        NSLog(@"Attach quantity blocks already added");
    }
    
    hasShadows = TRUE;
}

/**
 *  @updateQuantityEffect
 */
-(void) updateQuantityEffect {
    
    float maxBarSize = quantityLongBar.frame.size.width -3;
    
    float newBarSize = (COIN_DEFAULT_SIZE/4) * blockQuantity;
    
    if (newBarSize > maxBarSize) {
        newBarSize = maxBarSize;
        
        if (quantityBreakLine == nil) {
            quantityBreakLine = [[SKSpriteNode alloc] init];
            quantityBreakLine.name = @"quantitybreakline";
            [quantityBreakLine setSize:CGSizeMake(5, 0)];
            [quantityBreakLine setPosition:CGPointMake(quantityLongBar.frame.size.width+1,12)];
            if ([self isBitCoinBlock]) {
               [quantityBreakLine setColor:[UIColor colorWithRed:230.0f/255.0f green:133.0f/255.0f blue:32.0f/255.0f alpha:1.0]];
            }
            else {
                [quantityBreakLine setColor:[UIColor colorWithRed:106.0f/255.0f green:168.0f/255.0f blue:79.0f/255.0f alpha:1.0]];
            }
            
            quantityBreakLine.zPosition = 5;
            //[quantityLongBar addChild:quantityBreakLine];
            //[quantityBreakLine runAction:[SKAction resizeToHeight:quantityLongBar.frame.size.height*1.5 duration:1]];
        }
        
        
    }
    
    if (newBarSize <  COIN_MIN_BAR_WIDTH ) {
        newBarSize = COIN_MIN_BAR_WIDTH;
    }
    else if (newBarSize > COIN_MAX_BAR_WIDTH) {
        newBarSize = COIN_MAX_BAR_WIDTH;
    }
    
    float newLabelPoint = newBarSize - quantityEffectQuantityLabel.frame.size.width/2;
    if ([self isBitCoinBlock] && [self isDropLineBlock]) {
        newLabelPoint =  0 - newBarSize + quantityEffectQuantityLabel.frame.size.width + 10;
    }

    [[quantityLongBar childNodeWithName:@"barInside"] runAction:[SKAction resizeToWidth:newBarSize duration:1]];
 
}


/**
 *  @quantityLongBarSettings
 */
- (void) quantityLongBarSettings {
    
    SKAction *resetSpin;
    float bar_y = 0-COIN_DEFAULT_SIZE/3+3;
   
    if ([self isBitCoinBlock]) {
        resetSpin = [SKAction sequence:@[[SKAction rotateByAngle:blockDegToRad(2) duration:0.2]]];
       // [(SKSpriteNode *)[quantityLongBar childNodeWithName:@"barInside"] setColor:[UIColor colorWithRed:249.0f/255.0f green:111.0f/255.0f blue:20.0f/255.0f alpha:1.0]];
    }
    else {
        bar_y = 0 - COIN_DEFAULT_SIZE/4 -2;
        resetSpin = [SKAction sequence:@[[SKAction rotateByAngle:blockDegToRad(-2) duration:0.2]]];
       //[(SKSpriteNode *)[quantityLongBar childNodeWithName:@"barInside"] setColor:[UIColor blackColor]];
    }
    
    [(SKSpriteNode *)[quantityLongBar childNodeWithName:@"barInside"] setColor:[UIColor colorWithRed:0.17 green:0.17 blue:0.17 alpha:1.0]];

    quantityLongBar.zPosition = HEATMAP_QUANTITYBAR_ZPOSITION;
    [quantityLongBar setPosition:CGPointMake(0, bar_y)];
    [self runAction:resetSpin];
    
}


-(SKSpriteNode *) getQuantityLongBar {
    return quantityLongBar;
}


/**
 *  @updateQuantityEffectLabel
 */
- (void) updateQuantityEffectLabel : (float) newValue {
    
    if( [self floatHasDecimals:blockQuantity]) {
        
        quantityEffectQuantityLabel.text =[NSString stringWithFormat:@"x %.2f", blockQuantity];
        [quantityEffectQuantityLabel runAction:[self pulseBlockTimesObject:2 :0.7] completion:^ {
            
            [quantityEffectQuantityLabel runAction:[SKAction fadeOutWithDuration:0.2] completion:^ {
                quantityEffectQuantityLabel.text =[NSString stringWithFormat:@"x %.0f", blockQuantity];
                [quantityEffectQuantityLabel runAction:[SKAction fadeInWithDuration:0.2]];

            }];
            
        }];
  
        
    
    }
    
    else {
        
        quantityEffectQuantityLabel.text =[NSString stringWithFormat:@"x %.0f", blockQuantity];
    }
}

/**
 *  @attachQuantityEffectLabel
 */
- (void) attachQuantityEffectLabel {
    
    UIColor * labelBackColour = [UIColor colorWithRed:223/255.0f green:159.0f/255.0f blue:22.0f/255.0f alpha:1.0];
    
    if (![self isBitCoinBlock]) {
        labelBackColour = [UIColor colorWithRed:93.0/255.0f green:149.0f/255.0f blue:49.0f/255.0f alpha:1.0];
    }
    
    quantityEffectQuantityLabel = [[SKLabelNode alloc]init];
    quantityEffectQuantityLabel.text =[NSString stringWithFormat:@"x %.0f", blockQuantity];
    quantityEffectQuantityLabel.fontSize = 16;
    quantityEffectQuantityLabel.fontName = SLICK_OPTIMA_BOLD;
    quantityEffectQuantityLabel.fontColor = [UIColor whiteColor];
    
    quantityEffectQuantityLabel.position = CGPointMake(0+quantityLongBar.frame.size.width - quantityEffectQuantityLabel.frame.size.width, 8);
    quantityEffectQuantityLabel.zPosition = 400;


    if ([self isBitCoinBlock]) {
        CGPoint label_point = CGPointMake(0-quantityLongBar.self.frame.size.width + quantityEffectQuantityLabel.frame.size.width -5, quantityEffectQuantityLabel.frame.size.height/2);
        [quantityEffectQuantityLabel setPosition:label_point];
    }
    
    [quantityLongBar addChild:quantityEffectQuantityLabel];
    
}


/**
 *  @Set the trade value of this block
 */
-(void) updateCoinValueSettings : (float) blockInstanceValue_p : (float) blockQuantity_p {
    
    blockInstanceValue = blockInstanceValue_p;
    blockQuantity = blockQuantity_p;
    fullBlockValue = blockInstanceValue_p;
    fullBlockQuantityTradeValue = blockInstanceValue_p * blockQuantity_p;
    
    if (firstSetValue) {
        originalBlockValue = blockInstanceValue_p;
        firstSetValue = FALSE;
    }
    
    isIncompleteBlock = [self checkIncompleteBlock];
    [self attachPriceLabel];
    
    if ([self isBucketBlock] || [self isDropLineBlock]) {
        [self setupGlassValueEffect:blockInstanceValue_p];
    }
    
    
    if (![self isBucketBlock] && ![self isWalletCoin]) {
        if (blockQuantity > 1) {
            [self attachQuantityEffect];
        }
    }
}



/**
 *  @Returns true if it's an incomplete block
 */
- (BOOL) isIncompleteBlock {
    return isIncompleteBlock;
}

/**
 *  @Only related to bitcoins for percentual values.
 */
- (float) getPercentualValue {
    return bitcoinValuePercentage;
}

/**
 *
 */
-(float) getBitcoinTradePercentalValue {
    
    if ([self isIncompleteBlock]) {
        return blockInstanceValue/bitcoinValuePercentage;
    }
    else {
        return blockInstanceValue;
    }
    

}

/**
 *  @Get the quantity of blocks that are being traded.
 */
-(float) getTradeQuantityValue {
    
    return blockQuantity;
}


/**
 *  @The full Value of this block (quantity and instance value).
 */
- (float) getFullQuantityTradeValue {
    
    return fullBlockQuantityTradeValue;
}



/**
 *
 */
-(void) setLastDoingTrade : (BOOL) lastDoingTrade_p {
    isLastDoingTrade = lastDoingTrade_p;
}


/**
 *
 */
-(BOOL) isLastDoingTrade {
    return isLastDoingTrade;
}

/**
 *
 */
-(float) getTradeInstanceValue {
    
    return blockInstanceValue;
}



/**
 *  @getTradeValue
 */
-(float) getTradeValue {
    return fullBlockValue;
    
}



/**
 *
 */
-(BOOL) floatHasDecimals : (float) number_p {
    
    return (number_p-(int)number_p != 0);

}

/**
 *  @updatePriceLabel
 */
-(void) updatePriceLabel {
    
    if ([self isBucketBlock] || [self isWalletCoin]) {
        float formattedValue = blockInstanceValue/1000;
        
        
        if ([self isBitCoinBlock]) {
            if ([self floatHasDecimals:bitcoinValuePercentage]) {
                priceLabel.text =[NSString stringWithFormat:@"%.2f %@",bitcoinValuePercentage, BITCOIN_CURRENCY];
            }
            else {
                priceLabel.text =[NSString stringWithFormat:@"%.0f %@",bitcoinValuePercentage, BITCOIN_CURRENCY];
            }
           
        }
        else {
          priceLabel.text =[NSString stringWithFormat:@"$%.3fK",formattedValue];
        }
        
    }
    
    else {
        priceLabel.text =[NSString stringWithFormat:@"%.2f",blockInstanceValue];
    }
    
    if ([self isBucketBlock]) {
        if ([self isBitCoinBlock]) {
            [priceLabel setPosition:CGPointMake(0-self.frame.size.width, 0)];
        }
        else {
            [priceLabel setPosition:CGPointMake(0 + self.frame.size.width, 0)];
        }
    }
    
    else {
        
        if ([self isBitCoinBlock]) {
            [priceLabel setPosition:CGPointMake(0 + self.frame.size.width, 0 - self.frame.size.height/3 )];
        }
        else {
            [priceLabel setPosition:CGPointMake(0 - self.frame.size.width, 0 - self.frame.size.height/3 )];
        }
        
    }

    
}

/**
 *  @ Appends a label
 */
-(void) attachPriceLabel {

    if (priceLabel != nil) {
        [self updatePriceLabel];
    }
    
    else {
        priceLabel = [SKLabelNode labelNodeWithFontNamed:BLOCK_LABEL_FONT];
        [priceLabel setName:@"block_label"];
        [self updatePriceLabel];
        priceLabel.fontColor = [UIColor whiteColor];
        priceLabel.fontSize = 13;
        priceLabel.zPosition = 101;
        [self addChild:priceLabel];
        if ([self isDropLineBlock]) {
            [self setSize:CGSizeMake(self.frame.size.width+priceLabel.frame.size.width, self.frame.size.height)];
        }
        
    }
 
}


/**
 *  @setupValueEffect
 */
-(void) setupGlassValueEffect : (float) coinValue_p {
    
    
    if (coinValue_p < 0 ) {
        
        NSLog(@"SOMETHING WENT WRONG");
    }
    
    // First time built.
    if (coinValue_p == 0) {
        
        NSString * uniqueName;
    
        if ([self isBitCoinBlock]) {
            coinAlphaBackground =[SKSpriteNode spriteNodeWithImageNamed:BITCOIN_TEXTURE];
            coinTextureImage = [SKSpriteNode spriteNodeWithImageNamed:BITCOIN_TEXTURE];
            uniqueName = [NSMutableString stringWithFormat:@"%@_%@", BLOCK_BLOCKCOIN_PREFIX, BITCOIN_BUCKET_ID];
        }
        else {
            coinAlphaBackground =[SKSpriteNode spriteNodeWithImageNamed:DOLLAR_TEXTURE];
            coinTextureImage = [SKSpriteNode spriteNodeWithImageNamed:DOLLAR_TEXTURE];
            uniqueName = [NSMutableString stringWithFormat:@"%@_%@", BLOCK_BLOCKCOIN_PREFIX, MONEY_BUCKET_ID];
        }
        
        [coinAlphaBackground setAlpha:0.5];
        [coinAlphaBackground setName:@"block_coin_background"];
        [coinAlphaBackground setSize:self.frame.size];
        
        [coinTextureImage setName:@"block_coin_texture"];
        [coinTextureImage setSize:self.frame.size];
    
        coinTextureMask = [SKSpriteNode spriteNodeWithColor:[SKColor blackColor] size: CGSizeMake(300, coinTextureImage.frame.size.height)];
        float ten_y =CGRectGetMinY(coinTextureImage.frame) - coinTextureImage.self.frame.size.height;
     
        
        coinTextureMask.position = CGPointMake(CGRectGetMinX(coinTextureImage.frame), ten_y);
    
        coinCropNode = [SKCropNode node];
        [coinCropNode addChild: coinTextureImage];
        [coinCropNode setMaskNode: coinTextureMask];
        [coinCropNode setName:@"block_coin_crop"];

        coinAlphaBackground.zPosition = 14;
        coinCropNode.zPosition = 15;
        [self addChild:coinAlphaBackground];
        [self addChild:coinCropNode];
   

    }
    
    else {

        
        if (coinValue_p <= BLOCK_BUCKET_TRADE_UNIQUE_VALUE) {

            float new_height;
            
            if ([self isBitCoinBlock]) {
                new_height = self.frame.size.height * coinValue_p / bitcoin_reference_lowest_value;
            }
            
            else {
                new_height = self.frame.size.height * coinValue_p / BLOCK_BUCKET_TRADE_UNIQUE_VALUE;
            }
            
            float mask_root = -(COIN_DEFAULT_SIZE);
            float up_move_y = mask_root + new_height;
            
            if ([self isDropLineBlock]) {
                [coinTextureMask runAction:[SKAction moveToY:0 duration:1.2]];
            }
            else {
                [coinTextureMask runAction:[SKAction moveToY:up_move_y duration:1.2]];
            }

        }
        
        
        fullBlockValue = coinValue_p;
        blockInstanceValue = coinValue_p;
        
    }
}


/**
 *  @cloneBlockCoinImage
 */
- (SKSpriteNode *) cloneBlockCoinImage  {
    
    SKSpriteNode * cloneBlockCoin;
    
    if ([self isBitCoinBlock]) {
        cloneBlockCoin = [SKSpriteNode spriteNodeWithImageNamed:@"bitcoin_texture.png"];
    }
    
    else {
        cloneBlockCoin = [SKSpriteNode spriteNodeWithImageNamed:@"dollar_texture.png"];
    }
    NSString * uniqueName = [NSMutableString stringWithFormat:@"CLONECOIN_%@", [self name]];
    
    [cloneBlockCoin setName:uniqueName];
    [cloneBlockCoin setPosition:[coinTextureImage position]];
    [cloneBlockCoin setSize:[coinTextureImage size]];
    cloneBlockCoin.zPosition = 100;
    
    [clonedCoins addObject:cloneBlockCoin];
    
    [self addChild:cloneBlockCoin];
    return cloneBlockCoin;
}

/**
 *  @getBlockCoinImage
 */
-(SKSpriteNode *) getBlockCoinImage {
   
    if (coinTextureImage != nil) {
       
    }
    else {
        
        NSString * uniqueName;
        if ([self isBitCoinBlock]) {
            coinTextureImage = [SKSpriteNode spriteNodeWithImageNamed:@"bitcoin_texture.png"];
            uniqueName = [NSMutableString stringWithFormat:@"%@_bitcoin", BLOCK_BLOCKCOIN_PREFIX];
            
        }
        else {
            coinTextureImage = [SKSpriteNode spriteNodeWithImageNamed:@"dollar_texture.png"];
            uniqueName = [NSMutableString stringWithFormat:@"%@_money", BLOCK_BLOCKCOIN_PREFIX];
        }
        
        [coinTextureImage setName:uniqueName];
        [coinTextureImage setSize:CGSizeMake(self.frame.size.width,self.frame.size.height)];
       
    }
    
    return coinTextureImage;
}



/**
 *
 */
- (BOOL) isWalletCoin {
    
    if ([[self name] hasPrefix:BLOCK_WALLET_PREFIX]) {
        return TRUE;
    }
    
    return FALSE;

}


/**
 *  @setType
 */
-(void) setCoinType : (NSString *) blockType_p {
    

    blockType = blockType_p;
    

    if ([blockType isEqualToString:BITCOIN_BUCKET_ID]) {
        isBitCoin = TRUE;
        
    }
    
    else if ([blockType isEqualToString:MONEY_BUCKET_ID]) {
        isBitCoin = FALSE;
    }
    
    if ([blockType_p isEqualToString:BITCOIN_WALLET_NAME]) {
        isBitCoin = TRUE;
        isWalletCoin = TRUE;
    }
    
    if ([blockType_p isEqualToString:MONEY_WALLET_NAME]) {
        isBitCoin = FALSE;
        isWalletCoin = TRUE;
    }
    
    if ([self isBucketBlock] || [self isWalletCoin] || [self isDropLineBlock] ) {
        [self setupGlassValueEffect:0];
    }

    
}


/**
 *  @getTyoe
 */
-(NSString *) getType  {
    return blockType;
}



@end
